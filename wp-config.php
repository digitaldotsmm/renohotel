<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
switch ($_SERVER['HTTP_HOST']) {   
    case 'renohotel.dd':    
        define('DB_NAME', 'renohotel');
        define('DB_USER', 'webuser');
        define('DB_PASSWORD', 'nopassword');
        define('DB_HOST', '192.168.1.200');
        break;
    case 'renohotel.myanmarcafe.info':
        define('DB_NAME', 'digitald_renohotel');
        define('DB_USER', 'digitald_webuser');
        define('DB_PASSWORD', '9UrEbO{g0Ck3');
        define('DB_HOST', 'localhost');
        break;
    default :
        define('DB_NAME', 'renohote_production');
        define('DB_USER', 'renohote_webuser');
        define('DB_PASSWORD', '4TVzGZqCLTis');
        define('DB_HOST', 'localhost');
        break;
}
$protocol = (!empty($_SERVER['HTTPS']) ) ? 'https://' : 'http://';
define('WP_SITEURL', $protocol . $_SERVER['HTTP_HOST']);
define('WP_HOME', $protocol . $_SERVER['HTTP_HOST']);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}56$%dKy`nTZg.kPML6pPRtq|7$d_|IMnuE!AxcLH|f3Md.#x/-XXHiA}%Bm2]{o');
define('SECURE_AUTH_KEY',  '%lOE3fPe[OZ^I/Ig!Z4cLKLSl7LsTQ!3TTOC6,suXL*pUKI)ReFWW>mX?w9i!P(%');
define('LOGGED_IN_KEY',    '2OL}Ve^,#wAvJZ]J8[GWdXzS`&8Lxg>8kLhMdv*9/Oac@BxW<ukBdC?tLYRc{0EB');
define('NONCE_KEY',        'EMB2@w+Ru,W9<> Y}PL_T.+.d4qC^+u-+(5^^kKf)}Bob-6,<9{$F-ico5%_w :{');
define('AUTH_SALT',        'R{ktX*pFeS j>h+KKAhu;92Na4m(F3y$5e(bLRBS|5Q@SjO6-97O;bd3oI;;?#l-');
define('SECURE_AUTH_SALT', 'oyp{6fp,)n9kr}V._fLl>Wx)h@16,;0ids|Q=~81DoOD1{A:m<u-j3Po3!$;jjk$');
define('LOGGED_IN_SALT',   'ig0v/nq LS~v#@uGsliLt*PGMy:1{$SQpv%V~ht:g8`Z87_U,>O|6~F!2.btB<B:');
define('NONCE_SALT',       '}~c3Iz~YkTQ[adm Adm^R!u$r-H>]e%G>W61>QnZjsb|@^^phCW?O8|VX2{iuy ~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('AUTOSAVE_INTERVAL', 180);  
define( 'WP_AUTO_UPDATE_CORE', false);
define( 'WP_POST_REVISIONS', 3 );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
