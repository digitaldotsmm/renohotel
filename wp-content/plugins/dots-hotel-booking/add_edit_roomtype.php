<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

if(isset($_POST['submitRoomtype'])){
	include("includes/db.conn.php"); 
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->add_edit_roomtype();
	header("location:admin.php?page=room-type-manager");	
	exit;
}
include("includes/conf.class.php");
include("includes/admin.class.php");
if(isset($_GET['id']) && $_GET['id'] != ""){
	$id = $bsiCore->ClearInput($_GET['id']);
	if($id){
		$result = mysql_query($bsiAdminMain->getRoomtypesql($id));
		$row    = mysql_fetch_assoc($result);
		$readonly = 'readonly="readonly"';
	}else{
		$row    = NULL;
		$readonly = '';
	}
}else{
	header("location:admin.php?page=room-type-manager");
	exit;
}
?>  
 
      <div id="container-inside" class="wrap">      
          <h2>Room Type Add / Edit</h2>
          <div class="">
<input type="button" value="Back to Room Type List" class="button-secondary alignright" onClick="window.location.href='admin.php?page=room-type-manager'" />
          </div>
     <hr style="margin-top:10px;" />
   
        <form action="<?php echo admin_url('admin.php?page=aad-new-room-type&noheader=true'); ?>" method="post" id="form1" enctype="multipart/form-data">
          <table class="widefat" cellpadding="5" cellspacing="2" border="0" width="100%">
            <tr>
              <td width="200px"><strong>Room Type Title:</strong></td>
              <td valign="middle"><input type="text" name="roomtype_title" id="roomtype_title" class="required" value="<?php echo  $row['type_name']; ?>" style="width:250px;" /> &nbsp;&nbsp;example: Deluxe, Standard</td>
            </tr>
              <tr>
              <td><strong>Room Type Info Page:</strong></td>                           
              <td>
                  <?php $room_post_types = get_posts(array('post_type'=>RN_ROOM,'posts_per_page'=>-1,'post_status'=>'publish')); ?>                  
                  <?php if($room_post_types): ?>                   
                        <select id="room-post-id" name="room_post_id">
                          <?php foreach( $room_post_types as $room_post_type ): ?>                           
                          <option value="<?php echo $room_post_type->ID; ?>" <?php echo ($room_post_type->ID == (int) $row['room_post_id']) ? ' selected' : ''; ?>><?php echo $room_post_type->post_title; ?></option>
                          <?php endforeach; ?>
                        </select>
                  <?php else: ?>
                    <span>Sorry! There is no room type info page.</span>
                  <?php endif; ?>
              </td>
            </tr>
            <tr>
              <td><strong> Room Image:</strong></td>
              <td><input type="file" name="img" id="img" /></<td> 
			  
			  <?php if($row['img1'] != ""){ echo '<span style="margin-left:15px"><a href="'.HOTELBOOKING_MANAGER_URL ."front/img/". $row['img1'].'" target="_blank"><strong>View Image</a></strong>&nbsp;&nbsp;&nbsp;&nbsp;<font color="#FF0000"><strong>Delete Image</strong>&nbsp;</font><input type="checkbox" name="delimg"></span>'; }?></td>
            </tr>
            <tr>           
              <td><input type="hidden" name="addedit" value="<?php echo $id?>"></td>
              <td><input type="submit" value="Submit" name="submitRoomtype" class="button-primary" /></td>
            </tr>
          </table>
        </form>
      </div>
<script type="text/javascript">
	jQuery().ready(function() { jQuery("#form1").validate(); });       
</script>     
 
</div>
</div>
</div>