<?php

/*
  Plugin Name: Online Hotel Booking System
  Plugin URI: http://www.digitaldots.com.mm
  Description: Online hotel booking system
  Version: 1
  Author: DigitalDots
  Author URI: http://www.digitaldots.com.mm
 */

if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

/**
 * @internal Nobody should be able to overrule the real version number as this can cause serious issues
 * with the options, so no if ( ! defined() )
 */
define( 'HOTELBOOKING_VERSION', '1.0' );

define('HOTELBOOKING_MANAGER_URL', plugin_dir_url(__FILE__));
define('HOTELBOOKING_MANAGER_PATH', plugin_dir_path(__FILE__));
define('HOTELBOOKING_MANAGER_BASENAME', plugin_basename(__FILE__));

function db_install() {
    global $wpdb;
    //global $jal_db_version;
    $sql = "CREATE TABLE IF NOT EXISTS dots_advance_payment (
  month_num int(11) NOT NULL auto_increment,
  `month` varchar(255) NOT NULL,
  deposit_percent decimal(10,2) NOT NULL,
  PRIMARY KEY  (month_num)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_bookings (
  booking_id int(10) unsigned NOT NULL,
  booking_time datetime NOT NULL default '0000-00-00 00:00:00',
  start_date date NOT NULL default '0000-00-00',
  end_date date NOT NULL default '0000-00-00',
  client_id int(10) unsigned default NULL,
  child_count int(2) NOT NULL default '0',
  extra_guest_count int(2) NOT NULL default '0',
  discount_coupon varchar(50) default NULL,
  total_cost decimal(10,2) unsigned NOT NULL default '0.00',
  payment_amount decimal(10,2) NOT NULL default '0.00',
  payment_type varchar(255) NOT NULL,
  payment_success tinyint(1) NOT NULL default '0',
  payment_txnid varchar(100) default NULL,
  paypal_email varchar(500) default NULL,
  special_id int(10) unsigned NOT NULL default '0',
  special_requests text character set ucs2,
  is_block tinyint(4) NOT NULL default '0',
  is_deleted tinyint(4) NOT NULL default '0',
  block_name varchar(255) default NULL,
  PRIMARY KEY  (booking_id),
  KEY start_date (start_date),
  KEY end_date (end_date),
  KEY booking_time (discount_coupon)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_capacity (
  id int(11) NOT NULL auto_increment,
  title varchar(255) NOT NULL,
  capacity int(11) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_cc_info (
  booking_id varchar(100) NOT NULL,
  cardholder_name varchar(255) NOT NULL,
  card_type varchar(50) NOT NULL,
  card_number blob NOT NULL,
  expiry_date varchar(10) NOT NULL,
  ccv2_no int(4) NOT NULL,
  PRIMARY KEY  (booking_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_clients (
  client_id int(10) unsigned NOT NULL auto_increment,
  first_name varchar(64) default NULL,
  surname varchar(64) default NULL,
  title varchar(16) default NULL,
  street_addr text,
  city varchar(64) default NULL,
  province varchar(128) default NULL,
  zip varchar(64) default NULL,
  country varchar(64) default NULL,
  phone varchar(64) default NULL,
  fax varchar(64) default NULL,
  email varchar(128) default NULL,
  additional_comments text,
  ip varchar(32) default NULL,
  existing_client tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (client_id),
  KEY email (email)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_configure (
  conf_id int(11) NOT NULL auto_increment,
  conf_key varchar(100) NOT NULL,
  conf_value varchar(500) default NULL,
  PRIMARY KEY  (conf_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='bsi hotel configurations';
";
    $wpdb->query($sql);
    $sql = "INSERT INTO dots_configure VALUES
(1, 'conf_hotel_name', 'Hotel Booking System'),
(2, 'conf_hotel_streetaddr', 'Your Address'),
(3, 'conf_hotel_city', 'Your City'),
(4, 'conf_hotel_state', 'Your State'),
(5, 'conf_hotel_country', 'Your Country Name'),
(6, 'conf_hotel_zipcode', '111111'),
(7, 'conf_hotel_phone', '+95 9 7323 6760'),
(8, 'conf_hotel_fax', '+111222333'),
(9, 'conf_hotel_email', 'hello@digitaldots.com.mm'),
(13, 'conf_currency_symbol', '$'),
(14, 'conf_currency_code', 'USD'),
(20, 'conf_tax_amount', '12.50'),
(21, 'conf_dateformat', 'mm/dd/yy'),
(22, 'conf_booking_exptime', '1000'),
(25, 'conf_enabled_deposit', '1'),
(26, 'conf_hotel_timezone', 'Asia/Calcutta'),
(27, 'conf_booking_turn_off', '0'),
(28, 'conf_min_night_booking', '2'),
(30, 'conf_notification_email', 'hello@digitaldots.com.mm'),
(31, 'conf_price_with_tax', '0'),
(32, 'conf_tos_url', 'http://www.digitaldots.com.mm'),
(33, 'conf_language', 'en.php');";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_email_contents (
  id int(11) NOT NULL auto_increment,
  email_name varchar(500) NOT NULL,
  email_subject varchar(500) NOT NULL,
  email_text longtext NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "INSERT INTO dots_email_contents VALUES
(1, 'Confirmation Email', 'Confirmation of your successfull booking in our hotel', '<p><strong>Text can be change in admin panel</strong></p>\r\n'),
(2, 'Cancellation Email ', 'Cancellation Email subject', '<p><strong>Text can be change in admin panel</strong></p>\r\n\r\n\r\n');";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_invoice (
  booking_id int(10) NOT NULL,
  client_name varchar(500) NOT NULL,
  client_email varchar(500) NOT NULL,
  invoice longtext NOT NULL,
  PRIMARY KEY  (booking_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_payment_gateway (
  id int(11) NOT NULL auto_increment,
  gateway_name varchar(255) NOT NULL,
  gateway_code varchar(50) NOT NULL,
  account varchar(255) default NULL,
  enabled tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_priceplan (
  plan_id int(10) NOT NULL auto_increment,
  roomtype_id int(10) default NULL,
  capacity_id int(11) NOT NULL,
  start_date date default NULL,
  end_date date default NULL,
  sun decimal(10,2) default '0.00',
  mon decimal(10,2) default '0.00',
  tue decimal(10,2) default '0.00',
  wed decimal(10,2) default '0.00',
  thu decimal(10,2) default '0.00',
  fri decimal(10,2) default '0.00',
  sat decimal(10,2) default '0.00',
  default_plan tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (plan_id),
  KEY priceplan (roomtype_id,capacity_id,start_date,end_date)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "
CREATE TABLE IF NOT EXISTS dots_reservation (
  id int(11) NOT NULL auto_increment,
  bookings_id int(11) NOT NULL,
  room_id int(11) NOT NULL,
  room_type_id int(11) NOT NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "INSERT INTO dots_payment_gateway VALUES
(1, 'Paypal', 'pp', 'wtun.25@gmail.com', 1),
(2, 'Manual : Pay on Arrival', 'poa', NULL, 1),
(3, 'Credit Card', 'cc', NULL, 1);";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_room (
  room_ID int(10) NOT NULL auto_increment,
  roomtype_id int(10) default NULL,
  room_no varchar(255) default NULL,
  capacity_id int(10) default NULL,
  no_of_child int(11) NOT NULL default '0',
  extra_bed tinyint(1) default '0',  
  PRIMARY KEY  (room_ID),
  KEY roomtype_id (roomtype_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "CREATE TABLE IF NOT EXISTS dots_roomtype (
  roomtype_ID int(10) NOT NULL auto_increment,
  type_name varchar(500) default NULL,
  description text,
  path text NOT NULL,
  img1 varchar(100) NOT NULL default 'NOT NULL',
  img2 varchar(100) NOT NULL default 'NOT NULL',
  img3 varchar(100) NOT NULL default 'NOT NULL',
  img4 varchar(100) NOT NULL default 'NOT NULL',
  room_post_id(10) default NULL,
  PRIMARY KEY  (roomtype_ID)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
    $wpdb->query($sql);
    $sql = "
INSERT INTO dots_advance_payment VALUES
(1, 'January', 0.00),
(2, 'February', 0.00),
(3, 'March', 0.00),
(4, 'April', 0.00),
(5, 'May', 0.00),
(6, 'June', 0.00),
(7, 'July', 20.00),
(8, 'August', 20.00),
(9, 'September', 10.00),
(10, 'October', 10.00),
(11, 'November', 0.00),
(12, 'December', 0.00);";
    $wpdb->query($sql);
}

/* define('bsi_plugin_url', plugin_dir_url( __FILE__ ));
  wp_register_script('jquery-ui.min.js', bsi_plugin_url. '/js/jquery-ui.min.js', array('jquery'));
  wp_enqueue_script('jquery-ui.min.js');
  wp_register_script('jquery.ui.datepicker-en.js',bsi_plugin_url .'/js/dtpicker/jquery.ui.datepicker-en.js', array('jquery'));
  wp_enqueue_script('jquery.ui.datepicker-en.js');
  wp_register_style('datepicker.css', bsi_plugin_url . '/css/datepicker.css');
  wp_enqueue_style('datepicker.css'); */
register_activation_hook(__FILE__, 'db_install');
register_deactivation_hook(__FILE__, 'bsi_uninstall');
add_action('admin_enqueue_scripts', 'admin_f');
add_action('init', 'wp29r01_date_picker');

function admin_f() {
    wp_enqueue_script('jquery-ui-datepicker', array('jquery', 'jquery-ui-core'));
    wp_enqueue_style('jquery-style', HOTELBOOKING_MANAGER_URL.'front/css/datepicker.css');
    wp_enqueue_style('custom-style7', HOTELBOOKING_MANAGER_URL.'css/style.css');
    wp_enqueue_style('custom-style8', HOTELBOOKING_MANAGER_URL.'css/jquery.validate.css');
    wp_enqueue_script('jquery-validate', HOTELBOOKING_MANAGER_URL.'js/jquery.validate.js');
}

function wp29r01_date_picker() {
    wp_enqueue_script('jquery-ui-datepicker', array('jquery', 'jquery-ui-core'));
    wp_enqueue_style('jquery-style', HOTELBOOKING_MANAGER_URL.'front/css/datepicker.css');
    wp_enqueue_style('jquery-style2', HOTELBOOKING_MANAGER_URL.'front/css/style.css');
}

add_action('admin_menu', 'menu_creator');
if (!session_id()) {
    add_action('init', 'session_start');
}
add_shortcode("dots_hotel_booking", "dis_front_func");

function menu_creator() {
    $icon_url = HOTELBOOKING_MANAGER_URL.'images/plugin.ico';
    // starting of the 1st menu
    add_menu_page('Bsi Hotel', 'Hotel Manager', 'manage_options', 'hotel_manager', 'defaul', $icon_url);
    add_submenu_page("hotel_manager", "Hotel Details", "Hotel Details", "manage_options", "hotel-details", 'hotel_details'); // FOR THE MAIN MENU

    add_submenu_page("hotel_manager", "Room Manager", "Room Manager", "manage_options", "Room-Manager", 'room_list'); // FOR THE SUB MENU
    add_submenu_page('Room-Manager', 'Add New Room', 'Add New Room', 'manage_options', 'aad-new-room', 'add_room'); // FOR THE INNER PAGE
    //add_links_page( $page_title, $menu_title, $capability, $menu_slug, $function);
//  testing menu	
//add_submenu_page( "hotel_manager", "Room upload", "Room upload", "manage_options", "roomupload", 'room_upload'); // FOR THE SUB MENUadd_pages_page('Hotel Maneger', 'add new room', 'manage_options', 'add-room', 'add_room'); // FOR THE INNER PAGE

    add_submenu_page("hotel_manager", "room type manager", "Room Type Manager", "manage_options", "room-type-manager", 'room_type');
    add_submenu_page('Hotel Maneger', 'add new room type', 'add new room type', 'manage_options', 'aad-new-room-type', 'add_room_type'); // FOR THE INNER PAGE

    add_submenu_page("hotel_manager", "capacity manager", "Capacity Manager", "manage_options", "capacity-manager", 'capacity_manager');
    add_submenu_page('Hotel Maneger', 'add capacity', 'add capacity', 'manage_options', 'aad-edit-capacity', 'add_capacity'); // FOR THE INNER PAGE
    // ending of the 1st menu
    //2n menu starting menu

    add_menu_page('Bsi Hotel2', 'Price Manager', 'manage_options', 'price_manager', 'defaul', $icon_url);
    add_submenu_page("price_manager", "price plan", "Price Plan", "manage_options", "price-plan", 'price_plan'); // FOR THE MAIN MENU
    add_submenu_page('price-plan', 'Add Price Plan', 'Add Price Plan', 'manage_options', 'add-price-plan', 'add_price'); // FOR THE INNER PAGE
    add_submenu_page("price_manager", "advance payment", "Advance Payment", "manage_options", "advance-payment", 'advance_payment');


    add_menu_page('Bsi Hotel3', 'Booking Manager', 'manage_options', 'booking_maneger', 'booking_list', $icon_url);
    add_submenu_page("booking_maneger", "booking list", "Booking list", "manage_options", "booking-list", 'booking_list'); // FOR THE MAIN MENU
    add_submenu_page('booking-list', 'View Booking List ', 'View Booking List ', 'manage_options', 'View-Booking-List', 'view_booking_list'); // FOR THE INNER PAGE
// FOR THE INNER PAGE
    add_submenu_page('Customerlookup', 'customer booking', 'customer booking', 'manage_options', 'Customer-booking', 'Customer_booking'); // FOR THE INNER PAGE
    add_submenu_page('Customer-booking', 'customer booking edit', 'customer booking edit', 'manage_options', 'Customer-booking-edit', 'Customer_booking_edit'); // FOR THE INNER PAGE
    add_submenu_page('booking-list', 'View Details', 'View Details', 'manage_options', 'view-details', 'view_details'); // FOR THE INNER PAGE
    add_submenu_page("booking_maneger", "Customer Lookup", "Customer Lookup", "manage_options", "Customerlookup", 'Customer_Lookup'); // FOR THE MAIN MENU 
//    add_submenu_page("booking_maneger", "Calender View", "Calender View", "manage_options", "Calender-View", 'Calender_View'); // FOR THE MAIN MENU
    add_submenu_page("booking_maneger", "admin block room", "Admin Block Room", "manage_options", "admin-block-room", 'admin_block_room'); // FOR THE MAIN MENU
    add_submenu_page('admin-block-room', 'block room', 'block room', 'manage_options', 'block-room', 'block_room'); // FOR THE INNER PAGE
    add_submenu_page('Customerlookup', 'customer lookup edit', 'customer lookup edit', 'manage_options', 'customer-lookup-edit', 'customer_lookup_edit'); // FOR THE INNER PAGE

    add_menu_page('Bsi Hotel4', 'Hotel Setting', 'manage_options', 'Setting', 'defaul', $icon_url);
    add_submenu_page("Setting", "Global Setting", "Global Setting", "manage_options", "global-setting", 'global_setting'); // FOR THE MAIN MENU
    add_submenu_page("Setting", "Payment Getway", "Payment Getway", "manage_options", "Payment-Getway", 'Payment_Getway'); // FOR THE MAIN MENU
    add_submenu_page("Setting", "email content", "Email Content", "manage_options", "email-content", 'email_content'); // FOR THE MAIN MENU
    //removing the extra main menu from the menu list
    remove_submenu_page('booking_maneger', 'booking_maneger');
    remove_submenu_page('page', 'aad-new-room');
    remove_submenu_page('price_manager', 'price_manager');
    remove_submenu_page('hotel_manager', 'hotel_manager');
    remove_submenu_page('Setting', 'Setting');
    remove_submenu_page('Setting', 'Setting');
}

function Customer_booking_edit() {

    include('customerlookupEdit.php');
}

function bsi_uninstall() {
    global $wpdb;
    $sql = "DROP TABLE `dots_advance_payment`, `dots_bookings`, `dots_capacity`, `dots_cc_info`, `dots_clients`, `dots_configure`, `dots_email_contents`, `dots_invoice`, `dots_payment_gateway`, `dots_priceplan`, `dots_reservation`, `dots_room`, `dots_roomtype`;";
    $wpdb->query($sql);
}

function view_details() {
    include('viewdetails.php');
}

function defaul() {
    echo "<h1>Select a Submenu</h1>";
}

function hotel_details() {
    if (!isset($_SESSION['url'])) {
        $_SESSION['url'] = admin_url();
    }


    //include("/includes/conf.class.php");
    include('admin_hotel_details.php');
}

function room_list() {

    include('room_list.php');
//echo  HOTELBOOKING_MANAGER_URL."js/DataTables/jquery.dataTables.js";
}

function customer_lookup_edit() {
    include('customerlookupEdit.php');
}

function booking_details() {
    include('booking_details.php');
}

function add_room() {

//	echo $_SESSION['url'];
    include('add_edit_room.php');
    /* session_start();
      $_SESSION['url']=admin_url(); */
}

function room_type() {
    include('roomtype.php');
}

function capacity_manager() {
    include("admin_capacity.php");
}

function add_edit_capacity() {
    include('add_edit_capacity.php');
}

function add_capacity() {

    include('add_edit_capacity.php');
}

function price_plan() {

    include('priceplan.php');
}

function add_price() {
    include("add_edit_priceplan.php");
}

function advance_payment() {


    include("advance_payment.php");
}

function add_room_type() {
    include('add_edit_roomtype.php');
}

function room_upload() {
    
}

function booking_list() {
    include('view_bookings.php');
}

function view_booking_list() {

    include('view_active_or_archieve_bookings.php');
}

function Customer_Lookup() {
    include("customerlookup.php");
}

function Calender_View() {
    include('calendar_view.php');
}

function admin_block_room() {
    include("admin_block_room.php");
}

function block_room() {
    include("block_room.php");
}

function global_setting() {
    include('global_setting.php');
}

function Payment_Getway() {

    include('payment_gateway.php');
}

function email_content() {

    include("email_content.php");
}

function Customer_booking() {
    include('customerbooking.php');
}

function dis_front_func() {
    if (!isset($_REQUEST['check_in']) && !isset($_REQUEST['check_in']) && !isset($_REQUEST['registerButton']) && !isset($_REQUEST['registerButton2']) && !isset($_REQUEST['success_code']) && !isset($_REQUEST['error_code']) && !isset($_REQUEST['amount']) && !isset($_REQUEST['x_invoice_num']) && !isset($_REQUEST['CardNumber']) && !isset($_REQUEST['action'])) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/index.php";
        include($dir);
    }

    if (isset($_REQUEST['check_in'])) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/booking-search.php";
        include($dir);
    }


    if (isset($_REQUEST['registerButton'])) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/booking_details.php";
        include($dir);
    }

//    if (isset($_REQUEST['registerButton2'])) {
//        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/booking-process.php";
//        include($dir);
//    }

    if (isset($_REQUEST['success_code'])) {

        unset($_SESSION['success_code']);
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/booking-confirm.php";
        include($dir);
    }
    if (isset($_REQUEST['error_code'])) {

        unset($_SESSION['success_code']);
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/booking-confirm.php";
        include($dir);
    }
//    if ( (isset($_REQUEST['pay_type']) && $_REQUEST['pay_type'] == 'authorizenet') || isset($_POST["x_response_code"]) ) {                  
//        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/authorizenet.php";
//        include($dir);
//    }
    
    if ( ( isset($_REQUEST['registerButton2']) && isset($_REQUEST['payment_type']) && $_REQUEST['payment_type'] == 'aunet') || isset($_REQUEST["x_response_code"]) ) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/authorizenet.php";
        include($dir);
    }
    if ( isset($_REQUEST['amount']) || ( isset($_REQUEST['registerButton2']) && isset($_REQUEST['payment_type']) && $_REQUEST['payment_type'] == 'pp') ) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/paypal.php";
        include($dir);
    }
    
    if ( isset($_REQUEST['payment_type']) && $_REQUEST['payment_type'] == 'kbz') {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/kbz.php";
        include($dir);
    }
    if ( isset($_REQUEST['payment_type']) && $_REQUEST['payment_type'] == 'cb') {
        
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/cb.php";
        include($dir);
    }
    if ( isset($_REQUEST['payment_type']) && $_REQUEST['payment_type'] == 'mpu') {
        
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/mpu.php";
        include($dir);
    }

    if (isset($_REQUEST['x_invoice_num_cc'])) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/offlinecc-payment.php";
        include($dir);
    }

    if (isset($_REQUEST['CardNumber'])) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/cc_process.php";
        include($dir);
    }

    if (isset($_REQUEST['action'])) {
        $dir = ABSPATH . "wp-content/plugins/dots-hotel-booking/front/paypal.php";
        include($dir);
    }
}

function get_client_detail() {
    global $bsiCore;
    include_once("front/ajaxreq-processor.php");
    die;
}

add_action( 'wp_ajax_get_client_detail', 'get_client_detail' );
add_action( 'wp_ajax_nopriv_get_client_detail', 'get_client_detail' );

?>