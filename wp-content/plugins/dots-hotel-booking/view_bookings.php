<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
include("includes/conf.class.php");
include("includes/admin.class.php");
if(isset($_GET['book_id'])){ // delete booking
	$bsiAdminMain->delete_booking();
	header("location:admin.php?page=booking-list");	
	exit;
}
if(isset($_GET['payment_id'])){ // update payment status
	$bsiAdminMain->update_payment_status();
	header("location:admin.php?page=booking-list");	
	exit;
}
?>    
<?php
	wp_enqueue_style('jquery-style', HOTELBOOKING_MANAGER_URL .'front/css/datepicker.css');
	
	wp_enqueue_script( 'custom_script10', HOTELBOOKING_MANAGER_URL .'js/bsi_datatables.js');
		wp_enqueue_script( 'custom_script11', HOTELBOOKING_MANAGER_URL .'js/DataTables/jquery.dataTables.js');
		wp_enqueue_style('custom-style9', HOTELBOOKING_MANAGER_URL .'css/data.table.css');
		wp_enqueue_style('custom-style10', HOTELBOOKING_MANAGER_URL .'css/jqueryui.css');
?>
 <script type="text/javascript">
	jQuery(document).ready(function(){
		disableInput("#submit");
		jQuery('#book_type').change(function(){
			if(jQuery('#book_type').val() != ""){
				enableInput("#submit");			
			}else{
				disableInput("#submit");
			}
		});
		//Enabling Disabling Function
		function disableInput(id){
			jQuery(id).attr('disabled', 'disabled');
		}
		function enableInput(id){
			jQuery(id).removeAttr('disabled');	
		}
	});
</script>      
      <div id="container-inside" class="widefat">
      <h2><?php echo "VIEW BOOKING LIST";?></h2>
      <hr />
        <form action="<?php echo admin_url('admin.php?page=View-Booking-List'); ?>" method="post" id="form1">
          <table class="widefat" cellpadding="5" cellspacing="2" border="0">
            <tr>
            <td valign="middle" width="200px"><strong><?php echo "SELECT BOOKING TYPE";?></strong>:</td>
            <td><select name="book_type" id="book_type"><option value="" selected="selected">---BOOKINGS TYPE---</option><option value="1">ACTIVE BOOKING </option><option value="2" > BOOKING HISTORY</option></select> </td>
            
            </tr>
            
           <tr><td><strong>Date Range(optional)	</strong></td><td><input id="txtFromDate" name="fromDate" style="width:68px" type="text" readonly="readonly" />
      <span style="padding-left:0px;"><a id="datepickerImage" href="javascript:;"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>images/month.png" height="16px" width="16px" style=" margin-bottom:-4px;" border="0" /></a></span>&nbsp;&nbsp;&nbsp;&nbsp; <strong>TO</strong>&nbsp;&nbsp;&nbsp;<input id="txtToDate" name="toDate" style="width:68px" type="text" readonly="readonly"/>
      <span style="padding-left:0px;"><a id="datepickerImage1" href="javascript:;"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>images/month.png" height="18px" width="18px" style=" margin-bottom:-4px;" border="0" /></a></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong>BY    </strong>&nbsp;&nbsp;&nbsp;&nbsp;<select name="shortby"><option value="booking_time" selected="selected">BOOKING DATE</option><option value="start_date">CHECK IN DATE</option><option value="end_date">CHECK OUT DATE</option></select></td></tr>
           <tr><td></td><td><input type="submit" value="<?php echo "SUBMIT";?>" name="submit" id="submit" class="button-primary"/></td></tr>
          </table>
        </form>
        <br /><br />
         <span style="font-size:16px; font-weight:bold">Latest 10 Booking</span>
     	 <hr />
          <table class="display datatable" border="0" id="test5">
         <?php echo $bsiAdminMain->homewidget(1); ?>
        </table>
         <br />
         
         <span style="font-size:16px; font-weight:bold">Today Check In</span>
      	 <hr />
          <table class="display datatable" border="0">
         <?php echo $bsiAdminMain->homewidget(2); ?>
        </table>
         <br />
         
         <span style="font-size:16px; font-weight:bold">Today Check Out</span>
         <hr />
          <table class="display datatable" border="0">
        <?php echo $bsiAdminMain->homewidget(3); ?>
        </table>
         <br />
      </div>
      
      
         
<script type="text/javascript">
	jQuery().ready(function() {
		jQuery("#form1").validate();
		
     });
         
</script>      

<script type="text/javascript">
jQuery(document).ready(function(){
 jQuery.datepicker.setDefaults({ dateFormat: '<?php echo $bsiCore->config['conf_dateformat']?>' });
    jQuery("#txtFromDate").datepicker({
        maxDate: "+365D",
        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",
        onSelect: function(selected) {
    	var date = jQuery(this).datepicker('getDate');
         if(date){
            date.setDate(date.getDate());
          }
          jQuery("#txtToDate").datepicker("option","minDate", date)
        } 
    });
 
    jQuery("#txtToDate").datepicker({ 
        maxDate:"+365D",
        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",
        onSelect: function(selected) {
           jQuery("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });  
 jQuery("#datepickerImage").click(function() { 
    jQuery("#txtFromDate").datepicker("show");
  });
 jQuery("#datepickerImage1").click(function() { 
    jQuery("#txtToDate").datepicker("show");
  });
});
</script>

<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} ); 
} );
</script> 