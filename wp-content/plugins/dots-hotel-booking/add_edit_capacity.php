<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if(isset($_POST['submitCapacity'])){
	include("includes/db.conn.php"); 
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	
	$bsiAdminMain->add_edit_capacity();
	header("location:admin.php?page=capacity-manager");	
	exit;
}
include("includes/conf.class.php");
include("includes/admin.class.php");
if(isset($_GET['id']) && $_GET['id'] != ""){
	$id = $bsiCore->ClearInput($_GET['id']);
	if($id){
		$result = mysql_query($bsiAdminMain->getCapacitysql($id));
		$row    = mysql_fetch_assoc($result);
	}else{
		$row    = NULL;
		$readonly = '';
	}
}else{
	header("location:admin.php?page=capacity-manager");
	exit;
}
?>  
 
 
 <script type="text/javascript">
function capacitydelete(cid){
	var ans=confirm('Do You Want to delete the selected Capacity? Remember Corresponding Room And Roomtype Will Be Deleted Forever');
	if(ans){
		window.location='admin.php?page=capacity-manager&noheader=true&cdelid='+cid;
		return true;
		
	}else{
		return false;
		
	}
	
}
</script>
 
 
 
      <div id="container-inside" class="wrap">     
          <h2>Capacity Add / Edit</h2>
<input type="button" value="Back to Capacity List" class="button-secondary" onClick="window.location.href='admin.php?page=capacity-manager'" />
     <hr style="margin-top:10px;" />
        <form action="<?php echo admin_url('admin.php?page=aad-edit-capacity&noheader=true'); ?>" method="post" id="form1">
          <table class="widefat" cellpadding="5" cellspacing="2" border="0">
            <tr>
              <td width="200px"><strong>Title:</strong></td>
              <td valign="middle"><input type="text" name="capacity_title" id="capacity_title" class="required" value="<?php echo $row['title']?>" style="width:250px;" /> &nbsp;&nbsp;Example Single / Double </td>
            </tr>
            <tr>
              <td><strong>Number Of Adult:</strong></td>
              <td><input type="text" name="no_adult" id="no_adult" <?php echo $readonly?> value="<?php echo $row['capacity']?>" class="required number" style="width:70px;"  /></td>
            </tr>
            
            
              <td><input type="hidden" name="addedit" value="<?php echo $id?>"></td>
              <td><input type="submit" value="Submit" name="submitCapacity" class="button-primary" /></td>
            </tr>
          </table>
        </form>
      </div>
<script type="text/javascript">
	jQuery().ready(function() {jQuery("#form1").validate();  });
         
</script>      
</div>
</div>
</div>
