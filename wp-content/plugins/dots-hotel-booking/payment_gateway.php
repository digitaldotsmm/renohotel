<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if (isset($_POST['act_sbmt'])) {

    include("includes/db.conn.php");

    include("includes/conf.class.php");

    include("includes/admin.class.php");

    $bsiAdminMain->payment_gateway_post();
    header("location:admin.php?page=Payment-Getway");
}



include("includes/conf.class.php");

include("includes/admin.class.php");

$payment_gateway_val = $bsiAdminMain->payment_gateway();
?>

<div id="container-inside" class="wrap">

    <h2>Payment Gateway Title:</h2>



    <form action="<?php echo $_SERVER['PHP_SELF'] ?>?page=Payment-Getway&noheader=true" method="post" id="form1">

        <table cellpadding="5" cellspacing="2" border="0" class="widefat">

            <thead>

                <tr><th align="left">Enabled</th><th align="left">Gateway</th><th align="left">Payment Gateway Title</th><th align="left">Account Info</th></tr>



            </thead>

            <tbody>



                <tr>

                    <td><input type="checkbox" value="pp" name="pp"  id="pp" <?php echo ($payment_gateway_val['pp_enabled']) ? 'checked="checked"' : ''; ?> /></td>

                    <td><strong>Paypal:</strong></td>

                    <td><input type="text" name="pp_title" id="pp_title" value="<?php echo $payment_gateway_val['pp_gateway_name'] ?>" size="30" class="required"/></td>

                    <td><input type="text" name="paypal_id" id="paypal_id" class="required email" value="<?php echo $payment_gateway_val['pp_account'] ?>" size="40"/>

                        Enter your paypal Info</td>

                </tr>
                <tr>

                    <td><input type="checkbox" value="cb" name="cb"  id="aunet" <?php echo ($payment_gateway_val['cb_enabled']) ? 'checked="checked"' : ''; ?> /></td>

                    <td><strong>CB Bank:</strong></td>

                    <td><input type="text" name="cb_title" id="cb_title" value="<?php echo $payment_gateway_val['cb_gateway_name'] ?>" size="30" class="required"/></td>
                  

                </tr>
                
                <tr>

                    <td><input type="checkbox" value="kbz" name="kbz"  id="aunet" <?php echo ($payment_gateway_val['kbz_enabled']) ? 'checked="checked"' : ''; ?> /></td>

                    <td><strong>KBZ:</strong></td>

                    <td><input type="text" name="kbz_title" id="kbz_title" value="<?php echo $payment_gateway_val['kbz_gateway_name'] ?>" size="30" class="required"/></td>
                  

                </tr>



                <tr>

                    <td><input type="checkbox" value="poa" name="poa" id="poa"<?php echo ($payment_gateway_val['poa_enabled']) ? 'checked="checked"' : ''; ?> /></td>

                    <td><strong>Manual:</strong></td>

                    <td><input type="text"  name="poa_title" id="poa_title" value="<?php echo $payment_gateway_val['poa_gateway_name'] ?>" class="required" size="30"/></td>

                    <td></td>

                </tr>

                <tr>

                    <td><input type="checkbox" value="cc" name="cc" id="cc" <?php echo ($payment_gateway_val['cc_enabled']) ? 'checked="checked"' : ''; ?> /></td>

                    <td><strong>Credit Card:</strong></td>

                    <td><input type="text"  name="cc_title" id="cc_title" value="<?php echo $payment_gateway_val['cc_gateway_name'] ?>" class="required" size="30"/></td>

                    <td></td>

                </tr>





                <tr>



                    <td colspan="2"></td>

                    <td colspan="2"><input type="hidden" name="act_sbmt" value="1" /><input type="submit" value="Update" class="button-primary" /></td>

                </tr>

            </tbody>

        </table>

    </form>

</div>

<script type="text/javascript">

    jQuery().ready(function() {

        jQuery("#form1").validate();		

    });
</script>