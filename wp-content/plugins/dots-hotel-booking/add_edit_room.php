<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

if(isset($_POST['submitRoom'])){
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->add_edit_room();
	header("location:admin.php?page=Room-Manager");	
	exit;
}
include("includes/conf.class.php");
include("includes/admin.class.php");
if(isset($_GET['rid']) && $_GET['rid'] != ""){
	$rid = $bsiCore->ClearInput($_GET['rid']);
	$cid = $bsiCore->ClearInput($_GET['cid']);
	if($rid != 0 && $cid != 0){
		$row = mysql_fetch_assoc(mysql_query($bsiAdminMain->getRoomsql($rid, $cid))); 
		$rowrt = mysql_fetch_assoc(mysql_query($bsiAdminMain->getRoomtypesql($row['roomtype_id'])));
		$rowca = mysql_fetch_assoc(mysql_query($bsiAdminMain->getCapacitysql($row['capacity_id'])));
		$roomtypeCombo = $rowrt['type_name'].'<input type="hidden" name="roomtype_id" value="'.$row['roomtype_id'].'" />';
                $capacityCombo = $bsiAdminMain->generateCapacitycombo();
//		$capacityCombo = $rowca['title'].'<input type="hidden" name="capacity_id" value="'.$row['capacity_id'].'" />';
	}else{
		$row = NULL;
		$roomtypeCombo = $bsiAdminMain->generateRoomtypecombo();
		$capacityCombo = $bsiAdminMain->generateCapacitycombo();
	}
}else{
	header("location:admin.php?page=Room-Manager");
	exit;
}
?>        
      <div id="container-inside" class="wrap">
          <h2>Room Add / Edit</h2>
<input type="button" value="Back to Room List" class="button-secondary" onClick="window.location.href='admin.php?page=Room-Manager'" style="background: #EFEFEF; float:right"/>
     <hr style="margin-top:10px;" />
        <form action="<?php echo admin_url('admin.php?page=aad-new-room&noheader=true'); ?>" method="post" id="form1"> 
          <table class="widefat" cellpadding="5" cellspacing="2" border="0"> 
            <tr>
              <td width="200px"><strong>No Of Room:</strong></td> 
              <td valign="middle">
                  <input type="text" name="no_of_room" id="no_of_room" class="required digits" value="<?php echo $row['NoOfRoom']?>" style="width:50px;" /> &nbsp;&nbsp;EXAMPLE: 1, 2</td>
                  <input type="hidden" name="pre_room_cnt" value="<?php echo $row['NoOfRoom']?>" />
            </tr>
            <tr>
              <td><strong>Room Type:</strong></td>
              <td><?php echo $roomtypeCombo;?></td>
            </tr>
          
            <tr>
              <td><strong>No of Adult:</strong></td>
              <td><?php echo $capacityCombo;?></td>
            </tr>
            <!--  <tr>
              <td><strong>Max Child / Room:</strong></td>
              <td><input type="text" name="child_per_room" value="<?php //echo $row['no_of_child']; ?>" style="width:40px;"/> (leave blank if None!)</td>
            </tr> -->
            <tr>
              <td><strong>Extra Bed / Room:</strong></td>
              <td><input type="text" name="extrabed_per_room" value="<?php echo $row['extra_bed']; ?>" style="width:40px;"/> (leave blank if None!)</td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Submit" name="submitRoom" class="button-primary" /></td>
            </tr>
          </table>
        </form>
      </div>
<script type="text/javascript">
	jQuery().ready(function() {
		jQuery("#form1").validate();	
     });        
</script>