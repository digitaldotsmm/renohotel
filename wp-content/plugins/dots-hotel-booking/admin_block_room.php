<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if((isset($_GET['action'])) && ($_GET['action'] == "unblock")){

	include("includes/db.conn.php");

	include("includes/conf.class.php");

	$booking_id  = $bsiCore->ClearInput($_GET['bid']);

	$roomtype_id = $bsiCore->ClearInput($_GET['rti']);

	$capacity_id = $bsiCore->ClearInput($_GET['cid']);

	$result      = mysql_query("SELECT count(*) from dots_reservation br, dots_bookings bb, dots_roomtype brt where br.bookings_id=".$booking_id." and

						 bb.is_block=1 and br.room_type_id=brt.roomtype_id group by br.room_type_id");

	$num=mysql_num_rows($result);

	if($num<=1){

		mysql_query("delete from dots_reservation where bookings_id=".$booking_id." and room_type_id=".$roomtype_id);

		mysql_query("delete from dots_bookings where booking_id=".$booking_id."");
                
                mysql_query("delete from dots_available_room where booking_id=".$booking_id."");

	}else{
                
		mysql_query("delete from dots_reservation where bookings_id=".$booking_id." and room_type_id=".$roomtype_id);
                mysql_query("delete from dots_available_room where booking_id=".$booking_id."");

	}

	header("location:admin.php?page=admin-block-room");

	exit;

}



include("includes/conf.class.php");

include("includes/admin.class.php");
wp_enqueue_script( 'custom_script10', HOTELBOOKING_MANAGER_URL .'js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', HOTELBOOKING_MANAGER_URL .'js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', HOTELBOOKING_MANAGER_URL .'css/data.table.css');
wp_enqueue_style('custom-style10', HOTELBOOKING_MANAGER_URL .'css/jqueryui.css');
?>



<div id="container-inside" class="wrap">

    <h2>ROOM BLOCK LIST</h2>

    <input type="button" value=" Click Here To Block Room" onClick="window.location.href='admin.php?page=block-room'" class="button-primary" />

  <hr style="margin-top:10px;" />

  <table class="display datatable" border="0">

    <thead>

      <tr>

        <th>Description/Name</th>

        <th>Date Range</th>

        <th>Roomtype(capacity)</th>

        <th>Total Room</th>

        <th></th>

      </tr>

    </thead>

    <?php echo $bsiAdminMain->getBlockRoomDetails();?>

  </table>

</div>

<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'desc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );

</script>



