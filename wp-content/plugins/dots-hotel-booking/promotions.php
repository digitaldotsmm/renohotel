<?php
if (!defined('HOTELBOOKING_VERSION')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

if ( $_REQUEST['page'] == 'promotions' && !empty($_REQUEST['action'])) {
  
    // form submit
    if ( !empty($_POST['promo_submit']) ){
       
        // action add_new
        if( $_REQUEST['action'] == 'add_new' ){
                $form_data = array(
                    'name'          => $_POST['promo_name'],
                    'start_date'    => date('Y-m-d', strtotime(str_replace('/', '-', $_POST['promo_start_date']))),
                    'end_date'      => date('Y-m-d', strtotime(str_replace('/', '-', $_POST['promo_end_date']))),
                    'type'          => $_POST['promo_type'],
                    'value'         => $_POST['promo_value'],
                    'roomtype_id'   => $_POST['promo_room_type'],
                    'is_active'     => $_POST['promo_status']
                );

                // save data
                $saved_post_id = save_promtion($form_data);

                if ($saved_post_id) {

                    echo "<div><h2 style='padding: 15px 15px;color: green;border: 1px solid green;display: inline-block;background: #c7fab2;'> Your Promtion is successfully saved. </h2></div>";

                    echo '<script> setTimeout(function(){window.location.replace("/wp-admin/admin.php?page=promotions"); }, 1000);</script>';

                    exit();
                }
           
         // action edit
        }elseif( $_REQUEST['action'] == 'edit' ){    
            
            $form_data = array(
                    'promotion_id'  => $_REQUEST['promo_id'],
                    'name'          => $_POST['promo_name'],
                    'start_date'    => date('Y-m-d', strtotime(str_replace('/', '-', $_POST['promo_start_date']))),
                    'end_date'      => date('Y-m-d', strtotime(str_replace('/', '-', $_POST['promo_end_date']))),
                    'type'          => $_POST['promo_type'],
                    'value'         => $_POST['promo_value'],
                    'roomtype_id'   => $_POST['promo_room_type'],
                    'is_active'     => $_POST['promo_status']                    
                );
           
            //update data
            $response = update_promotion($form_data);           
           
            if ($response) {

                   echo "<div><h2 style='padding: 15px 15px;color: green;border: 1px solid green;display: inline-block;background: #c7fab2;'> Your Promtion is successfully Updated. </h2></div>";

                   echo '<script> setTimeout(function(){window.location.replace("/wp-admin/admin.php?page=promotions"); }, 1000);</script>';

                   exit();
               }    
    
        }
    }   
    
    // enqueue scripts
    wp_enqueue_script('jquery-ui-datepicker');
    
    //Default label
    $form_title = 'Add New Promotion';
    $btn_text = 'Create Promotion';
    
    //Default Form Data
    $promotion = array(
        'name'        => '',
        'start_date'  => '',
        'end_date'    => '',
        'roomtype_id' => '',
        'type'        => '',
        'value'       => '',
        'is_active'   => '1'
    );    
    
    
    // Populate Form for edit
    if( $_REQUEST['action'] == 'edit' ){
        //Label for edit
        $form_title = 'Edit Promotion';
        $btn_text = 'Save Changes';
        
        // get promotion data
        $result= get_promotion_by_id($_REQUEST['promo_id']);  
        $result[0]['start_date'] = date('d/m/Y', strtotime($result[0]['start_date']));
        $result[0]['end_date'] = date('d/m/Y', strtotime($result[0]['end_date']));        
        $promotion = $result[0];                  
        
    // Delete promotion    
    }elseif( $_REQUEST['action'] == 'delete' ){   
            
            // Delet data
            $success = delete_promotion( $_REQUEST['promo_id'] );
            
            if ($success) {

                   echo "<div><h2 style='padding: 15px 15px;color: green;border: 1px solid green;display: inline-block;background: #c7fab2;'> Your Promtion is successfully Removed. </h2></div>";

                   echo '<script> setTimeout(function(){window.location.replace("/wp-admin/admin.php?page=promotions"); }, 1000);</script>';

                   exit();
            }
    }

    

    ?>              
    <div class ="wrap">
        <h2><?php echo $form_title; ?></h2>
    </div>

    <div class="add-new-promotion-wrapper" style="width: 100%;">
        <form id="add-new-promo" action="" method="POST" style="min-width: 320px;">
            <ul>
                <li style="margin-bottom: 15px;">
                    <label>Promotion Name</label><br/>
                    <input type="text" id="promo-name" name="promo_name" class="required" value="<?php echo $promotion['name']; ?>"/>                            
                </li>
                <li style="margin-bottom: 15px;">
                    <label>Start Date</label><br/>
                    <input type="text" id="promo-start-date" name="promo_start_date" class="date required" value="<?php echo $promotion['start_date']; ?>"/>                                                    
                </li>
                <li style="margin-bottom: 15px;">
                    <label>End Date</label><br/>
                    <input type="text" id="promo-end-date" name="promo_end_date" class="date required" value="<?php echo $promotion['end_date']; ?>"/>                            
                </li>
                <li style="margin-bottom: 15px;">
                    <label>Room Type</label><br/>
                    <?php $room_types = get_all_room_type(); ?>
                    <select id="promo-room-type" name="promo_room_type">
                        <?php foreach ($room_types as $room_type) : ?>                        
                            <option value="<?php echo $room_type->roomtype_ID; ?>" <?php echo ($promotion['roomtype_id'] == $room_type->roomtype_ID ) ? 'selected' : ''; ?>><?php echo $room_type->type_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
                <li style="margin-bottom: 15px;">
                    <label>Promotion Type</label><br/>                            
                    <select id="promo-type" name="promo_type">                                
                        <option value="percentage" <?php echo ($promotion['type'] == 'percentage' ) ? 'selected' : ''; ?>>Percentage</option>
                        <option value="fixed" <?php echo ($promotion['type'] == 'fixed' ) ? 'selected' : ''; ?>>Fixed</option>                                
                    </select>                     
                </li>
                <li>
                    <label>Promotion Value (<i id="notice" style="color:#dd382d; font-weight: bold;">Please don't include "%" sign.</i>)</label><br/>                            
                    <input type="text" id="promo-value" class="required" name="promo_value" value="<?php echo $promotion['value']; ?>"/>                                
                </li>
                 <li style="margin-bottom: 30px;">
                    <label>Status</label><br/>                            
                    <input type="radio" id="promo-status" name="promo_status" value="1" <?php echo ($promotion['is_active']) ? 'checked' : ''; ?>/>Activate                                
                    <input style="margin-left: 5px;" type="radio" id="promo-status" name="promo_status" value="0"  <?php echo ( !$promotion['is_active']) ? 'checked' : ''; ?>/>De-activate                               
                </li>
                <li>                            
                    <input type="submit" class="button-primary" id="promo-submit" name="promo_submit" value="<?php echo $btn_text; ?>"/>                                
                    <input type="hidden" name="action" value="<?php echo $_REQUEST['action']; ?>" />
                </li>
            </ul>                
        </form>
    </div>
    <script>
        jQuery(document).ready(function(){
            var promoForm = jQuery('#add-new-promo');
            if ( promoForm.length > 0 ){
                       
                //add date picker
                promoForm.find('#promo-start-date').datepicker({  
                    minDate: 0,
                    maxDate: null,
                    dateFormat: "dd/mm/yy",
                    onSelect: function(selected) {

                        var date = jQuery(this).datepicker('getDate');

                        if(date){
                            date.setDate(date.getDate() + 1 );
                        }

                        jQuery("#promo-end-date").datepicker("option","minDate", date);
                    }
                            
                });                    
                        
                promoForm.find('#promo-end-date').datepicker({                             
                    minDate:  0,
                    maxDate: null,
                    dateFormat: "dd/mm/yy"                        
                }); 
                        
                        
                //form submit
                promoForm.on('submit', function(){    
                    var valid = true; 
                    //validation                       
                    jQuery(this).find('.required').each(function(){
                               
                        // reset invalid                            
                        jQuery(this).css('border', 'none');
                                
                        if( jQuery(this).val() == '' ){
                            jQuery(this).css('border', '1px solid #d4250d');
                            valid = false;
                        }
                    });                      
                           
                    return valid;
                           
                });
                       
                //promo value label description change
                promoForm.find('#promo-type').on('change', function(){
                    if(jQuery(this).val() == 'percentage'){
                        jQuery('#notice').text('Please don\'t include "%" sign.');
                    }else{
                        jQuery('#notice').text('Please don\'t include "." or "," sign.');
                    }
                });
            }
        });
               
    </script>
    <?php
} else {

    global $wpdb;
    $promotions = get_all_promotions();
    ?>

    <div class="wrap">
        <h2>
            Promotions
            <a href="admin.php?page=promotions&action=add_new" class="button-primary">Add New Promotion</a>
        </h2>
    </div>
    <table class="wp-list-table widefat fixed striped posts">
        <thead>
            <tr>                    
                <th scope="col" id="title" class="manage-column column-title desc" style=""><span>Promotion Name</span></th>
                <th scope="col" id="daterange" class="manage-column column-daterange" style="">Date Range</th>
                <th scope="col" id="roomtype" class="manage-column column-roomtype" style="">Room Type</th>
                <th scope="col" id="promotype" class="manage-column column-promotype" style="">Promotion Type</th>
                <th scope="col" id="promovalue" class="manage-column column-promovalue" style="">Discount Value</th>                    
                <th scope="col" id="promovalue" class="manage-column column-promovalue" style="">Status</th>                    
                <th scope="col" id="promoaction" class="manage-column column-promoaction" style=""></th>                    
            </tr>
        </thead>

        <tbody id="the-list">                
            <?php foreach ($promotions as $promotion): ?>
                <tr>
                    <td> <?php echo $promotion->name; ?> </td>

                    <?php $daterange = date('d/m/Y', strtotime($promotion->start_date)) . ' - ' . date('d/m/Y', strtotime($promotion->end_date)); ?>
                    <td> <?php echo $daterange; ?> </td>

                    <?php
                    $room_name = get_room_type_name($promotion->roomtype_id);
                    ?>
                    <td> <?php echo $room_name; ?> </td>

                    <td> <?php echo $promotion->type; ?> </td>

                    <td> <?php echo $promotion->value; ?><?php echo ( $promotion->type == 'percentage' ) ? '%' : ''; ?> </td>
                    
                    <td> <?php echo ( $promotion->is_active ) ? '<span style="color:green;">Active</span>' :  '<span style="color:#d4250d;">Deactive</span>'; ?> </td>

                    <td> <a href="/wp-admin/admin.php?page=promotions&action=edit&promo_id=<?php echo $promotion->promotion_id; ?>">Edit</a> / 
                         <a href="/wp-admin/admin.php?page=promotions&action=delete&promo_id=<?php echo $promotion->promotion_id; ?>">Delete</a>
                    </td>
                </tr>                
            <?php endforeach; ?>
        </tbody>

        <tfoot>
            <tr>
                <th scope="col" class="manage-column column-title desc" style=""><span>Promotion Name</span></th>
                <th scope="col" class="manage-column column-daterange" style="">Date Range</th>
                <th scope="col" class="manage-column column-roomtype" style="">Room Type</th>
                <th scope="col" class="manage-column column-promotype" style="">Promotion Type</th>
                <th scope="col" class="manage-column column-promovalue" style="">Discount Value</th>           
                <th scope="col" class="manage-column column-promovalue" style="">Status</th>           
                <th scope="col" class="manage-column column-promoaction" style=""></th> 
            </tr>
        </tfoot>

    </table>


    <?php
}

function get_all_promotions() {
    global $wpdb;
    return $wpdb->get_results('SELECT * FROM dots_promotions');
}

function get_all_room_type() {
    global $wpdb;
    return $wpdb->get_results('SELECT * FROM dots_roomtype');
}

function get_room_type_name($roomtype_ID) {
    global $wpdb;
    return $wpdb->get_var("SELECT type_name FROM dots_roomtype WHERE roomtype_ID = $roomtype_ID");
}

function save_promtion($data) {
    global $wpdb;
    $wpdb->insert('dots_promotions', $data);
    return $wpdb->insert_id;
}

function update_promotion($data){
    global $wpdb;
    return $wpdb->replace('dots_promotions', $data);   
}

function get_promotion_by_id($promotion_ID){
    global $wpdb;
    return $wpdb->get_results('SELECT * FROM dots_promotions WHERE promotion_id=' . $promotion_ID, ARRAY_A);
}

function delete_promotion($promotion_ID){
    global $wpdb;
    return $wpdb->delete( 'dots_promotions', array( 'promotion_id' => $promotion_ID ) );
}