<?php
session_start();
$bsiAdminHtml = new bsiAdminHtmlTemplate;
class bsiAdminHtmlTemplate{
	function bsi_hotel_details_form()
	{
	return '<h1>Hotel Deatils</h1>
			<hr/>
			<form action="" method="post" id="form1">
			<table cellpadding="5" cellspacing="2" border="0">
			  <tr>
				<td valign="middle"><strong>Hotel Name:</strong></td>
				<td><input type="text" name="hotel_name" class="required" size="50" value="Hotel Booking System"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>Street Address:</strong></td>
				<td><input type="text" name="str_addr" class="required" size="40" value="99 xxxxx Road"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>City:</strong></td>
				<td><input type="text" name="city" size="30" class="required" value="Your City"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>State:</strong></td>
				<td><input type="text" name="state" class="required" size="30" value="Your State"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>Country:</strong></td>
				<td><input type="text" name="country" class="required" size="30" value="USA"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>Zip / Post code:</strong></td>
				<td><input type="text" name="zipcode" class="required" size="10" value="11211"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>Phone Number:</strong></td>
				<td><input type="text" name="phone" class="required" size="15" value="+18778888972"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>Fax:</strong></td>
				<td><input type="text" name="fax" class="" size="15" value="+18778888972"/></td>
			  </tr>
			  <tr>
				<td valign="middle"><strong>Email Id:</strong></td>
				<td><input type="text" name="email" class="required email" size="30" value="sales@bestsoftinc.com"/></td>
			  </tr>
			  <tr>
				<td></td>
				<td><input type="submit" value="Submit" name="sbt_details" id="sbt_details"  style="background:#e5f9bb; cursor:pointer; cursor:hand;" /></td>
			  </tr>
			</table>
		  </form>';
		}
	function Room_list()
	{ include('admin.class.php'); ?>
	
	<div id="container-inside">

<span style="font-size:16px; font-weight:bold">Room List</span><span style="font-size:13px; color:#F00; padding-left:200px;">
<?php if(isset($_SESSION['msg_exists'])){ echo $_SESSION['msg_exists']; }



unset($_SESSION['msg_exists']);?></span>



    <input type="button" value="Add New Room" onClick="window.location.href='admin.php?page=add_room&rid=0&cid=0'" style="background: #EFEFEF; float:right"/>

 <hr />

  <table class="display datatable" border="0">

    <thead>

      <tr>

        <th>Room Type</th>

        <th>Adult / Room</th>

        <th>Total Room</th>

        <th>&nbsp;</th>

      </tr>

    </thead>

    <?php echo $bsiAdminMain->generateRoomListHtml()?>

  </table>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/DataTables/jquery.dataTables.js"></script> 

<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/bsi_datatables.js"></script>

<link href="<?php echo plugins_url()?>/bsihotel/css/data.table.css" rel="stylesheet" type="text/css" />

<link href="<?php echo plugins_url()?>/bsihotel/css/jqueryui.css" rel="stylesheet" type="text/css" />
	
<?php	}

function RoomType_list()
     { include('admin.class.php'); ?>
    <div id="container-inside">

 <span style="font-size:16px; font-weight:bold">RoomType List</span>

    <input type="button" value="Add New Roomtype" onClick="window.location.href='admin.php?page=add_room_type&id=0'" style="background: #EFEFEF; float:right"/>

 <hr />

  <table class="display datatable" border="0">

    <thead>

      <tr>

        <th width="30%">RoomType Name</th>

        <th>&nbsp;</th>

      </tr>

    </thead>

    <?php echo $bsiAdminMain->generateRoomtypeListHtml()?>

  </table>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/DataTables/jquery.dataTables.js"></script> 

<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/bsi_datatables.js"></script>

<link href="<?php echo plugins_url()?>/bsihotel/css/data.table.css" rel="stylesheet" type="text/css" />

<link href="<?php echo plugins_url()?>/bsihotel/css/jqueryui.css" rel="stylesheet" type="text/css" />
<?php }

function Capacity()
     { include('admin.class.php');?>
	
	<div id="container-inside">

<span style="font-size:16px; font-weight:bold">Capacity List</span>

    <input type="button" value="Add new Capacity" onClick="window.location.href='admin.php?page=add_capacity&id=0'" style="background: #EFEFEF; float:right"/>

 <hr />

  <table class="display datatable" border="0">

    <thead>

      <tr>

        <th width="20%">Capacity Title</th>

        <th width="20%">No of Adult</th>

        <th>&nbsp;</th>

      </tr>

    </thead>

    <?php echo $bsiAdminMain->generateCapacityListHtml()?>

  </table>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/DataTables/jquery.dataTables.js"></script> 

<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/bsi_datatables.js"></script>

<link href="<?php echo plugins_url()?>/bsihotel/css/data.table.css" rel="stylesheet" type="text/css" />

<link href="<?php echo plugins_url()?>/bsihotel/css/jqueryui.css" rel="stylesheet" type="text/css" />
<?php
 }

function Lookup()
     {?>	
	
    <div id="container-inside">

       <span style="font-size:16px; font-weight:bold">Customer List</span>

      <hr />

        <table class="display datatable" border="0">

          <thead>

	<tr><th width="18%" nowrap="nowrap">Guset Name</th><th width="27%" nowrap="nowrap">Street Address</th><th width="15%" nowrap="nowrap">Phone Number</th><th width="25%" nowrap="nowrap">Email Id</th><th width="15%" nowrap="nowrap">&nbsp;</th></tr>

 </thead>

 <tbody id="getcustomerHtml">

 	<?php echo $html?>

 </tbody>

        </table>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/DataTables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo plugins_url()?>/bsihotel/js/bsi_datatables.js"></script>
<link href="<?php echo plugins_url()?>/bsihotel/css/data.table.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url()?>/bsihotel/css/jqueryui.css" rel="stylesheet" type="text/css" />
<?php }





} ?>

