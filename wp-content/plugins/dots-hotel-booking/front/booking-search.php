<?php
//if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
//	header( 'Status: 403 Forbidden' );
//	header( 'HTTP/1.1 403 Forbidden' );
//	exit();
//}
include_once( HOTELBOOKING_MANAGER_PATH . "/front/includes/db.conn.php" );
include("includes/conf.class.php");
$bsiCore = new bsiHotelCore;
include("languages/" . $bsiCore->config['conf_language']);
include("includes/search.class.php");
$bsisearch = new bsiSearch();
//$bsiCore->clearExpiredBookings();
$pos2 = strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']);
if ($bsisearch->nightCount == 0 and !$pos2) {
    //header('Location: booking-failure.php?error_code=9');
    echo "<script>
  alert(" . DIRECT_ACCESS_TO_THIS_PAGE_RESTRICTED_ALERT_TEXT . ")
</script>";
    die;
}
wp_enqueue_style('jquery-style1', HOTELBOOKING_MANAGER_URL . 'front/css/jquery.lightbox-0.5.css');
wp_enqueue_script('custom_script1', HOTELBOOKING_MANAGER_URL . 'front/js/hotelvalidation.js');
wp_enqueue_script('custom_script2', HOTELBOOKING_MANAGER_URL . 'front/js/jquery.lightbox-0.5.min.js');

if (file_exists(TEMPLATEPATH . '/includes/hotel-search-form.php')):
    include_once( TEMPLATEPATH . '/includes/hotel-search-form.php');
else :
    ?>
    <div id="wrapper">
        <h2 align="left"  style="padding-left:5px;"><?php echo SEARCH_INPUT_TEXT; ?></h2>
        <table cellpadding="0"  cellspacing="7" border="0" align="left" style="text-align:left;">
            <tr>
                <td><strong><?php echo CHECK_IN_D_TEXT; ?>:</strong></td>
                <td><?php echo $bsisearch->checkInDate; ?></td>
            </tr>
            <tr>
                <td><strong><?php echo CHECK_OUT_D_TEXT; ?>:</strong></td>
                <td><?php echo $bsisearch->checkOutDate; ?></td>
            </tr>
            <tr>
                <td><strong><?php echo TOTAL_NIGHTS_TEXT; ?>:</strong></td>
                <td><?php echo $bsisearch->nightCount; ?></td>
            </tr>
            <tr>
                <td><strong><?php echo ADULT_ROOM_TEXT; ?>:</strong></td>
                <td><?php echo $bsisearch->guestsPerRoom; ?></td>
            </tr>
            <?php if ($bsisearch->childPerRoom) { ?>
                <tr>
                    <td><strong><?php echo CHILD_PER_ROOM_TEXT; ?>:</strong></td>
                    <td><?php echo $bsisearch->childPerRoom; ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php endif; ?>
<br />
<br />
<div id="book_searchwrapper">
    <h2 align="left"  style="color: #B53E04;"><?php echo SEARCH_RESULT_TEXT; ?></h2>
    <p>Select number of rooms below for booking.</p>
    <form name="searchresult" id="searchresult" method="post" action="" onSubmit="return validateSearchResultForm('<?php echo SELECT_ONE_ROOM_ALERT; ?>');">
        <?php
        $gotSearchResult = false;
        $idgenrator = 0;
        $ik = 1;
        
        $roomTypeID_list = explode(",", $bsisearch->roomTypeID);
        
        foreach ($bsisearch->roomType as $room_type) {            
//            if (!empty($bsisearch->roomTypeID) && $bsisearch->roomTypeID > 0) { //if book for specific room, we don't need to loop all room types               
//                if ($room_type['rtid'] != $bsisearch->roomTypeID){                                       
//                    continue; //not what we are looking for, just skip it
//                }
//            }
            
            if ( $roomTypeID_list != null ) { //if book for specific room, we don't need to loop all room types               
                if ( !in_array($room_type['rtid'], $roomTypeID_list) ){                                       
                    continue; //not what we are looking for, just skip it
                }
            }
         
            foreach ($bsisearch->multiCapacity as $capid => $capvalues) {
              
                $room_result = $bsisearch->getAvailableRooms($room_type['rtid'], $room_type['rtname'], $capid);
                $total_extra_bed = $room_result['total_extrabed'];                
                if (intval($room_result['roomcnt']) > 0) {
                    $gotSearchResult = true;
                    echo '<script> jQuery(document).ready(function() { ';
                    echo 'jQuery("#' . str_replace(" ", "", $room_type['rtid']) . '_' . str_replace(" ", "", $capid) . $ik . '").click(function () {
                                jQuery("#' . str_replace(" ", "", $room_type['rtid']) . '_' . str_replace(" ", "", $capid) . '").slideToggle("slow");
                                if(jQuery("a#' . str_replace(" ", "", $room_type['rtid']) . '_' . str_replace(" ", "", $capid) . $ik . '").html()=="' . VIEW_PRICE_DETAILS_TEXT . '"){
                                  jQuery("a#' . str_replace(" ", "", $room_type['rtid']) . '_' . str_replace(" ", "", $capid) . $ik . '").html("' . HIDE_PRICE_DETAILS_TEXT . '")
                                }else{
                                  jQuery("a#' . str_replace(" ", "", $room_type['rtid']) . '_' . str_replace(" ", "", $capid) . $ik . '").html("' . VIEW_PRICE_DETAILS_TEXT . '")
                                }
                              });
';
                    echo '}); </script>';
                    echo '<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery("#mySlides_' . $capid . '_' . $room_type['rtid'] . ' a").lightBox();
});
</script>';
                    ?>
                    <div id="sresultwrapper" class="row">
                       
                        <?php
                        $rpost_id = $room_type['rtpostid'];
                        $rtypeid=$room_type['rtid'];
                        $roomtypename=$bsisearch->getroomtypename($rtypeid);
                        $room = get_post($rpost_id);
                        $thumb = get_post_thumbnail_id($rpost_id);
                        $img_url = wp_get_attachment_url($thumb, 'large'); //get full URL to image (use "large" or "medium" if the images too big)
                        $image = aq_resize($img_url, 500, 286, true); //resize & crop the image
                        $permalink = get_permalink($rpost_id);
                        $capname=$bsisearch->getcapicity($capid);
                        ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="<?php echo $permalink; ?>"><img class="img-polaroid img-border img-responsive" src="<?php echo $image; ?>" /></a>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12" id="roomdes">
                            <a href="<?php echo $permalink; ?>"><h2 class="lined-heading"><span><?php echo $roomtypename; ?></span></h2></a>
                            <p><?php echo apply_filters('the_content', $room->post_content);//strip_tags(substr($room->post_content, 0, 300)); ?></p>
                         
                            <table>
                                <tr>
                                    <td style="vertical-align:top; " >
                                        <table cellpadding="3" cellspacing="2" border="0" width="100%" bgcolor="#FFFFFF" style=" margin:0px 0px 0px 0px !important;" >

<!--                                            <tr>
                                                <td><strong><?php //echo MAX_OCCUPENCY_TEXT; ?></strong></td>
                                                <td><?php //echo $capvalues['capval'] ?>  <?php //echo ADULT_TEXT; ?> 
                                                   
            <?php //if ($room_result['child_flag']) { ?> <br /> <?php// echo $bsisearch->childPerRoom; ?> <?php //echo CHILD_TEXT; ?> <?php //} ?></td>
                                            </tr>-->
                                            <tr>
                                                <td style=" width:180px;font-size:14px;" nowrap="nowrap"><strong><?php echo SELECT_NUMBER_OF_ROOM_TEXT; ?></strong></td>
                                                <?php 
                                                $extrabedprice=$bsisearch->getextrabedprice($room_type['rtid']);
                                                
                                                ?>
                                                <?php $excludebreakfast=$bsisearch->getexcludebreakfastprice($room_type['rtid']);?>
                                                <td width="90">
                                                    <select name="svars_selectedrooms[]" class="slectbox form-control reservation-label" id="svar_selectedroom" data-extrabedprice="<?php echo round($extrabedprice); ?>">
                                                        <?php echo $room_result['roomdropdown'] ?>
                                                    </select>
                                                </td>
                                                <td style="width:200px;font-size:13px;padding-left:10px;">
                                                    <strong> Select Number of Extra Bed </strong>  
                                                </td>
                                                <td width="90">
                                                    
                                                    <select name="extra_bed[]" class="slectbox form-control reservation-label" id="svar_selectedroom" >
                                                        <?php 
                                                        $extra_bed = range(0, (int)$total_extra_bed);
                                                        foreach($extra_bed as $bed):
                                                            echo '<option value='.round($extrabedprice * $bed).'>'.$bed.'</option>';
                                                        endforeach;
                                                        ?>
                                                    </select>                                                    
                                                </td>
                                            </tr>
<!--                                            <tr>
                                                <td class="need_extrabed">Exclude breakfast</td>
                                                
                                                <td id=""><input type="checkbox" name="excludebreakfast" value="<?php //echo $excludebreakfast; ?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="need_extrabed">Need an extra bed?</td>
                                                
                                                <td id="extrabedwrapper"><div class="scrolltd"></div></td>
                                            </tr>-->

                                        </table></td>
                                
                                
<!--                                    <td>
                                        <div class="hidedetailbtn"><i class="btn fa fa-tags"></i>&nbsp;<a href="javascript:;" id="<?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid).$ik?>"><?php echo HIDE_PRICE_DETAILS_TEXT; ?></a></div>
                                    </td>-->
                                </tr>
                               

                            </table>
                        </div>
                    </div><!--end sresultwrapper -->                                       
                    
                    <span id="<?php echo  str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid)?>" id=" <?php echo str_replace(" ","",$room_type['rtid']).'_'.str_replace(" ","",$capid)?>">
                    
                    
                        <div id="detailpriceplan" class="priceplantable">
                            <div class="priceplan">
                                <p><i class="fa fa-calendar"></i>&nbsp;DETAIL PRICE PLAN</p>
                                <!-- <p style="float: right; color: #fff; background: #0077A2; margin-right: 100px;">Room Price ($)</p> -->
                            </div>
                            <div class="table-responsive">
                                <table class="pricetable table table-striped"cellpadding="0" cellspacing="0" style=" text-align: center;">
                                    <?php echo $room_result['pricedetails'] ?>
                                </table>
                            </div>
                            

                        </div><!-- end detail price plan -->
                    </span>                    
                    <?php
                }
            }
        }
        
        ?>
                    <br />
                    <p class="policy">Please ensure that you have read & understood the <a href="<?php echo WP_HOME; ?>/terms-conditions/" target="_blank">Cancellation & Refund Policies</a> </p>
                    <?php

        if ($gotSearchResult) {
            echo '<div id="" style=""><table cellpadding="5" cellspacing="0" border="0" width="100%" >';
            echo '<tr><td align="center" style="padding-right:30px;"><button id="registerButton" name="registerButton" type="submit" style="background: #c9a205; margin-top: 30px; color: #fff; padding: 8px 20px;border-color: #c9a205;">' . CONTINUE_TEXT . '</button></td></tr>';
            echo '</table></div>';
        } else {
            echo '<table cellpadding="4" cellspacing="0" width="100%"><tbody><tr><td style="font-size:13px; color:#F00;" align="center"><br /><br />';
            if ($bsisearch->searchCode == "SEARCH_ENGINE_TURN_OFF") {
                echo SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_PLEASE_TRY_LATER;
            } else if ($bsisearch->searchCode == "OUT_BEFORE_IN") {
                echo SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_PLEASE_TRY_WITH_INVALID_SEARCHING_CRITERIA;
            } else if ($bsisearch->searchCode == "NOT_MINNIMUM_NIGHT") {
                echo MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT . ' ' . $bsiCore->config['conf_min_night_booking'] . ' ' . PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT;
            } else if ($bsisearch->searchCode == "TIME_ZONE_MISMATCH") {
                $tempdate = date("l F j, Y G:i:s T");
                echo BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT . ' ' . $bsisearch->checkInDate . ' ' . PLEASE_MODIFY_YOUR_SEARCH_CRITERIA_ACCORDING_TO_HOTELS_DATE_TIME_TEXT . ' <br> ' . HOTELS_CURRENT_DATE_TIME_TEXT . ' ' . $tempdate;
            } else {
                echo SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_PLEASE_TRY_WITH_DIFFERENT_DATE_SLOT_TEXT;
            }
            echo '<br /><br /><br /></td></tr></tbody></table>';
        }
        ?>
    </form>
    <br  />
</div>
