<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
	session_destroy(); 
?>

<div id="wrapper" style="width:400px !important; height:200px;">
<h4 style="text-align:center;"><?php echo THANK_YOU_TEXT; ?>!</h4><br /><h6 style="text-align:center;"><?php echo YOUR_BOOKING_CONFIRMED_TEXT; ?>. <?php echo INVOICE_SENT_EMAIL_ADDRESS_TEXT; ?>.</h6>
</div>