<?php

//if (!defined('HOTELBOOKING_VERSION')) {
//    header('Status: 403 Forbidden');
//    header('HTTP/1.1 403 Forbidden');
//    exit();
//}

class mpu_class {

    var $kbz_url = "";
    var $merchantid = "";
    var $secret_key = "";
    var $last_error;                 // holds the last error encountered
    var $ipn_log;                    // bool: log IPN results to text file?
    var $test_mode = false;
    var $ipn_response;               // holds the IPN response from paypal   
    var $ipn_data = array();         // array contains the POST values for IPN
    var $fields = array();           // array holds the fields to submit to paypal
    var $x_relay_url = "";
    var $amount;
    var $fingerprint;

    function __construct() {

        // initialization constructor.  Called when class is created.
        if (ECOM_DEBUG) {
            $this->mpu_url = "https://www.mpu-ecommerce.com/Payment/Payment/pay";
        } else {
           // $this->mpu_url = 'https://www.mpu-ecommerce.com/Payment/Payment/pay';
            $this->mpu_url = 'https://www.mpu-ecommerce.com/Payment/Payment/pay';
        }

        //$this->mpu_url = "http://122.248.120.252:60145/UAT/Payment/Payment/pay";
        $this->merchantid = "206104000000729";
        $this->secret_key = "CUDQMMHF2VMCXGVUJXTIQ04Z3LIE2KYD";
        $this->last_error = '';

        $this->sequence = rand(1, 1000);
        // a timestamp is generated
        $this->timeStamp = time();

        // an invoice is generated using the date and time
        // a sequence number is randomly generated
        // populate $fields array with a few default values.  See the paypal
        // documentation for a list of fields and their data types. These defaul
        // values can be overwritten  by the calling script.
    }

    function add_field($field, $value) {

        // adds a key=>value pair to the fields array, which is what will be 
        // sent to paypal as POST variables.  If the value is already in the 
        // array, it will be overwritten.

        $this->fields["$field"] = $value;
    }

    function submit_mpu_post() {

        // this function actually generates an entire HTML page consisting of
        // a form with hidden elements which is submitted to authorize.net via the 
        // BODY element's onLoad attribute.  We do this so that you can validate
        // any POST vars from you custom form before submitting to paypal.  So 
        // basically, you'll have your own form which is submitted to your script
        // to validate the data, which in turn calls this function to create
        // another hidden form and submit to paypal.
        // The user will briefly see a message on the screen that reads:
        // "Please wait, your order is being processed..." and then immediately
        // is redirected to paypal.
        ////echo "<body onLoad=\"document.form1.submit();\">\n";
        echo "<html>";
        echo "<head>";
        echo "<script src=\"//cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js\"></script>";
        echo "</head>";
        echo "<body>\n";
        echo "<center>
            <p>&nbsp;</p>
            <p><img src=\"" . ASSET_URL . "images/logo.png\" alt=\"Great Treasure Hotel\"></p>
            <h3>Please wait, your order is being processed...</h3>           
        </center>";
        echo "<form method=\"post\" name=\"form1\" action=\"" . $this->mpu_url . "\" id=\"processForm\">\n";
        foreach ($this->fields as $name => $value) {
            echo "<input type=\"hidden\" name=\"$name\" id=\"$name\" value=\"$value\">";
        }

        echo "</form>\n";

        echo "<script type=\"text/javascript\">
        jQuery(document).ready(function($){
            $(\"#processForm\").submit();
        });
    </script></body></html>";
    }

    function create_signature_string($post) {
        unset($post["hashValue"]);
        //$array = array($post['amount'],$post['invoiceNo'],$currencycode, $productdescription, $post['merchantID'],$post['userDefined1'],$post['userDefined2'],$post['userDefined3']);
        sort($post, SORT_STRING);
        $signature_string = "";

        foreach ($post as $value) {
            if ($value != "") {
                $signature_string .= $value;
            }
        }
        return $signature_string;
    }

    function generate_hash_value($post) {
        $input_fields_array = $post;

        $signature_string = $this->create_signature_string($input_fields_array);
        $hash_value = hash_hmac('sha1', $signature_string, $this->secret_key, false);
        $hash_value = strtoupper($hash_value);

        return $hash_value;
    }

    function is_hash_value_matched($post) {
        var_dump($post);
        $is_matched = false;
        $generated_hash_value = $this->generate_hash_value($post);
        $server_hash_value = $post["hashValue"];
        if ($generated_hash_value == $server_hash_value) {
            $is_matched = true;
        }

        return $is_matched;
    }

}

