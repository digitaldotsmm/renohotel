<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
class authorize_net_class {

    var $anet_url = "";
    var $login_id = "";
    var $transaction_key = "";
    var $last_error;                 // holds the last error encountered
    var $ipn_log;                    // bool: log IPN results to text file?
    var $test_mode = false;
    var $ipn_response;               // holds the IPN response from paypal   
    var $ipn_data = array();         // array contains the POST values for IPN
    var $fields = array();           // array holds the fields to submit to paypal
    var $x_relay_url = "";
    var $amount;
    var $fingerprint;

    function authorize_net_class() {

        // initialization constructor.  Called when class is created.

        $this->anet_url = 'https://secure.authorize.net/gateway/transact.dll';
        $this->login_id = "7Y45Qdr4"; //sandbox log in id
        $this->transaction_key = "5XcN5qc3t66F3gfM"; //sandbox log in id
        $this->last_error = '';

        $this->sequence = rand(1, 1000);
        // a timestamp is generated
        $this->timeStamp = time();

        // an invoice is generated using the date and time
        // a sequence number is randomly generated
        // populate $fields array with a few default values.  See the paypal
        // documentation for a list of fields and their data types. These defaul
        // values can be overwritten  by the calling script.

        $this->add_field('x_login', $this->login_id);
        $this->add_field('x_fp_sequence', $this->sequence);
        $this->add_field('x_fp_timestamp', $this->timeStamp);        
        $this->add_field('x_relay_response', true);
        $this->add_field('x_method', 'CC');
        $this->add_field('x_show_form', 'PAYMENT_FORM');
    }

    function get_fingerprint() {
        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $this->login_id . "^" . $this->sequence . "^" . $this->timeStamp . "^" . $this->amount . "^", $this->transaction_key);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $this->login_id . "^" . $this->sequence . "^" . $this->timeStamp . "^" . $this->amount . "^", $this->transaction_key));
        }
        return $fingerprint;
    }

    function add_field($field, $value) {

        // adds a key=>value pair to the fields array, which is what will be 
        // sent to paypal as POST variables.  If the value is already in the 
        // array, it will be overwritten.

        $this->fields["$field"] = $value;
    }

    function submit_anet_post() {

        // this function actually generates an entire HTML page consisting of
        // a form with hidden elements which is submitted to authorize.net via the 
        // BODY element's onLoad attribute.  We do this so that you can validate
        // any POST vars from you custom form before submitting to paypal.  So 
        // basically, you'll have your own form which is submitted to your script
        // to validate the data, which in turn calls this function to create
        // another hidden form and submit to paypal.
        // The user will briefly see a message on the screen that reads:
        // "Please wait, your order is being processed..." and then immediately
        // is redirected to paypal.
        echo "<html>\n";
        echo "<head><title>Processing Payment...</title></head>\n";
        echo "<body onLoad=\"document.form.submit();\">\n";
        echo "<center><h3>Please wait, your order is being processed...</h3></center>\n";
        echo "<form method=\"post\" name=\"form\" action=\"" . $this->anet_url . "\">\n";
        foreach ($this->fields as $name => $value) {
            echo "<input type=\"hidden\" name=\"$name\" value=\"$value\">";
        }

        echo "</form>\n";
        echo "</body></html>\n";
    }

}

