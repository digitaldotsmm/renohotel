<?php
if (!defined('HOTELBOOKING_VERSION')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}
include_once( HOTELBOOKING_MANAGER_PATH . "/front/includes/db.conn.php" );
include("includes/conf.class.php");
$bsiCore = new bsiHotelCore;
include("languages/" . $bsiCore->config['conf_language']);
$pos2 = strpos($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']);
if (!$pos2) {
    //header('Location: booking-failure.php?error_code=9');
    echo "<script>
  alert(" . DIRECT_ACCESS_TO_THIS_PAGE_RESTRICTED_ALERT_TEXT . ")
</script>";
    die;
}
include("includes/details.class.php");
$bsibooking = new bsiBookingDetails;
//$bsiCore->clearExpiredBookings();
wp_enqueue_script('custom_script5', HOTELBOOKING_MANAGER_URL . 'front/js/jquery.validate.js');

?>



<script type="text/javascript">
    jQuery().ready(function() {
        jQuery("#form1").validate();
    });        
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#btn_exisitng_cust').click(function() {      
            jQuery('#exist_wait').html("<img src='<?php echo HOTELBOOKING_MANAGER_URL; ?>front/images/ajax-loader.gif' border='0'>")
            var querystr = 'action=get_client_detail&actioncode=2&existing_email='+jQuery('#email_addr_existing').val();
     
            //jQuery.post("<?php echo HOTELBOOKING_MANAGER_URL; ?>front/ajaxreq-processor.php", querystr, function(data){
            jQuery.post("<?php echo WP_HOME; ?>/wp-admin/admin-ajax.php", querystr, function(data){
         
                if(data.errorcode == 0){
                    jQuery('#title').html(data.title)
                    jQuery('#fname').val(data.first_name)
                    jQuery('#lname').val(data.surname)
                    jQuery('#str_addr').val(data.street_addr)
                    jQuery('#city').val(data.city)
                    jQuery('#state').val(data.province)
                    jQuery('#zipcode').val(data.zip)
                    jQuery('#country').val(data.country)
                    jQuery('#phone').val(data.phone)
                    jQuery('#fax').val(data.fax)
                    jQuery('#email').val(data.email)
                    jQuery('#exist_wait').html("")
                }else { 
                    alert(data.strmsg);
                    jQuery('#fname').val('')
                    jQuery('#lname').val('')
                    jQuery('#str_addr').val('')
                    jQuery('#city').val('')
                    jQuery('#state').val('')
                    jQuery('#zipcode').val('')
                    jQuery('#country').val('')
                    jQuery('#phone').val('')
                    jQuery('#fax').val('')
                    jQuery('#email').val('')
                    jQuery('#exist_wait').html("")
                }	
            }, "json");
        });
    });
    function myPopup2(booking_id){
        var width = 730;
        var height = 650;
        var left = (screen.width - width)/2;
        var top = (screen.height - height)/2;
        var url='terms-and-services.php?bid='+booking_id;
        var params = 'width='+width+', height='+height;
        params += ', top='+top+', left='+left;
        params += ', directories=no';
        params += ', location=no';
        params += ', menubar=no';
        params += ', resizable=no';
        params += ', scrollbars=yes';
        params += ', status=no';
        params += ', toolbar=no';
        newwin=window.open(url,'Chat', params);
        if (window.focus) {newwin.focus()}
        return false;
    }
</script>


<?php
    $bookingDetails = $bsibooking->generateBookingDetails();
    
    $from = 'USD'; /* change it to your required currencies */
    $to = 'MMK';
    $url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s=' . $from . $to . '=X';
    $handle = @fopen($url, 'r');

    if ($handle) {
        $result = fgets($handle, 4096);
        fclose($handle);
    }
    $allData = explode(',', $result); /* Get all the contents to an array */
    $dollarValue = (float) $allData[1];
    $dollarValue = round($dollarValue);
    
?>
<form method="post" action="" id="form1" class="signupform">
<div id="wrapper" class="booking_details">
    <h2 align="left" style="padding-left:10px; color: #c9a205; margin: 20px 0px;" ><i class="fa fa-book"></i>&nbsp; <?php echo BOOKING_DETAILS_TEXT; ?></h2>
    <div class="table-responsive">
        <table class="table table-striped tbl-booking-details" cellpadding="4" cellspacing="1" border="0" width="100%" bgcolor="#FFFFFF" style="font-size:14px;">
            <tr>
                <td align="center" style="background-color: #c9a205 !important;padding-left:10px; color: #fff;height: 35px;"><strong><?php echo CHECKIN_DATE_TEXT; ?></strong></td>
                <td align="center" style="background-color: #c9a205 !important;color: #fff; padding-left:10px;height: 35px;"><strong><?php echo CHECKOUT_DATE_TEXT; ?></strong></td>
                <td align="center" style="background-color: #c9a205 !important;color: #fff; padding-left:10px;height: 35px;"><strong><?php echo TOTAL_NIGHT_TEXT; ?></strong></td>
                <td align="center" style="background-color: #c9a205 !important;color: #fff; padding-left:10px;height: 35px;"></td>
                <td align="center" style="background-color: #c9a205 !important;color: #fff; padding-left:10px;height: 35px;"><strong><?php echo TOTAL_ROOMS_TEXT; ?></strong></td>
            </tr>
            <tr>
                <td align="center" bgcolor="" style="padding-left:10px; color: #572d03;height: 30px;"><?php echo $bsibooking->checkInDate ?></td>
                <td align="center" bgcolor="" style="color: #000;height: 30px;"><?php echo $bsibooking->checkOutDate ?></td>
                <td align="center" bgcolor="" style="color: #000;height: 30px;"><?php echo $bsibooking->nightCount ?></td>
                <td align="center" bgcolor="" style="color: #000;height: 30px;"></td>
                <td align="center" bgcolor="" style="color: #000;height: 30px;"><?php echo $bsibooking->totalRoomCount ?></td>
            </tr>
<!--            <tr style="height: 30px;"></tr>-->
            
            
            <!--</table>-->
            <!--</div>-->

<!--<table style="border: 1px solid #5c2e91; width: 100%;">-->
            <?php
            $roomType = "";
            foreach ($bookingDetails as $bookings) {                
                $extrabedprice=(float)$bsibooking->getextrabedprice($bookings['roomtypeid']);                
                if($extrabedprice){
                    $extrabed_count = $bookings['extrabedmoney']/$extrabedprice;                    
                }
                // $mmk = $bookings['grosstotal'] * $dollarValue;
                $child_title2 = ($bookings['child_flag2']) ? " + " . $bookings['childcount3'] . " Child " : "";
                echo '<tr>';
                echo '<td align="center" style="padding-left:35px;">' . $bookings['roomno'] . '</td>';
                echo '<td align="center" style="padding-left:10px;">' . $bookings['roomtype'] . '</td>';
                $roomType = $bookings['roomtype'];
                echo '<td></td>';
                echo '<td align="right" style="padding-left:0px;"><strong>Price</strong></td>';
                echo '<td align="right" style="padding-right:5px; font-weight: bold;">' . $bsiCore->config['conf_currency_symbol'] . number_format($bookings['grosstotal'] - $bookings['extrabedmoney'], 2, '.', ',') . '</td>';                
                echo '</tr>';
                
                if (isset($bookings['extrabedcount']) && $extrabed_count != 0) { 
                 echo '<tr>';
                 echo '<td></td>';
                 echo '<td></td>';
                 echo '<td></td>';
                 echo '<td align="right" style="padding-left:0px;"><strong>Extra Bed ( ' . $bsiCore->config['conf_currency_symbol'] . number_format($extrabedprice, 2, '.', ',').' each ) x '.$extrabed_count . '</strong></td>';
                 echo '<td align="right" style="padding-right:5px; font-weight: bold;">' . $bsiCore->config['conf_currency_symbol'] . number_format($bookings['extrabedmoney'], 2, '.', ',') . '</td>';                
                 echo '</tr>';
                echo '<input type="hidden" name="extrabed_count" value="'.$extrabed_count.'">';
               }
                
               echo '<tr>';
                 echo '<td></td>';
                 echo '<td></td>';
                 echo '<td></td>';                
                 echo '<td align="right" style="padding-left:0px;"><strong>Total</strong></td>';
                echo '<td align="right" style="padding-right:5px; font-weight: bold;">' . $bsiCore->config['conf_currency_symbol'] . number_format($bookings['grosstotal'], 2, '.', ',') . '</td>';                
                echo '</tr>';
            }
            ?>
            <tr>
                <td colspan="4" align="right" style="text-align:right !important"><strong><?php echo SUB_TOTAL_TEXT; ?>:</strong></td>
                <td align="right" style="padding-right:5px;">
                    <strong><?php echo $bsiCore->config['conf_currency_symbol'] ?><?php echo number_format($bsibooking->roomPrices['subtotal'], 2, '.', ',') ?></strong>
                </td>
            </tr>
            <?php
            if ($bsiCore->config['conf_tax_amount'] > 0 && $bsiCore->config['conf_price_with_tax'] == 0) {
                $taxtext = "";
                ?>
                <tr>
                    <td colspan="4" align="right" style="text-align:right !important"> <strong> Surcharge(<?php echo $bsiCore->config['conf_tax_amount'] ?> %):</strong></td>
                    <td align="right" bgcolor="#f5f9f9" style="padding-right:5px;">
                        <span id="taxamountdisplay">
                            <strong><?php echo $bsiCore->config['conf_currency_symbol'] ?><?php echo number_format($bsibooking->roomPrices['totaltax'], 2, '.', ',') ?></strong>
                        </span>
                    </td>
                </tr>
            <?php
            } else {
                $taxtext = (BD_INC_TAX);
            }
            ?>
            <tr>
                <td colspan="4" align="right"style="text-align:right !important"><strong><?php echo GRAND_TOTAL_TEXT; ?>:</strong></td>
                <td align="right" style="padding-right:5px;"><strong> 
                        <span id="grandtotaldisplay">
                            <?php echo $bsiCore->config['conf_currency_symbol'] ?><?php echo number_format($bsibooking->roomPrices['grandtotal'], 2, '.', ',') ?>
                        </span></strong>
                </td>
            </tr>
            <?php
            if ($bsiCore->config['conf_enabled_deposit'] && ($bsibooking->depositPlans['deposit_percent'] > 0 && $bsibooking->depositPlans['deposit_percent'] < 100)) {
                ?>
                <tr id="advancepaymentdisplay">
                    <td colspan="4" align="right" bgcolor="#f9f9f9"  style="text-align:right !important: font-weight: bold;"><strong><?php echo ADVANCE_PAYMENT_TEXT; ?>(<span style="font-size:11px;">
                                <?php echo $bsibooking->depositPlans['deposit_percent'] ?> 
                                % <?php echo OF_GRAND_TOTAL_TEXT; ?></span>):</strong></td>
                    <td align="right" bgcolor="#F6EEFF" style="padding-right:5px; font-weight: bold;"><span id="advancepaymentamount">
                            <strong><?php echo $bsiCore->config['conf_currency_symbol'] ?><?php echo number_format($bsibooking->roomPrices['advanceamount'], 2, '.', ',') ?></strong>
                        </span></td>
                </tr>
                <?php }
            ?>
        </table>
    </div>
</div>
<br />
<br />
<div id="wrapper" class="booking_details" style="background: #f9f9f9;">
    <h2 align="left" style="padding-left:5px;margin: 20px 10px;color:#c9a205;"><i class="fa fa-user"></i>&nbsp;<?php echo CUSTOMER_DETAILS_TEXT; ?></h2>
    <div class="customer_details row">
        <div class="col-md-4 col-sm-4 col-xs-4 ext_cus width-100">
            <img src="<?php echo ASSET_URL; ?>images/excbg.png" />
        </div>      
        <div class="col-md-7 col-sm-7 col-xs-8 formnbutton width-100">
            <div class="row">
                <div class="col-md-1 col-sm-2 col-xs-3 email_text">
                    <?php echo EMAIL_TEXT; ?>:
                </div>
                <div class="col-md-10 col-sm-10 col-xs-9 eadd">
                    <input type="text" name="email_addr_existing" id="email_addr_existing" class="reservation-label"/>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button id="btn_exisitng_cust" type="submit"><?php echo FETCH_DETAILS_TEXT; ?></button>
                </div>
            </div>                  
        </div>       

    </div>

    <div class="row">
        <div class="col-md-5 col-sm-5 col-xs-4" id="hrspan"><hr><hr></div>
        <div class="col-md-2 col-sm-2 col-xs-4 customer_wrap">
            <h1 align="center" class="which_customer" style="" ><?php echo OR_TEXT; ?></h1>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-4" id="hrspan"><hr><hr></div>
    </div>



    <h3 align="left" style="padding-left:15px; color:#fff; background: #c9a205; padding: 10px 15px;"><i class="fa fa-file-text"></i>&nbsp; <?php echo NEW_CUSTOMER_TEXT; ?>?</h3>
    <!--form method="post" action="" id="form1" class="signupform"-->
        <input type="hidden" name="allowlang" id="allowlang" value="no" />
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo TITLE_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100">
                <select name="title" id="title" class="slectbox reservation-label form-control ">
                    <option value="Mr."><?php echo MR_TEXT; ?>.</option>
                    <option value="Ms."><?php echo MS_TEXT; ?>.</option>
                    <option value="Mrs."><?php echo MRS_TEXT; ?>.</option>
                    <option value="Miss."><?php echo MISS_TEXT; ?>.</option>
                    <option value="Dr."><?php echo DR_TEXT; ?>.</option>
                    <option value="Prof."><?php echo PROF_TEXT; ?>.</option>
                </select>
            </div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo FIRST_NAME_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100"><input type="text" name="fname" id="fname"  class="form-control reservation-label required" /></div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo LAST_NAME_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100"><input type="text" name="lname" id="lname"  class="form-control reservation-label  required" /></div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo ADDRESS_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100"><input type="text" name="str_addr" id="str_addr"  class="form-control reservation-label required" /></div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo CITY_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100"><input type="text" name="city"  id="city" class="form-control reservation-label required" /></div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo COUNTRY_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100">
                <select id="country" name="country" class="selectbox reservation-label form-control required">
                    <option value="">Choose a Country</option>
                    <option value="US">United States</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan Republic</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BR">Brazil</option>
                    <option value="BN">Brunei</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="C2">China</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Cote D'Ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="CD">Democratic Republic of the Congo</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="GA">Gabon Republic</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GT">Guatemala</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IE">Ireland</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Laos</option>
                    <option value="LV">Latvia</option>
                    <option value="LS">Lesotho</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MK">Macedonia</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia</option>
                    <option value="MD">Moldova</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="AN">Netherlands Antilles</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PW">Palau</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn Islands</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="QA">Qatar</option>
                    <option value="CG">Republic of the Congo</option>
                    <option value="RE">Reunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russia</option>
                    <option value="RW">Rwanda</option>
                    <option value="KN">Saint Kitts and Nevis Anguilla</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">São Tomé and Príncipe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="KR">South Korea</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SH">St. Helena</option>
                    <option value="LC">St. Lucia</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="TW">Taiwan</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania</option>
                    <option value="TH">Thailand</option>
                    <option value="TG">Togo</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="UY">Uruguay</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VA">Vatican City State</option>
                    <option value="VE">Venezuela</option>
                    <option value="VN">Vietnam</option>
                    <option value="VG">Virgin Islands (British)</option>
                    <option value="WF">Wallis and Futuna Islands</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                </select>
            </div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo PHONE_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100"><input type="text" name="phone"  id="phone" class="form-control reservation-label required" maxlenght="3"/></div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo EMAIL_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100"><input type="text" name="email"  id="email" class="form-control reservation-label required email" /></div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo PURPOSE_TRIP_TEXT; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100">
                <input type="radio" name="purpose" value="Leisure" checked="checked" id="leisure"/> Leisure
                <input type="radio" name="purpose" value="business" id="business"/> Business
            </div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md- col-sm-3 col-xs-6 cus_label width-100">
                <?php echo SPECIAL_REQUEST_TEXT; ?>  :           
            </div>
            <div class="col-md-8 col-sm-6 col-xs-6 cus_input width-100">
                <p class="estimatie_time"><?php echo SPECIAL_REQUEST_DESCRIPTION_TEXT; ?></p>                
                <textarea name="message" width="90%" class="form-control col-xs-12" rows="5"></textarea>
            </div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"><?php echo PAYMENT_METHOD; ?>:</div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100">
                <input type="radio" name="payment_type" value="cb" class="payment_type" id="cb"/> <label for="cb"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>front/img/visa.png"></label>
                <!-- <input type="radio" name="payment_type" value="mpu" class="payment_type" id="mpu"/> <label for="mpu"><img src="<?php //echo HOTELBOOKING_MANAGER_URL; ?>front/img/mpu.png"></label> -->
            </div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label width-100"></div>
            <div class="col-md-4 col-sm-6 col-xs-12 cus_input width-100">
                <input type="checkbox" name="tos" id="tos" value="" style="width:15px !important"  class="required"/>
                &nbsp;     
                <?php echo I_AGREE_WITH_THE_TEXT; ?> <a href="<?php echo $bsiCore->config['conf_tos_url']; ?>" target="_blank" > <?php echo TERMS_AND_CONDITIONS_TEXT; ?>.</a>
            </div>
        </div>
        <div class="newcustomer_datas">
            <div class="col-md-3 col-sm-3 col-xs-6 cus_label"></div>
            <div class="col-md-4 col-sm-6 col-xs-6 cus_input width-100">
                <button id="registerButton2" type="submit" name="registerButton2" value="a"><?php echo CONFIRM_TEXT; ?> &amp; <?php echo CHECKOUT_TEXT; ?></button>              
            </div>
        </div>
        <?php
//        $paymentGatewayDetails = $bsiCore->loadPaymentGateways();
//        foreach ($paymentGatewayDetails as $key => $value) {
//            echo '<input type="hidden" name="payment_type" style="margin: 0px 10px 0px 0px;" id="payment_type_' . $key . '" value="' . $key . '"/>';
//        }
        ?> 
        <input type="hidden" name="vpc_MerchTxnRef" value="<?php echo time(); ?>">
        <input type="hidden" name="vpc_OrderInfo" value="<?php echo time(); ?>">
        <input type="hidden" name="vpc_Amount" value="<?php echo $bsibooking->roomPrices['grandtotal'] * 100; ?>" >        
        <input type="hidden" name="virtualPaymentClientURL" value="https://migs.mastercard.com.au/vpcpay">
        <input type="hidden" name="vpc_Version" value="1">
        <input type="hidden" name="vpc_Command" value="pay">
        <input type="hidden" name="vpc_AccessCode" value="A194199E">
        <input type="hidden" name="vpc_Merchant" value="CB0000000141">
        <input type="hidden" name="vpc_Locale" value="en">
        <input type="hidden" name="vpc_Currency" value="USD">
        <input type="hidden" name="vpc_ReturnURL" value="<?php echo WP_HOME . '/thank/'; ?>">

    </form>
</div>
