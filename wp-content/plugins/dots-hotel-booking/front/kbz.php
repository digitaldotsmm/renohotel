<?php
include_once("includes/conf.class.php");
$bsiCore = new bsiHotelCore;
include_once("languages/" . $bsiCore->config['conf_language']);
//include_once("includes/mail.class.php");
// This sample code requires the mhash library for PHP versions older than
// 5.1.2 - http://hmhash.sourceforge.net/
// the parameters for the payment can be configured here
// the API Login ID and Transaction Key must be replaced with valid values
require_once('cb.class.php');  // include the class file
include("includes/mail.class.php");
include("includes/process.class.php");
$bsiMail = new bsiMail();
$kbz = new kbz_class();
$emailContent = $bsiMail->loadEmailContent();

$currencycode = 840;    // currencyCode [Numeric 3, Mandatory - Y]

if (isset($_REQUEST["respCode"])) {
    $merchantID = $_REQUEST["merchantID"];
    $respCode = $_REQUEST["respCode"];
    $pan = $_REQUEST["pan"];
    $amount = $_REQUEST["amount"];
    $tranRef = $_REQUEST["tranRef"];
    $invoiceNo = $_REQUEST["invoiceNo"];
    $productdescription = $_REQUEST["productDesc"];
    $approvalCode = $_REQUEST["approvalCode"];
    $eci = $_REQUEST["eci"];
    $dateTime = $_REQUEST["dateTime"];
    $date = date('d-m-Y', strtotime($dateTime));
    $status = $_REQUEST["status"];
    $fraudCode = $_REQUEST["fraudCode"];
    $userdefined1 = $_REQUEST["userDefined1"];
    $userdefined2 = $_REQUEST["userDefined2"];
    $userdefined3 = $_REQUEST["userDefined3"];
}
?>

<?php

if ($respCode == "00" && $status == "RS") { // if transaction is approved
    $successROWS = mysql_fetch_assoc(mysql_query("SELECT payment_success FROM dots_bookings WHERE booking_id='" . $invoiceNo . "'"));
    if($successROWS['payment_success'] == 0 ){
        mysql_query("UPDATE dots_bookings SET payment_success=true, payment_txnid='" . $tranRef . "', paypal_email='" . $userdefined3 . "' WHERE booking_id='" . $invoiceNo . "'");
        $terms = get_post(TG_TERMS);
        $invoiceROWS = mysql_fetch_assoc(mysql_query("SELECT client_name, client_email, invoice FROM dots_invoice WHERE booking_id='" . $invoiceNo . "'"));
        $bookingROWS = mysql_fetch_assoc(mysql_query("SELECT * FROM dots_bookings WHERE booking_id='" . $invoiceNo . "'"));
        mysql_query("UPDATE dots_clients SET existing_client = 1 WHERE email='" . $invoiceROWS['client_email'] . "'");

        $invoiceHTML = $invoiceROWS['invoice'];
    //    mysql_query("UPDATE dots_invoice SET invoice = '$invoiceHTML' WHERE booking_id='" . $invoiceNo . "'");

        $emailBody = '<a href="' . WP_HOME . '" target="_blank"><img src="' . WP_HOME . '/wp-content/uploads/2015/01/logo.png" alt="tawwingarden" style="width:120px;"/></a><br><br>';
        $emailBody .= '<div style="text-align: right;margin-right: 13px;">Date : ' . date('d M Y') . '</div>';

        $flag = 1;
        $bsiMail->sendEMail($invoiceROWS['client_email'], $emailContent['subject'], $emailBody, $invoiceNo, $flag);
        $notifyEmailSubject = BOOKING_NO . $invoiceNo . " - " . NOTIFICATION_OF_ROOM_BOOKING_BY . $invoiceROWS['client_name'];

        $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceHTML);
    }
//echo '<p>Thank you for choosing us for your stay in Yangon.</p>';
}elseif ($respCode != "00" && $status == "FA") { // if transaction is not approved or something wrong
    echo '<p>Something goes wrong. Please try again.</p>';
}else {
    $bookprs = new BookingProcess();
    global $wpdb;
    $client = $wpdb->get_row("SELECT * FROM dots_clients where client_id IN (select client_id from dots_bookings where booking_id ='" . $bookprs->bookingId . "')");

    class MessageSorting {

        // everything else is sorted at the end
        static $char2order;

        static function compare($a, $b) {
            if ($a == $b) {
                return 0;
            }
            // lazy init mapping
            $LETTERS = range(chr(32), chr(127));
            $strorder = join('', $LETTERS);
            if (empty(self::$char2order)) {
                $order = 1;
                $len = mb_strlen($strorder);
                for ($order = 0; $order < $len; ++$order) {
                    self::$char2order[mb_substr($strorder, $order, 1)] = $order;
                }
            }
            $len_a = mb_strlen($a);
            $len_b = mb_strlen($b);
            $max = min($len_a, $len_b);
            for ($i = 0; $i < $max; ++$i) {
                $char_a = mb_substr($a, $i, 1);
                $char_b = mb_substr($b, $i, 1);
                if ($char_a == $char_b)
                    continue;
                $order_a = (isset(self::$char2order[$char_a])) ? self::$char2order[$char_a] : 9999;
                $order_b = (isset(self::$char2order[$char_b])) ? self::$char2order[$char_b] : 9999;
                return ($order_a < $order_b) ? -1 : 1;
            }
            return ($len_a < $len_b) ? -1 : 1;
        }

    }

    $amount = str_pad($bookprs->totalPaymentAmount * 100, 12, '0', STR_PAD_LEFT);
    $merchantid = $kbz->merchantid;
    $secret_keys = $kbz->secret_key;
    $userdefined1 = $bookprs->bookingId; // userDefined1 [Character 150, Mandatory - N]    
    $userdefined2 = round($bookprs->totalPaymentAmount);    // userDefined2 [Character 150, Mandatory - N]    
    $userdefined3 = $bookprs->clientEmail;
    $invoicenumber = $bookprs->bookingId;
    $productdescription = $bookprs->bookingDesc;


    $array = array($amount, $invoicenumber, $currencycode, $productdescription, $merchantid, $userdefined1, $userdefined2, $userdefined3);
    uasort($array, 'MessageSorting::compare');
    $signdata = join('', $array);
    $hash = hash_hmac('sha1', $signdata, $secret_keys, false);
    $hash = strtoupper($hash);
    $hash = urlencode($hash);

    $kbz->add_field('merchantID', $merchantid);
    $kbz->add_field('invoiceNo', $invoicenumber);
    $kbz->add_field('productDesc', $productdescription);
    $kbz->add_field('amount', $amount);
    $kbz->add_field('currencyCode', $currencycode);
    $kbz->add_field('userDefined1', $userdefined1);
    $kbz->add_field('userDefined2', $userdefined2);
    $kbz->add_field('userDefined3', $userdefined3);
    $kbz->add_field('hashValue', $hash);
    $kbz->submit_kbz_post();
}
?>