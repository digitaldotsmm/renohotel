<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
include("includes/conf.class.php");
include_once("languages/" . $bsiCore->config['conf_language']);
//include_once("includes/mail.class.php");
// This sample code requires the mhash library for PHP versions older than
// 5.1.2 - http://hmhash.sourceforge.net/
// the parameters for the payment can be configured here
// the API Login ID and Transaction Key must be replaced with valid values
require_once('authorize.net.class.php');  // include the class file
include("includes/mail.class.php");
include("includes/process.class.php");
$bsiMail = new bsiMail();
$anet = new authorize_net_class();
$emailContent = $bsiMail->loadEmailContent();
$anet->relay_url = WP_HOME . '/processing/';
//for production, change test mode to false and comment out the authorzine net url
$anet->test_mode = true;
//$anet->anet_url = "https://test.authorize.net/gateway/transact.dll";
$anet->anet_url = "https://secure.authorize.net/gateway/transact.dll";


if (isset($_REQUEST["x_response_code"])) {
    $ResponseCode = trim($_REQUEST["x_response_code"]);
    $ResponseReasonText = trim($_REQUEST["x_response_reason_text"]);
    $ResponseReasonCode = trim($_REQUEST["x_response_reason_code"]);
    $InvoiceNo = trim($_REQUEST['x_invoice_num']);
    $AVS = trim($_REQUEST["x_avs_code"]);
    $TransID = trim($_REQUEST["x_trans_id"]);
    $AuthCode = trim($_REQUEST["x_auth_code"]);
    $Amount = trim($_REQUEST["x_amount"]);
    $EmailAddress = trim($_REQUEST['x_email']);
}
?>

<?php
if ($ResponseCode == 1) : // if transaction is approved  
    mysql_query("UPDATE dots_bookings SET payment_success=true, payment_txnid='" . $TransID . "', paypal_email='" . $EmailAddress . "' WHERE booking_id='" . $InvoiceNo . "'");

    $invoiceROWS = mysql_fetch_assoc(mysql_query("SELECT client_name, client_email, invoice FROM dots_invoice WHERE booking_id='" . $InvoiceNo . "'"));
    mysql_query("UPDATE dots_clients SET existing_client = 1 WHERE email='" . $invoiceROWS['client_email'] . "'");

    $invoiceHTML = $invoiceROWS['invoice'];
    $invoiceHTML.= '<br><br><table style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; bgcolor:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . INV_PAY_DETAILS . '</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . INV_PAY_OPTION . '</td><td align="left" style="background:#ffffff">Authorize.net</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . PAYER_EMAIL . '</td><td align="left" style="background:#ffffff">' . $EmailAddress . '</td></tr><tr><td align="left" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . INV_TXN_ID . '</td><td align="left" style="background:#ffffff">' . $TransID . '</td></tr></table>';

    mysql_query("UPDATE dots_invoice SET invoice = '$invoiceHTML' WHERE booking_id='" . $InvoiceNo . "'");


    $emailBody = '<a href="'.WP_HOME.'" target="_blank"><img src="'.WP_HOME.'/wp-content/themes/hotelshalom/assets/images/York1.jpg" alt="york residence" style="width:200px;margin-left: 237px;"/></a><br><br>';
    $emailBody .= "Dear " . $invoiceROWS['client_name'] . ",<br><br>";
   // $emailBody .= html_entity_decode($emailContent['body']) . "<br><br>";
    $emailBody .= $invoiceHTML;
    $emailBody .= "<br><br>" . PP_REGARDS . ",<br>" . $bsiCore->config['conf_hotel_name'] . '<br>' . $bsiCore->config['conf_hotel_phone'];
    $emailBody .= "<br><br><font style=\"color:#F00; font-size:10px;\">[ " . PP_CARRY . " ]</font>";
    $flag = 1;
    $bsiMail->sendEMail($invoiceROWS['client_email'], $emailContent['subject'], $emailBody, $InvoiceNo, $flag);

    /* Notify Email for Hotel about Booking */
    $notifyEmailSubject = BOOKING_NO . $InvoiceNo . " - " . NOTIFICATION_OF_ROOM_BOOKING_BY . $invoiceROWS['client_name'];

    $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceHTML);
    //echo '<p>Thank you for choosing us for your stay in Yangon.</p>';
elseif ($ResponseCode > 1) : // if transaction is not approved or something wrong
    echo '<p>Something goes wrong. Please try again.</p>';
else :
    $bookprs = new BookingProcess();
    global $wpdb;
    $client = $wpdb->get_row("SELECT * FROM dots_clients where client_id IN (select client_id from dots_bookings where booking_id ='" . $bookprs->bookingId . "')");
    $anet->amount = $bookprs->totalPaymentAmount;
    $anet->add_field('x_amount', number_format($bookprs->totalPaymentAmount, 2, '.', ''));
    $anet->add_field('x_invoice_num', $bookprs->bookingId);
    $anet->add_field('x_login', '7Y45Qdr4');
    $anet->add_field('x_description', $bookprs->bookingDesc);
    $anet->add_field('x_first_name', $client->first_name);
    $anet->add_field('x_last_name', $client->surname);
    $anet->add_field('x_address', $client->street_addr);
    $anet->add_field('x_city', $client->city);
    $anet->add_field('x_state', $client->province);
    $anet->add_field('x_zip', $client->zip);
    $anet->add_field('x_country', $client->country);
    $anet->add_field('x_phone', $client->phone);
    $anet->add_field('x_fax', $client->fax);
    $anet->add_field('x_email', $client->email);
    $anet->add_field('x_fp_hash', $anet->get_fingerprint());
    $anet->add_field('x_test_request', $anet->test_mode);
    $anet->add_field('x_relay_response', 'true');
    $anet->add_field('x_relay_always', 'true');
    $anet->add_field('x_relay_url', $anet->relay_url);
//    $anet->add_field('x_receipt_link_method', 'POST');
//    $anet->add_field('x_receipt_link_text', 'Click here to return to our home page');
//    $anet->add_field('x_receipt_link_URL', $anet->relay_url);
    $anet->submit_anet_post();
endif; 

