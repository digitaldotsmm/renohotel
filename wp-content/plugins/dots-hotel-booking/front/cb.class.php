<?php

//if (!defined('HOTELBOOKING_VERSION')) {
//    header('Status: 403 Forbidden');
//    header('HTTP/1.1 403 Forbidden');
//    exit();
//}
ob_start(); 
class cb_class {

    var $kbz_url = "";
    var $merchantid = "";
    var $secret_key = "";
    var $access_code = "";
    var $last_error;                 // holds the last error encountered
    var $ipn_log;                    // bool: log IPN results to text file?
    var $test_mode = false;
    var $ipn_response;               // holds the IPN response from paypal   
    var $ipn_data = array();         // array contains the POST values for IPN
    var $fields = array();           // array holds the fields to submit to paypal
    var $x_relay_url = "";
    var $amount;
    var $fingerprint;

    public function __construct() {
        // initialization constructor.  Called when class is created.

        $this->kbz_url = 'https://migs.mastercard.com.au/vpcpay';
        $this->merchantid = "CB0000000141"; //sandbox log in id
        $this->secret_key = "07EAA9D20EF287C154AC513D1E598826"; //sandbox log in id
        $this->access_code = "A194199E";
        $this->last_error = '';

        $this->sequence = rand(1, 1000);
        // a timestamp is generated
        $this->timeStamp = time();
    }

    function process($bookingId) {
            $SECURE_SECRET = $this->secret_key;
        $vpcURL = $_POST["virtualPaymentClientURL"] . "?";
        unset($_POST["virtualPaymentClientURL"]);
        unset($_POST["SubButL"]);
        // merchant secret has been provided.
        //$md5HashData = $SECURE_SECRET;
        //ksort($_POST);

        $SECURE_SECRET = pack('H*', $SECURE_SECRET);
        $value_string = "";
        ksort($_POST);

// set a parameter to show the first pair in the URL
        $appendAmp = 0;

        $allowed_keys = array("vpc_MerchTxnRef", "vpc_OrderInfo", "vpc_Amount", "virtualPaymentClientURL",
            "vpc_Version", "vpc_Command", "vpc_AccessCode", "vpc_Merchant", "vpc_Locale", "vpc_ReturnURL");
        foreach ($_POST as $key => $value) {
            if (!in_array($key, $allowed_keys)) {
                continue;
            }
//            // create the md5 input and URL leaving out any fields that have no value
//            if (count($value) > 0) {
//                if ($key == 'vpc_MerchTxnRef' || $key == 'vpc_OrderInfo') {
//                    $value = $bookingId;
//                }
//                // this ensures the first paramter of the URL is preceded by the '?' char
//                if ($appendAmp == 0) {
//                    if (!is_array($value) && !is_array($key)) {
//                        $vpcURL .= urlencode($key) . '=' . urlencode($value);
//                    }
//                    $appendAmp = 1;
//                } else {
//                    if (!is_array($value) && !is_array($key)) {
//                        $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
//                    }
//                }
//                if (!is_array($value)) {
//                    $md5HashData .= $value;
//                }
//            }
            // create the md5 input and URL leaving out any fields that have no value
            if (strlen($value) > 0) {
                if ($key == 'vpc_MerchTxnRef' || $key == 'vpc_OrderInfo') {
                    $value = $bookingId;
                }
                // this ensures the first paramter of the URL is preceded by the '?' char
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $value_string .= $key . '=' . $value;
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                    $value_string .= '&' . $key . "=" . $value;
                }
            }
        }

// Create the secure hash and append it to the Virtual Payment Client Data if
// the merchant secret has been provided.
        //https://migs.mastercard.com.au/vpcpay?Title=Thank+You&vpc_AccessCode=FDC4C34B&vpc_Amount=15400&vpc_Command=pay&vpc_Locale=en&vpc_MerchTxnRef=1447225959&vpc_Merchant=CB0000000093&vpc_OrderInfo=1447225959&vpc_ReturnURL=http%3A%2F%2Fgreattreasure.dd%2Fthank%2F&vpc_Version=1&vpc_SecureHash=85050806BD5EF936CCFF422977256B3D
        //https://migs.mastercard.com.au/vpcpay?Title=Thank+You&vpc_AccessCode=FDC4C34B&vpc_Amount=12600&vpc_Command=pay&vpc_Locale=en&vpc_Merchant=CB0000000093&vpc_ReturnURL=http%3A%2F%2Fgreattreasure.dd%2Fthank%2F&vpc_Version=1&vpc_MerchTxnRef=1447325874&vpc_OrderInfo=1447325874&vpc_SecureHash=5C634E63D030AF5107EC514E6B883B1B
//        if (strlen($SECURE_SECRET) > 0) {
//            $vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
//        }
        $hash_value = strtoupper(hash_hmac("sha256", $value_string, $SECURE_SECRET));
        if (strlen($SECURE_SECRET) > 0) {
            $vpcURL .= "&vpc_SecureHash=" . $hash_value . "&vpc_SecureHashType=SHA256";
        }
        header("Location: " . $vpcURL);
        exit;
// FINISH TRANSACTION - Redirect the customers using the Digital Order
// ===================================================================        
    }

    function getResponseDescription($responseCode) {

        switch ($responseCode) {
            case "0" : $result = "Transaction Successful";
                break;
            case "?" : $result = "Transaction status is unknown";
                break;
            case "1" : $result = "Unknown Error";
                break;
            case "2" : $result = "Bank Declined Transaction";
                break;
            case "3" : $result = "No Reply from Bank";
                break;
            case "4" : $result = "Expired Card";
                break;
            case "5" : $result = "Insufficient funds";
                break;
            case "6" : $result = "Error Communicating with Bank";
                break;
            case "7" : $result = "Payment Server System Error";
                break;
            case "8" : $result = "Transaction Type Not Supported";
                break;
            case "9" : $result = "Bank declined transaction (Do not contact Bank)";
                break;
            case "A" : $result = "Transaction Aborted";
                break;
            case "C" : $result = "Transaction Cancelled";
                break;
            case "D" : $result = "Deferred transaction has been received and is awaiting processing";
                break;
            case "F" : $result = "3D Secure Authentication failed";
                break;
            case "I" : $result = "Card Security Code verification failed";
                break;
            case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)";
                break;
            case "N" : $result = "Cardholder is not enrolled in Authentication scheme";
                break;
            case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed";
                break;
            case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed";
                break;
            case "S" : $result = "Duplicate SessionID (OrderInfo)";
                break;
            case "T" : $result = "Address Verification Failed";
                break;
            case "U" : $result = "Card Security Code Failed";
                break;
            case "V" : $result = "Address Verification and Card Security Code Failed";
                break;
            default : $result = "Unable to be determined";
        }
        return $result;
    }

//  -----------------------------------------------------------------------------
// This method uses the verRes status code retrieved from the Digital
// Receipt and returns an appropriate description for the QSI Response Code
// @param statusResponse String containing the 3DS Authentication Status Code
// @return String containing the appropriate description

    function getStatusDescription($statusResponse) {
        if ($statusResponse == "" || $statusResponse == "No Value Returned") {
            $result = "3DS not supported or there was no 3DS data provided";
        } else {
            switch ($statusResponse) {
                Case "Y" : $result = "The cardholder was successfully authenticated.";
                    break;
                Case "E" : $result = "The cardholder is not enrolled.";
                    break;
                Case "N" : $result = "The cardholder was not verified.";
                    break;
                Case "U" : $result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer.";
                    break;
                Case "F" : $result = "There was an error in the format of the request from the merchant.";
                    break;
                Case "A" : $result = "Authentication of your Merchant ID and Password to the ACS Directory Failed.";
                    break;
                Case "D" : $result = "Error communicating with the Directory Server.";
                    break;
                Case "C" : $result = "The card type is not supported for authentication.";
                    break;
                Case "S" : $result = "The signature on the response received from the Issuer could not be validated.";
                    break;
                Case "P" : $result = "Error parsing input from Issuer.";
                    break;
                Case "I" : $result = "Internal Payment Server system error.";
                    break;
                default : $result = "Unable to be determined";
                    break;
            }
        }
        return $result;
    }

//  -----------------------------------------------------------------------------
// If input is null, returns string "No Value Returned", else returns input
    function null2unknown($data) {
        if ($data == "") {
            return "No Value Returned";
        } else {
            return $data;
        }
    }
    
    public function getResponse($input){
        $SECURE_SECRET = $this->secret_key;
        $SECURE_SECRET = pack("H*", $SECURE_SECRET);
        $vpc_Txn_Secure_Hash = $input["vpc_SecureHash"];
        unset($input["vpc_SecureHash"]);
        $value_string = "";
        // set a flag to indicate if hash has been validated
        $errorExists = false;
        $increment = 0;
        ksort($_GET);

        if (strlen($SECURE_SECRET) > 0 && $input["vpc_TxnResponseCode"] != "7" && $input["vpc_TxnResponseCode"] != "No Value Returned") {

            $md5HashData = $SECURE_SECRET;

//            // sort all the incoming vpc response fields and leave out any with no value
//            foreach($_GET as $key => $value) {
//                if ($key != "vpc_SecureHash" or strlen($value) > 0) {
//                    $md5HashData .= $value;
//                }
//            }
            // sort all the incoming vpc response fields and leave out any with no value
            foreach ($_GET as $key => $value) {
                if ($key != "vpc_SecureHash" and $key != "vpc_SecureHashType" and strlen($value) > 0) {
                    if ($increment == 0) {
                        $value_string .= $key . "=" . $value;
                        $increment = 1;
                    } else {
                        $value_string .= "&" . $key . "=" . $value;
                    }
                    // $md5HashData .= $value;
                }
            }

            // Validate the Secure Hash (remember MD5 hashes are not case sensitive)
            // This is just one way of displaying the result of checking the hash.
            // In production, you would work out your own way of presenting the result.
            // The hash check is all about detecting if the data has changed in transit.
//            if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData))) {
//                // Secure Hash validation succeeded, add a data field to be displayed
//                // later.
//                $hashValidated = "<FONT color='#00AA00'><strong>CORRECT</strong></FONT>";
//            } else {
//                // Secure Hash validation failed, add a data field to be displayed
//                // later.
//                $hashValidated = "<FONT color='#FF0066'><strong>INVALID HASH</strong></FONT>";
//                $errorExists = true;
//            }
            if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(hash_hmac("sha256", $value_string, $SECURE_SECRET))) {
                // Secure Hash validation succeeded, add a data field to be displayed
                // later.
                $hashValidated = "<FONT color='#00AA00'><strong>CORRECT</strong></FONT>";
            } else {
                // Secure Hash validation failed, add a data field to be displayed
                // later.
                $hashValidated = "<FONT color='#FF0066'><strong>INVALID HASH</strong></FONT>";
                $errorExists = true;
            }
        } else {
            // Secure Hash was not validated, add a data field to be displayed later.
            $hashValidated = "<FONT color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></FONT>";
        }
        
        $amount          = $this->null2unknown($input["vpc_Amount"]);
        $locale          = $this->null2unknown($input["vpc_Locale"]);
        $batchNo         = $this->null2unknown($input["vpc_BatchNo"]);
        $command         = $this->null2unknown($input["vpc_Command"]);
        $message         = $this->null2unknown($input["vpc_Message"]);
        $version         = $this->null2unknown($input["vpc_Version"]);
        $cardType        = $this->null2unknown($input["vpc_Card"]);
        $orderInfo       = $this->null2unknown($input["vpc_OrderInfo"]);
        $receiptNo       = $this->null2unknown($input["vpc_ReceiptNo"]);
        $merchantID      = $this->null2unknown($input["vpc_Merchant"]);
        if(isset($input["vpc_AuthorizeId"])){
            $authorizeID     = $this->null2unknown($input["vpc_AuthorizeId"]);
        }
        $merchTxnRef     = $this->null2unknown($input["vpc_MerchTxnRef"]);
        $transactionNo   = $this->null2unknown($input["vpc_TransactionNo"]);
        $acqResponseCode = $this->null2unknown($input["vpc_AcqResponseCode"]);
        $txnResponseCode = $this->null2unknown($input["vpc_TxnResponseCode"]);


        // 3-D Secure Data
        $verType         = array_key_exists("vpc_VerType", $_GET)          ? $input["vpc_VerType"]          : "No Value Returned";
        $verStatus       = array_key_exists("vpc_VerStatus", $_GET)        ? $input["vpc_VerStatus"]        : "No Value Returned";
        $token           = array_key_exists("vpc_VerToken", $_GET)         ? $input["vpc_VerToken"]         : "No Value Returned";
        $verSecurLevel   = array_key_exists("vpc_VerSecurityLevel", $_GET) ? $input["vpc_VerSecurityLevel"] : "No Value Returned";
        $enrolled        = array_key_exists("vpc_3DSenrolled", $_GET)      ? $input["vpc_3DSenrolled"]      : "No Value Returned";
        $xid             = array_key_exists("vpc_3DSXID", $_GET)           ? $input["vpc_3DSXID"]           : "No Value Returned";
        $acqECI          = array_key_exists("vpc_3DSECI", $_GET)           ? $input["vpc_3DSECI"]           : "No Value Returned";
        $authStatus      = array_key_exists("vpc_3DSstatus", $_GET)        ? $input["vpc_3DSstatus"]        : "No Value Returned";
        
        $errorTxt = "";

        // Show this page as an error page if vpc_TxnResponseCode equals '7'
        if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
            $errorTxt = "Error ";
        }
        
        $data['bookingid'] = $orderInfo;
        $data['transactionNo'] = $transactionNo;
        $data['receiptNo'] = $receiptNo;
        $data['txnResponseCode'] = $txnResponseCode;
        
        return $data;
        
        
    }

}

