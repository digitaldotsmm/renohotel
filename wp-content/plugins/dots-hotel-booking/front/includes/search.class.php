<?php

/**
 * @package BSI
 * @author BestSoft Inc see README.php
 * @copyright BestSoft Inc.
 * See COPYRIGHT.php for copyright notices and details.
 */
class bsiSearch {

    public $checkInDate = '';
    public $checkOutDate = '';
    public $mysqlCheckInDate = '';
    public $mysqlCheckOutDate = '';
    public $guestsPerRoom = 0;
    public $childPerRoom = 0;
    public $extrabedPerRoom = 0;
    //public $extrabedPerRoom = false;
    public $nightCount = 0;
    public $roomTypeID = '';
    public $fullDateRange;
    public $roomType = array();
    public $multiCapacity = array();
    public $searchCode = "SUCCESS";
    public $maxDisplayedDays = 15;

    const SEARCH_CODE = "SUCCESS";

    function bsiSearch() {
        
        $this->setRequestParams();
        $this->getNightCount();
        $this->checkSearchEngine();
        if ($this->searchCode == self::SEARCH_CODE) {
            $this->loadMultiCapacity();
            $this->loadRoomTypes();            
            $this->fullDateRange = $this->getDateRangeArray($this->mysqlCheckInDate, $this->mysqlCheckOutDate);
            $this->setMySessionVars();
        }
    }

    private function setRequestParams() {
        $bsiCore = new bsiHotelCore;
        
        $tmpVar = isset($_POST['check_in']) ? $_POST['check_in'] : NULL;
        $this->setMyParamValue($this->checkInDate, $bsiCore->ClearInput($tmpVar), NULL, true);
        $tmpVar = isset($_POST['check_out']) ? $_POST['check_out'] : NULL;
        $this->setMyParamValue($this->checkOutDate, $bsiCore->ClearInput($tmpVar), NULL, true);
        $tmpVar = isset($_POST['capacity']) ? $_POST['capacity'] : 0;
        $this->setMyParamValue($this->guestsPerRoom, $bsiCore->ClearInput($tmpVar), 0, true);
        $tmpVar = isset($_POST['child_per_room']) ? $_POST['child_per_room'] : 0;
        $this->setMyParamValue($this->childPerRoom, $bsiCore->ClearInput($tmpVar), 0, true);
        $tmpVar = isset($_POST['extrabed_per_room']) ? $_POST['extrabed_per_room'] : 0;
        $this->setMyParamValue($this->extrabedPerRoom, $bsiCore->ClearInput($tmpVar), 0, true);
        $tmpVar = isset($_POST['room_type_id']) ? $_POST['room_type_id'] : 0;
        $this->setMyParamValue($this->roomTypeID, $bsiCore->ClearInput($tmpVar), 0, true);
        $tmpVar = isset($_POST['roomtype_id']) ? $_POST['roomtype_id'] : 0;
        $this->setMyParamValue($this->roomTypeID, $bsiCore->ClearInput($tmpVar), 0, true);
        
        $this->mysqlCheckInDate = $bsiCore->getMySqlDate($this->checkInDate);
        $this->mysqlCheckOutDate = $bsiCore->getMySqlDate($this->checkOutDate);
    }

    private function setMyParamValue(&$membervariable, $paramvalue, $defaultvalue, $required = false) {
        if ($required) {
            if (!isset($paramvalue)) {
                $this->invalidRequest();
            }
        }
        if (isset($paramvalue)) {
            $membervariable = $paramvalue;
        } else {
            $membervariable = $defaultvalue;
        }
    }

    private function setMySessionVars() {
        if (isset($_SESSION['sv_checkindate']))
            unset($_SESSION['sv_checkindate']);
        if (isset($_SESSION['sv_checkoutdate']))
            unset($_SESSION['sv_checkoutdate']);
        if (isset($_SESSION['sv_mcheckindate']))
            unset($_SESSION['sv_mcheckindate']);
        if (isset($_SESSION['sv_mcheckoutdate']))
            unset($_SESSION['sv_mcheckoutdate']);
        if (isset($_SESSION['sv_nightcount']))
            unset($_SESSION['sv_nightcount']);
        if (isset($_SESSION['sv_guestperroom']))
            unset($_SESSION['sv_guestperroom']);
        if (isset($_SESSION['sv_childcount']))
            unset($_SESSION['sv_childcount']);
        if (isset($_SESSION['sv_extrabedcount']))
            unset($_SESSION['sv_extrabedcount']);

        $_SESSION['sv_checkindate'] = $this->checkInDate;
        $_SESSION['sv_checkoutdate'] = $this->checkOutDate;
        $_SESSION['sv_mcheckindate'] = $this->mysqlCheckInDate;
        $_SESSION['sv_mcheckoutdate'] = $this->mysqlCheckOutDate;
        $_SESSION['sv_nightcount'] = $this->nightCount;
        $_SESSION['sv_guestperroom'] = $this->guestsPerRoom;
        $_SESSION['sv_childcount'] = $this->childPerRoom;
        $_SESSION['sv_extrabedcount'] = $this->extrabedPerRoom;
        $_SESSION['svars_details'] = array();
    }

    private function invalidRequest() {
        header('Location: booking-failure.php?error_code=9');
        die;
    }

    private function getNightCount() {
        $checkin_date = getdate(strtotime($this->mysqlCheckInDate));
        $checkout_date = getdate(strtotime($this->mysqlCheckOutDate));
        $checkin_date_new = mktime(12, 0, 0, $checkin_date['mon'], $checkin_date['mday'], $checkin_date['year']);
        $checkout_date_new = mktime(12, 0, 0, $checkout_date['mon'], $checkout_date['mday'], $checkout_date['year']);
        $this->nightCount = round(abs($checkin_date_new - $checkout_date_new) / 86400);
    }

    private function getDateRangeArray($startDate, $endDate, $nightAdjustment = true) {
        $date_arr = array();
        $day_array = array();
        $total_array = array();
        $time_from = mktime(1, 0, 0, substr($startDate, 5, 2), substr($startDate, 8, 2), substr($startDate, 0, 4));
        $time_to = mktime(1, 0, 0, substr($endDate, 5, 2), substr($endDate, 8, 2), substr($endDate, 0, 4));
        if ($time_to >= $time_from) {
            if ($nightAdjustment) {
                while ($time_from < $time_to) {
                    array_push($date_arr, date('Y-m-d', $time_from));
                    array_push($day_array, date('D', $time_from));
                    $time_from+= 86400; // add 24 hours
                }
            } else {
                while ($time_from <= $time_to) {
                    array_push($date_arr, date('Y-m-d', $time_from));
                    array_push($day_array, $time_from);
                    $time_from+= 86400; // add 24 hours
                }
            }
        }
        array_push($total_array, $date_arr);
        array_push($total_array, $day_array);
        return $total_array;
    }

    private function checkSearchEngine() {
        $bsiCore = new bsiHotelCore;
        if (intval($bsiCore->config['conf_booking_turn_off']) > 0) {
            $this->searchCode = "SEARCH_ENGINE_TURN_OFF";
            return 0;
        }
        $diffrow = mysql_fetch_assoc(mysql_query("SELECT DATEDIFF('" . $this->mysqlCheckOutDate . "', '" . $this->mysqlCheckInDate . "') AS INOUTDIFF"));
        $dateDiff = intval($diffrow['INOUTDIFF']);
        if ($dateDiff < 0) {
            $this->searchCode = "OUT_BEFORE_IN";
            return 0;
        } else if ($dateDiff < intval($bsiCore->config['conf_min_night_booking'])) {
            $this->searchCode = "NOT_MINNIMUM_NIGHT";
            return 0;
        }
        $userInputDate = strtotime($this->mysqlCheckInDate);
        $hotelDateTime = strtotime(date("Y-m-d"));
        $timezonediff = ($userInputDate - $hotelDateTime);
        if ($timezonediff < 0) {
            $this->searchCode = "TIME_ZONE_MISMATCH";
            return 0;
        }
    }

    private function loadRoomTypes() {
        $sql = mysql_query("SELECT * FROM dots_roomtype");
        
        while ($currentrow = mysql_fetch_assoc($sql)) {
            array_push($this->roomType, array('rtid' => $currentrow["roomtype_ID"], 'rtname' => $currentrow["type_name"], 'rtimg' => $currentrow["img1"], 'rtpostid' => $currentrow['room_post_id']));
        }
        mysql_free_result($sql);
    }

    private function loadMultiCapacity() {
        $sql = mysql_query("SELECT * FROM dots_capacity WHERE capacity >= " . $this->guestsPerRoom);
        while ($currentrow = mysql_fetch_assoc($sql)) {
            $this->multiCapacity[$currentrow["id"]] = array('capval' => $currentrow["capacity"], 'captitle' => $currentrow["title"]);
        }
        mysql_free_result($sql);
    }
    
    public function getextrabedprice($roomTypeId){
       $sql = mysql_query("SELECT sun FROM dots_priceplan WHERE roomtype_id='$roomTypeId' and capacity_id='1002'"); 
       $row=mysql_fetch_assoc($sql);
       return $row['sun'];
       
    }
    
    public function getroomtypename($roomTypeId){
       $sql = mysql_query("SELECT type_name FROM dots_roomtype WHERE roomtype_id='$roomTypeId'"); 
       $row=mysql_fetch_assoc($sql);
       return $row['type_name'];
       
    }
    
    public function getexcludebreakfastprice($roomTypeId){
       $sql = mysql_query("SELECT sun FROM dots_priceplan WHERE roomtype_id='$roomTypeId' and capacity_id='1003'"); 
       $row=mysql_fetch_assoc($sql);
       return $row['sun'];
       
    }
    
    public function getcapicity($capacityId){
       $sql = mysql_query("SELECT title FROM dots_capacity WHERE id='$capacityId'"); 
       $row=mysql_fetch_assoc($sql);
       
       return $row['title'];
       
    }
    public function getAvailableRooms($roomTypeId, $roomTypeName, $capcityid) {
        $bsiCore = new bsiHotelCore;
        $child_flag = false;
        $currency_symbol = $bsiCore->config['conf_currency_symbol'];
        $searchresult = array('roomtypeid' => $roomTypeId, 'roomtypename' => $roomTypeName, 'capacityid' => $capcityid, 'capacitytitle' => $this->multiCapacity[$capcityid]['captitle'], 'capacity' => $this->multiCapacity[$capcityid]['capval']);
        $room_count = 0;
        $dropdown_html = '<option value="0" selected="selected">0</option>';
        $price_details_html = '';
        $total_price_amount = 0;
        $calculated_extraprice = 0;
        $total_extrabed = 0;
        $tmpTotalDays = $this->getDateRangeArray($this->mysqlCheckInDate, $this->mysqlCheckOutDate);
        $tmpDayCollections = $tmpTotalDays[0];
        $actual_day_count = count($tmpDayCollections);

        $tmpMysqlCheckOutDate = $this->mysqlCheckOutDate; //we don't want to touch the actual check out date at all
        if ($actual_day_count <= $this->maxDisplayedDays) {
            $add_days = $this->maxDisplayedDays - $actual_day_count;
            //$tmpMysqlCheckOutDate = date('Y-m-d', strtotime($tmpMysqlCheckOutDate . " +".$add_days."days"));
        } else {
            $reminder = $actual_day_count % $this->maxDisplayedDays;
            $add_days = $this->maxDisplayedDays - $reminder;
        }
        $tmpMysqlCheckOutDate = date('Y-m-d', strtotime($tmpMysqlCheckOutDate . " +" . $add_days . "days"));

        $searchsql = "
		SELECT rm.room_ID, rm.room_no,rm.extra_bed
		  FROM dots_room rm
		 WHERE rm.roomtype_id IN (" . $roomTypeId . ") 
			   AND rm.capacity_id = " . $capcityid . "
			   AND rm.room_id NOT IN
					  (SELECT resv.room_id
						 FROM dots_reservation resv, dots_bookings boks
                                                WHERE     boks.is_deleted = FALSE
							  AND resv.bookings_id = boks.booking_id
							  AND resv.room_type_id IN (" . $roomTypeId . ")
							  AND (('" . $this->mysqlCheckInDate . "' BETWEEN boks.start_date AND DATE_SUB(boks.end_date, INTERVAL 1 DAY))
							   OR (DATE_SUB('" . $tmpMysqlCheckOutDate . "', INTERVAL 1 DAY) BETWEEN boks.start_date AND DATE_SUB(boks.end_date, INTERVAL 1 DAY)) OR (boks.start_date BETWEEN '" . $this->mysqlCheckInDate . "' AND DATE_SUB('" . $this->mysqlCheckOutDate . "', INTERVAL 1 DAY))   					   OR (DATE_SUB(boks.end_date, INTERVAL 1 DAY) BETWEEN '" . $this->mysqlCheckInDate . "' AND DATE_SUB('" . $this->mysqlCheckOutDate . "', INTERVAL 1 DAY))))";
    //  echo $searchsql;die;
        $sql = mysql_query($searchsql);
        $tmpctr = 1;
        $searchresult['availablerooms'] = array();
        
        while ($currentrow = mysql_fetch_assoc($sql)) {            
            $dropdown_html.= '<option value="' . $tmpctr . '">' . $tmpctr . '</option>';
            array_push($searchresult['availablerooms'], array('roomid' => $currentrow["room_ID"], 'roomno' => $currentrow["room_no"]));
            $tmpctr++;
            $total_extrabed = $currentrow['extra_bed'];
        }
        
        
        

        mysql_free_result($sql);
        
        if ($tmpctr > 1) {
            $totalDays = $this->getDateRangeArray($this->mysqlCheckInDate, $tmpMysqlCheckOutDate);
            $totalamt3 = 0;
            //			$dayName=array_count_values($totalDays[1]);                        
            $dayName = $totalDays[1];
            $daycollections = $totalDays[0];
            foreach ($daycollections as $key => $eachday) {
                $day_arr = explode('-', date('M-d-D', strtotime($eachday)));
                $valid_dates[$key]['month'] = $day_arr[0];
                $valid_dates[$key]['day_num'] = $day_arr[1];
                $valid_dates[$key]['day'] = $day_arr[2];
            }
            $_month = date('M', strtotime($this->mysqlCheckInDate));
            $month_ = date('M', strtotime($tmpMysqlCheckOutDate));
            $_color = '#f2f2f2';
            $color_ = '#f2f2f2';
            if ($_month == $month_) {
                $mon = $_month;
            } else {
                $mon = $_month . ' - ' . $month_;
            }

//            $price_details_html .= '<tr style="background: #EDF8FA; color: #0077A2; font-style: italic; font-weight: bold; padding: 20px;"><td style="width: 10px"></td>';
////			  foreach($dayName as $days => $totalnum){                         
//            
//            foreach ($valid_dates as $date) {
////				 if($days == 'Sat' || $days == 'Sun'){
//                if ($date['day'] == 'Sat' || $date['day'] == 'Sun') {
////				 	 $price_details_html.='<td bgcolor='.$_color.'><b>'.$totalnum.' x '.constant(strtoupper($days)).'</b></td>';
//                    $price_details_html.='<td width="150">' . $date['day'] .'</td>';
//                } else {
////					 $price_details_html.='<td bgcolor='.$color_.'><b>'.$totalnum.' x '.constant(strtoupper($days)).'</b></td>';
//                    $price_details_html.='<td width="150">' . $date['day'] .  '</td>';                    
//                }                
//                $days[$date['day']] = 0;
//            }
//            $price_details_html .= '<td style="width: 10px"></td><td style="width: 10px"></td>';
//            $price_details_html .= '</tr>';
//            $price_details_html .= '<tr><td style="width: 10px"></td>';
//            foreach ($valid_dates as $date) {
////				 if($days == 'Sat' || $days == 'Sun'){
//                if ($date['day'] == 'Sat' || $date['day'] == 'Sun') {
////				 	 $price_details_html.='<td bgcolor='.$_color.'><b>'.$totalnum.' x '.constant(strtoupper($days)).'</b></td>';
//                    
//                    $price_details_html.='<td>'. $date['day_num'] . '&nbsp;' . $date['month'] . '</td>';
//                } else {
////					 $price_details_html.='<td bgcolor='.$color_.'><b>'.$totalnum.' x '.constant(strtoupper($days)).'</b></td>';
//                    
//                    $price_details_html.='<td>'  . $date['day_num'] . '&nbsp;' .  $date['month'] . '</td>';
//                }
//                $days[$date['day']] = 0;
//            }
//            $price_details_html .= '<td style="width: 10px"></td><td style="width: 10px"></td></tr>';
//            $price_details_html.='';
            foreach ($daycollections as $date2 => $val) {
                $day_arr = explode('-', date('M-d-D', strtotime($val)));
                $valid_dates[$date2]['month'] = $day_arr[0];
                $valid_dates[$date2]['day_num'] = $day_arr[1];
                $valid_dates[$date2]['day'] = $day_arr[2];


                $pricesql = mysql_query("SELECT * FROM dots_priceplan WHERE roomtype_id = " . $roomTypeId . " AND capacity_id = " . $capcityid . " AND ('" . $val . "' BETWEEN start_date AND end_date)");
                if (mysql_num_rows($pricesql)) {
                    $row = mysql_fetch_assoc($pricesql);
                } else {
                    $pricesql2 = mysql_query("SELECT * FROM dots_priceplan WHERE roomtype_id = " . $roomTypeId . " AND capacity_id = " . $capcityid . " AND  default_plan=1");
                    $row = mysql_fetch_assoc($pricesql2);
                }
                $day = date('D', strtotime($val));
//				$$day+=$row[strtolower($day)];
                $days[] = array($day => $row[strtolower($day)]);

//                 $chld_row2 = mysql_fetch_assoc(mysql_query("SELECT distinct(`no_of_child`) FROM `dots_room` WHERE `capacity_id`=" . $capcityid . " and `roomtype_id`=" . $roomTypeId . ""));
//                 if ($chld_row2['no_of_child'] >= $this->childPerRoom && $this->childPerRoom != 0) {
//                     $child_flag = true;
//                     $childpricesql = mysql_query("SELECT * FROM dots_priceplan WHERE roomtype_id = " . $roomTypeId . " AND capacity_id =1001 AND ('" . $val . "' BETWEEN start_date AND end_date)");
//                     if (mysql_num_rows($pricesql)) {
//                         $chrow = mysql_fetch_assoc($childpricesql);
//                     } else {
//                         $childpricesql2 = mysql_query("SELECT * FROM dots_priceplan WHERE roomtype_id = " . $roomTypeId . " AND capacity_id =1001 AND  default_plan=1");
//                         $chrow = mysql_fetch_assoc($childpricesql2);
//                     }
//                     $day = date('D', strtotime($val));
// //					  $$day+=($chrow[strtolower($day)]*$this->childPerRoom);                                           
//                     $days[] = array($day => $chrow[strtolower($day)] * $this->childPerRoom);
//                 }
            }
            $night_count_at_customprice = 0;
            $searchresult['prices'] = array();
//            $price_details_html.='<tr class="price">';
            if ($bsiCore->config['conf_tax_amount'] > 0 && $bsiCore->config['conf_price_with_tax'] == 1) {
                $price_details_html.='<td style="width: 10px;  background: none; border-bottom: none;"></td>';
//				foreach($dayName as $days => $totalnum){
                foreach ($dayName as $key => $day) {

//					$pricewithtax=$$days+(($$days * $bsiCore->config['conf_tax_amount'])/100);
                    $pricewithtax = $days[$key][$day] + (($days[$key][$day] * $bsiCore->config['conf_tax_amount']) / 100);
                    if ($day == 'Sat' || $day == 'Sun') {
                        $price_details_html.='<td>' . $currency_symbol . number_format($pricewithtax, 2, '.', ',') . '</td>';
                    } else {
                        $price_details_html.='<td>' . $currency_symbol . number_format($pricewithtax, 2, '.', ',') . '</td>';
                    }
                    $totalamt3 = $totalamt3 + number_format($pricewithtax, 2, '.', ',');
                }
            } else {
//                $price_details_html.='<td style="width: 10px;  background: none; border-bottom: none;"></td>';
//				foreach($dayName as $days => $totalnum){ 
                $i = 1;
                $day_count = count($daycollections);
                $available_room = count($searchresult['availablerooms']);
                $max_room_sql = mysql_query("select count(*) as num from dots_room
                            where capacity_id = " . $searchresult['capacityid'] . " and roomtype_id = " . $searchresult['roomtypeid'] .
                        " group by capacity_id, roomtype_id");

                $row = mysql_fetch_assoc($max_room_sql);
                $max_available_room = $row['num'];

                $av_room = mysql_query("select book_date, count(*) as booked from dots_available_room 
                           where room_type_id=" . $searchresult['roomtypeid'] .
                           " and book_date >=CURDATE() and book_date >= '". $this->mysqlCheckInDate .
                           "' GROUP BY book_date, room_type_id");
                
                
                while ($arow = mysql_fetch_assoc($av_room)) {
                    $adata[]=array($arow['book_date']=>$max_available_room - $arow['booked']);
                }
                
                

                foreach ($daycollections as $key => $myday) {
                    

                    if ( $adata[$key][$myday] != '' ) {
                        $available_room = $adata[$key][$myday];                        
                    } else {                        
                        $available_room = $max_available_room;
                    }
                    
                    $day_arr = explode('-', date('M-d-D', strtotime($myday)));
                    $valid_dates[$key]['month'] = $day_arr[0];
                    $valid_dates[$key]['day_num'] = $day_arr[1];
                    $valid_dates[$key]['day'] = $day_arr[2];
                    $day = $day_arr[2];
//                    if ($day == 'Sat' || $day == 'Sun') {
//                        $price_details_html.='<td>' . $currency_symbol . number_format($days[$key][$day], 2, '.', ',') . '</td>';
//                    } else {
                    if ($i % 1 == 1) {
                        $price_details_html .='<tr>';
                    }



                    if ($key == 0) {
                        $mycolor = "bgcolor";
                    } else {
                        $mycolor = '';
                    }

                    $price_details_html .='<td class="'. $mycolor . '"><div class="dategrp"><span class="dayname">' . $day . '</span>';
                    $price_details_html .='<span class="day">' . $day_arr[1] . '</span>';
                    $price_details_html .='<span class="month">' . $day_arr[0] . '</span>';
                    $price_details_html.='<span class="mprice">' . $currency_symbol . number_format($days[$key][$day], 2, '.', ',') . '</span>';
                    $price_details_html.='<span class="available">Available</span>';
                    $price_details_html.='<span class="available_room">' . $available_room . '</span></div></td>';
                    if ($i % 16 == 0) {
                        $price_details_html .='</tr>';
                    }
//                    }

                    if ($key < $actual_day_count) {
                        $totalamt3 = $totalamt3 + $days[$key][$day];
                    }
                    $i++;
                }
//                $price_details_html.='<td style="width: 10px; background: none; border-right: 0;"></td><td style="width: 10px; background: none; border-right: 0;"></td>';
            }
            $total_price_amount = $totalamt3;
            //$price_details_html.='<td bgcolor=' . $color_ . ' align="right">' . $currency_symbol . number_format($total_price_amount, 2, '.', ',') . '</td></tr>';         
        }
        
        $searchresult['roomprice'] = $total_price_amount;
        if ($tmpctr > 1)
            array_push($_SESSION['svars_details'], $searchresult);
        unset($searchresult);

        return array(
            'roomcnt' => $tmpctr - 1,
            'roomdropdown' => $dropdown_html,
            'pricedetails' => $price_details_html,
            'totalprice' => number_format($total_price_amount, 2, '.', ','),
            'child_flag' => $child_flag,
            'total_extrabed' => $total_extrabed
        );
    }
    

}

?>