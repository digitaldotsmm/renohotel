<?php
/**
* @package BSI
* @author BestSoft Inc see README.php
* @copyright BestSoft Inc.
* See COPYRIGHT.php for copyright notices and details.
*/
class BookingProcess
{
	private $guestsPerRoom		= 0;
	private $checkInDate		= '';
	private $checkOutDate		= '';
	private $noOfNights			= 0;
	private $noOfRooms			= 0;
	private $mysqlCheckInDate	= '';
	private $mysqlCheckOutDate	= '';	  
	private $clientdata			= array();		
	private $expTime			= 0;	
	private $roomIdsOnly		= '';
	
	private $pricedata			= array();
	private $taxAmount 			= 0.00;
	private $taxPercent			= 0.00;
	private $grandTotalAmount 	= 0.00;
        private $extrabedcount         =0;
	private $currencySymbol		= '';
        private $extrabedprice = 0;
	
	private $depositenabled		= false;
	
	private $taxWithPrice;
	public $clientId			= 0;
	public $clientName			= '';
	public $clientEmail			= '';
	public $bookingId			= 0;
	public $paymentGatewayCode	= '';		
	public $totalPaymentAmount 	= 0.00;	
	public $invoiceHtml			= '';
        public $bookingDesc = "";
        
	
	function BookingProcess() {				
		$this->setMyRequestParams();
		$this->removeSessionVariables();
		$this->checkAvailability();
		$this->saveClientData();
		$this->saveBookingData();
		$this->createInvoice();
                $this->generateBookingDesc();
	}
	
	private function setMyRequestParams(){ 
            
		$bsiCore = new bsiHotelCore;
		
		$this->setMyParamValue($this->guestsPerRoom, 'SESSION', 'sv_guestperroom', 0, true);	
		$this->setMyParamValue($this->checkInDate, 'SESSION', 'sv_checkindate', NULL, true);
		$this->setMyParamValue($this->checkOutDate, 'SESSION', 'sv_checkoutdate', NULL, true);
		$this->setMyParamValue($this->noOfNights, 'SESSION', 'sv_nightcount', 0, true);
		$this->setMyParamValue($this->mysqlCheckInDate, 'SESSION', 'sv_mcheckindate', NULL, true);
		$this->setMyParamValue($this->mysqlCheckOutDate, 'SESSION', 'sv_mcheckoutdate', NULL, true);
		$this->setMyParamValue($this->roomIdsOnly, 'SESSION', 'dv_roomidsonly', '', true);		
		$this->setMyParamValue($this->reservationdata2, 'SESSION', 'dvars_details2', NULL, true);	
		$this->setMyParamValue($this->reservationdata, 'SESSION', 'dvars_details', NULL, true);						
		$this->setMyParamValue($this->pricedata, 'SESSION', 'dvars_roomprices', NULL, true);		
	
		$this->setMyParamValue($this->clientdata['title'], 'POST', 'title', true); 
		$this->setMyParamValue($this->clientdata['fname'], 'POST', 'fname', '', true);
		$this->setMyParamValue($this->clientdata['lname'], 'POST', 'lname', '', true);
		$this->setMyParamValue($this->clientdata['address'], 'POST', 'str_addr', '', true);
		$this->setMyParamValue($this->clientdata['city'], 'POST', 'city', '', true);
//		$this->setMyParamValue($this->clientdata['state'], 'POST', 'state', '', true);
//		$this->setMyParamValue($this->clientdata['zipcode'], 'POST', 'zipcode', '', true);
		$this->setMyParamValue($this->clientdata['country'], 'POST', 'country', '', true);
		$this->setMyParamValue($this->clientdata['phone'], 'POST', 'phone', '', true);
//		$this->setMyParamValue($this->clientdata['fax'], 'POST', 'fax', '', false); //optionlal
		$this->setMyParamValue($this->clientdata['email'], 'POST', 'email', '', true);
		$this->setMyParamValue($this->clientdata['message'], 'POST', 'message', '', false);
                $this->setMyParamValue($this->clientdata['purpose'], 'POST', 'purpose', '', false);
                $this->setMyParamValue($this->clientdata['checkinhour'], 'POST', 'checkinhour', '', false);
                $this->setMyParamValue($this->clientdata['roomfloor'], 'POST', 'roomfloor', '', false);
		$this->setMyParamValue($this->clientdata['clientip'], 'SERVER', 'REMOTE_ADDR', '', false);
		$this->setMyParamValue($this->paymentGatewayCode, 'POST', 'payment_type','', true);	
		
		$this->bookingId		= time();
		$this->expTime 			= intval($bsiCore->config['conf_booking_exptime']);	
		$this->currencySymbol 	= $bsiCore->config['conf_currency_symbol'];
		$this->taxPercent 		= $bsiCore->config['conf_tax_amount'];
		$this->clientName 		= $this->clientdata['fname']." ". $this->clientdata['lname'];
		$this->clientEmail		= $this->clientdata['email'];
		$this->noOfRooms		= count(explode(",", $this->roomIdsOnly));
		$this->taxWithPrice     = $bsiCore->config['conf_price_with_tax'];
			
		if($bsiCore->config['conf_enabled_deposit'])
			$this->depositenabled = true;			
		
		$this->taxAmount 			= $this->pricedata['totaltax'];
		$this->grandTotalAmount 	= $this->pricedata['grandtotal'];
		$this->totalPaymentAmount 	= $this->pricedata['advanceamount']; 
                //$this->extrabedcount           =  $this->pricedata['extrabedcount'];
                 if(isset($_POST['extrabed_count'])){
                     $this->extrabedcount =  $this->setMyParamValue($this->clientdata['extrabed_count'], 'POST', 'extrabed_count', '', false);
                 }
                
                $this->extrabedprice  = $this->pricedata['extrabedprice'];
		
	}
	
	private function setMyParamValue(&$membervariable, $vartype, $param, $defaultvalue, $required = false){
		$bsiCore = new  bsiHotelCore;
		switch($vartype){
			case "POST": 
				if($required){if(!isset($_POST[$param])){$this->invalidRequest(9);} 
     else{$membervariable = $bsiCore->ClearInput($_POST[$param]);}}
    else{if(isset($_POST[$param])){$membervariable = $bsiCore->ClearInput($_POST[$param]);} 
     else{$membervariable = $defaultvalue;}}    
    break;
			case "GET":
				if($required){if(!isset($_GET[$param])){$this->invalidRequest(9);} 
					else{$membervariable = $bsiCore->ClearInput($_GET[$param]);}}
				else{if(isset($_GET[$param])){$membervariable = $bsiCore->ClearInput($_GET[$param]);} 
					else{$membervariable = $defaultvalue;}}				
				break;	
			case "SESSION":			  
				if($required){if(!isset($_SESSION[$param])){$this->invalidRequest(9);} 
     				else{$membervariable = $_SESSION[$param];}}
    			else{if(isset($_SESSION[$param])){$membervariable = $_SESSION[$param];} 
     						else{$membervariable = $defaultvalue;}}    
    			break;
			case "REQUEST":
				if($required){if(!isset($_REQUEST[$param])){$this->invalidRequest(9);}
					else{$membervariable = $bsiCore->ClearInput($_REQUEST[$param]);}}
				else{if(isset($_REQUEST[$param])){$membervariable = $bsiCore->ClearInput($_REQUEST[$param]);}
					else{$membervariable = $defaultvalue;}}				
				break;
			case "SERVER":
				if($required){if(!isset($_SERVER[$param])){$this->invalidRequest(9);}
					else{$membervariable = $_SERVER[$param];}}
				else{if(isset($_SERVER[$param])){$membervariable = $_SERVER[$param];}
					else{$membervariable = $defaultvalue;}}				
				break;	
		}		
	}	
	
	private function invalidRequest($errocode = 9){	
		header('Location: ?error_code='.$errocode.'');
		die;
	}
	
	private function removeSessionVariables(){
		if(isset($_SESSION['sv_checkindate'])) unset($_SESSION['sv_checkindate']);
		if(isset($_SESSION['sv_checkoutdate'])) unset($_SESSION['sv_checkoutdate']);
		if(isset($_SESSION['sv_mcheckindate'])) unset($_SESSION['sv_mcheckindate']);
		if(isset($_SESSION['sv_mcheckoutdate'])) unset($_SESSION['sv_mcheckoutdate']);	
		if(isset($_SESSION['sv_nightcount'])) unset($_SESSION['sv_nightcount']);
		if(isset($_SESSION['sv_guestperroom'])) unset($_SESSION['sv_guestperroom']);
		if(isset($_SESSION['sv_childcount'])) unset($_SESSION['sv_childcount']);
                if(isset($_SESSION['sv_extrabedcount'])) unset($_SESSION['sv_extrabedcount']);
		if(isset($_SESSION['svars_details'])) unset($_SESSION['svars_details']);
		if(isset($_SESSION['dvars_details'])) unset($_SESSION['dvars_details']);
		if(isset($_SESSION['dv_roomidsonly'])) unset($_SESSION['dv_roomidsonly']);	
		if(isset($_SESSION['dvars_roomprices'])) unset($_SESSION['dvars_roomprices']);
	}
	 
	/* Check Immediate Booking Status For Concurrent Access */
	private function checkAvailability(){
		$sql = "
		SELECT resv.room_id
		  FROM dots_reservation resv, dots_bookings boks
		 WHERE     resv.bookings_id = boks.booking_id
			   AND ((NOW() - boks.booking_time) < ".$this->expTime.")
			   AND boks.is_deleted = FALSE
			   AND resv.room_id IN (".$this->roomIdsOnly.")
			   AND (('".$this->mysqlCheckInDate."' BETWEEN boks.start_date AND DATE_SUB(boks.end_date, INTERVAL 1 DAY))
				OR (DATE_SUB('".$this->mysqlCheckOutDate."', INTERVAL 1 DAY) BETWEEN boks.start_date AND DATE_SUB(boks.end_date, INTERVAL 1 DAY))
				OR (boks.start_date BETWEEN '".$this->mysqlCheckInDate."' AND DATE_SUB('".$this->mysqlCheckOutDate."', INTERVAL 1 DAY))
				OR (DATE_SUB(boks.end_date, INTERVAL 1 DAY) BETWEEN '".$this->mysqlCheckInDate."' AND DATE_SUB('".$this->mysqlCheckOutDate."', INTERVAL 1 DAY)))";				
		$sql = mysql_query($sql);
		if(mysql_num_rows($sql)){	
			mysql_free_result($sql);
			$this->invalidRequest(13);
			die;
		}
		mysql_free_result($sql);
	}
	
	private function saveClientData(){
		$sql1 = mysql_query("SELECT client_id FROM dots_clients WHERE email = '".$this->clientdata['email']."'");
		if(mysql_num_rows($sql1) > 0){
			$clientrow = mysql_fetch_assoc($sql1);
			$this->clientId = $clientrow["client_id"];	
			$sql2 = mysql_query("UPDATE dots_clients SET first_name = '".$this->clientdata['fname']."', surname = '".$this->clientdata['lname']."', title = '".$this->clientdata['title']."', street_addr = '".$this->clientdata['address']."', city = '".$this->clientdata['city']."' , country = '".$this->clientdata['country']."', phone = '".$this->clientdata['phone']."', fax = '".$this->clientdata['fax']."', additional_comments = '".$this->clientdata['message']."', ip = '".$this->clientdata['clientip']."' WHERE client_id = ".$this->clientId);				
		}else{
                        $sql2 = mysql_query("INSERT INTO dots_clients (first_name, surname, title, street_addr, city, country, phone, email, additional_comments, ip) values('".$this->clientdata['fname']."', '".$this->clientdata['lname']."', '".$this->clientdata['title']."', '".$this->clientdata['address']."', '".$this->clientdata['city']."' , '".$this->clientdata['country']."', '".$this->clientdata['phone']."','".$this->clientdata['email']."', '".$this->clientdata['message']."', '".$this->clientdata['clientip']."')");
			$this->clientId = mysql_insert_id();			
		}
		mysql_free_result($sql1);		
	}
	
	private function saveBookingData(){
         
                $total_extrabedcount = 0;
                if(isset($_POST['extrabed_count'])){
                    $total_extrabedcount = $_POST['extrabed_count'];
                }
                $sql = mysql_query("INSERT INTO dots_bookings (booking_id, booking_time, start_date, end_date, client_id, total_cost, payment_amount, payment_type, special_requests,extrabed_count,extrabed_price,purpose_trip) values(" . $this->bookingId . ", NOW(), '" . $this->mysqlCheckInDate . "', '" . $this->mysqlCheckOutDate . "', " . $this->clientId . ", " . $this->grandTotalAmount . ", " . $this->totalPaymentAmount . ", '" . $this->paymentGatewayCode . "', '" . $this->clientdata['message'] . "','" .$total_extrabedcount."', '".$this->extrabedprice . "', '" . $this->clientdata['purpose'] . "')");
	   
               
                foreach($this->reservationdata as $revdata){
			foreach($revdata['availablerooms'] as $rooms){
									
			$sql = mysql_query("INSERT INTO dots_reservation (bookings_id, room_id, room_type_id) values(".$this->bookingId.",  ".$rooms['roomid'].", ".$revdata['roomtypeid'].")");
                     
                        //insert into availabel room id
                        $checkInDate=strtotime($this->mysqlCheckInDate);
                        $checkoutDate=strtotime("-1 day", strtotime ( $this->mysqlCheckOutDate ));
                        for ($i = $checkInDate; $i <= $checkoutDate; $i = $i + 86400) {
                            $thisDate = date('Y-m-d', $i); 
                            $sql=mysql_query("INSERT INTO dots_available_room(booking_id,book_date,room_type_id) VAlUES (".$this->bookingId.",'".$thisDate."',".$revdata['roomtypeid'].")");
                        }
                     } 
		}
	}	
	
	private function createInvoice(){
		$bsiCore = new bsiHotelCore;
                if(isset($_POST['extrabed_count'])){
                    $total_extrabedcount = $_POST['extrabed_count'];
                }
                var_dump($this->extrabed_price);
                $bookingROWS = mysql_fetch_assoc(mysql_query("SELECT * FROM dots_bookings WHERE booking_id='" . $this->bookingId . "'"));
                $this->invoiceHtml = '<center class="wrapper" style="display: table;table-layout: fixed;width: 100%;min-width: 1000px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #f6f9fb">
                        <table class="preheader" style="border-collapse: collapse;border-spacing: 0;width: 100%;background-color: #f6f9fb">
                            <tbody>
                                <tr>
                                    <td class="title" style="padding: 8px 32px;vertical-align: top;font-size: 11px;font-weight: 400;letter-spacing: 0.01em;line-height: 17px;width: 50%;color: #b3b3b3;text-align: left;font-family: sans-serif"></td>          
                                </tr>
                            </tbody>
                        </table>
                        <table class="header" style="border-collapse: collapse;border-spacing: 0;width: 100%;background-color: #f6f9fb">
                          <tbody>
                              <tr>
                                <td style="padding: 0;vertical-align: top">&nbsp;</td>
                                <td class="logo" style="padding: 0;vertical-align: top;mso-line-height-rule: at-least;padding-top: 16px;padding-bottom: 32px;width: 1000px"><div class="logo-center" style="font-size: 15px;letter-spacing: -0.01em;line-height: 14px;color: #2e3b4e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header">
                                        <h1>'.$this->clientdata['title'].$this->clientdata['fname'] .' '. $this->clientdata['lname'] .'</h1>
                                        <h2>'.$this->noOfRooms. ' room - ' .$this->currencySymbol.$this->pricedata['grandtotal'] . '</h2>
                                        <p>'.$this->clientdata['address'].','.$this->clientdata['city'].','.$this->clientdata['country'].'</p>
                                        <p>'.$this->clientdata['email'].'</p>
                                        <p>'.date('l, M, Y - h:m:s').' - #'.$this->bookingId.'</p>
                                        <br/>
                                        <p><h2>Reno Hotel Myanmar  - #'.$this->bookingId.'</h2></p>
                                        <p>This guest is travelling for <b>'.$this->clientdata['purpose'].'</b></p></div></td>
                                <td style="padding: 0;vertical-align: top">&nbsp;</td>
                              </tr>
                            </tbody>
                        </table>

                        <table class="two-col-bg" style="border-collapse: collapse;border-spacing: 0;width: 100%;background-color: #ebf2f6">
                            <tbody>
                                <tr>
                                <td style="padding: 0;vertical-align: top" align="center">
                                    <table class="two-col centered" style="border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 1000px">
                                        <tbody>
                                            <tr>
                                                <td class="column first" style="padding: 0;vertical-align: top;text-align: left;width: 300px">
                                                  <div><div class="column-top" style="font-size: 52px;line-height: 52px">&nbsp;</div></div>
                                                    <table class="contents" style="border-collapse: collapse;border-spacing: 0;width: 100%;font-size: 15px;">
                                                      <tbody>
                                                          <tr><td><h3>'.$this->clientdata['noOfRooms']. ' Room</h3></td></tr>
                                                    <tr><td>Guest Name</td><td>'.$this->clientdata['title'].$this->clientdata['fname'] .' '. $this->clientdata['lname'] .'</td></tr>
                                                    <tr><td>Check In</td><td>'.$this->mysqlCheckInDate.'</td></tr>
                                                    <tr><td>Check Out</td><td>'.$this->mysqlCheckOutDate.'</td></tr>
                                                    <tr><td>Number of nights</td><td>'.$this->noOfNights.'</td></tr>';                                   
                                                    if($this->extrabedprice > 0){
                                                    $this->invoiceHtml .= '<tr><td>Number of Extra Bed</td><td>'.$total_extrabedcount.'</td></tr>';
                                                    }
                                                    $this->invoiceHtml .= '<tr><td>&nbsp;</td></tr> 
                                                    <tr><td>Smoking Performance</td><td>no smoking</td></tr>
                                                    <tr><td>Applicable</td><td colspan="2">If cancelled or modified up to 7days before date of arrival,no fee will be charged</td></tr>
                                                    <tr><td>Cancellation Policy</td><td colspan="2">If cancelled or modified later or in case of no show, the total price of the reservation will be charged.</td></tr>
                                                    <tr><td>Applicable Deposit Policy</td><td colspan="2">No deposit will be charged.</td></tr>
                                                    <tr><td>Applicable Meal Plan</td><td colspan="2">Breakfast is included in room rate.</td></tr>
                                                    <tr><td>&nbsp;</td></tr> 
                                                    <tr>
                                                        <td colspan="3">
                                                        <b>
                                                        <br>Regards
                                                        <br> Reno Hotel Myanmar
                                                        <br>No.123, Kaba Aye Pagoda Road, Bahan Township, Yangon. 
                                                        <br>+959  255 913 800 ~ 3 , +959 799 992 333 ~ 6  
                                                        </b>
                                                        <br><br>

                                                    </td>
                                                    </tr>
                                                    </tbody>
                                                  </table>                       
                                                  <div class="column-bottom" style="font-size: 30px;line-height: 30px">&nbsp;</div>
                                                </td>
                                            <td class="column second" style="padding: 0;vertical-align: top;text-align: left;width: 300px">
                                              <div><div class="column-top" style="font-size: 52px;line-height: 52px">&nbsp;</div></div>
                                                <table class="contents" style="border-collapse: collapse;border-spacing: 0;width: 100%;font-size: 15px;">
                                                    <tbody>';
                                                     foreach($this->reservationdata2 as $revdata){
                                                        //$child_title2=($revdata['child_flag2'])? " + ".$revdata['childcount3']." ".CHILD_TEXT." ":"";
                                                        $this->invoiceHtml .= '<tr><td colspan="2"><h3>'.$revdata['roomtype'].' - '.$revdata['roomno'].' rooms </h3></td></tr>                                   
                                                        <tr><td width="200">Number of persons</td><td>'.$revdata['capacity'].'</td></tr>';        
                                                        if($this->extrabedprice > 0){
                                                            $pricebyoneextra_bedprice = $this->extrabedprice / $total_extrabedcount;
                                                            $room_price = $revdata['grosstotal'] - $this->extrabedprice;
                                                            $this->invoiceHtml .= '<tr><td>'.$revdata['roomtype'].' - '.$revdata['roomno'].' rooms - </td> <td> '.$this->currencySymbol.number_format($room_price, 2 , '.', ',').'</td></tr>';
                                                            $this->invoiceHtml .= '<tr><td>Extra Bed ( ' . $bsiCore->config['conf_currency_symbol'] . number_format($pricebyoneextra_bedprice, 2, '.', ',').' each ) x '.$total_extrabedcount .'</td><td>'.$this->currencySymbol.number_format($this->extrabedprice, 2 , '.', ',').'</td></tr>';
                                                        }else{
                                                           $this->invoiceHtml .= '<tr><td>'.$revdata['roomtype'].' - '.$revdata['roomno'].' rooms - </td><td>'.$this->currencySymbol.number_format($revdata['grosstotal'], 2 , '.', ',').'</td></tr>'; 
                                                        }
                                                        $this->invoiceHtml .= '<tr><td>Total cost confirmed</td><td>Room price</td><td> '.$this->currencySymbol.number_format($revdata['grosstotal'], 2 , '.', ',').'  </td></tr>
                                                        <tr><td></td><td>Total Confirm to guest</td><td>'.$this->currencySymbol.number_format($revdata['grosstotal'], 2 , '.', ',').'</td></tr>
                                                        <tr><td>Status</td><td>OK</td></tr>
                                                        <tr><td colspan="3"><b><i><font color="#eb7507">( Total costs have 10% service charge and 5% tax. )</font></i></b></td></tr> ';                                                  
                                                    }
                                                    $this->invoiceHtml .='</tbody>
                                                </table>
                                                <div class="column-bottom" style="font-size: 30px;line-height: 30px">&nbsp;</div>
                                            </td>
                                        </tr>
                                      </tbody>
                                </table>
                                </td>
                              </tr>
                            </tbody>
                        </table>

                        <table class="footer" style="border-collapse: collapse;border-spacing: 0;width: 100%;background-color: #f6f9fb">
                          <tbody><tr>
                            <td class="inner" style="padding: 0;vertical-align: top;padding-top: 60px;padding-bottom: 55px" align="center">
                              <table class="cols" style="border-collapse: collapse;border-spacing: 0;width: 1000px">
                                <tbody><tr>
                                  <td class="left" style="padding: 0;vertical-align: top;font-size: 11px;font-weight: 400;letter-spacing: 0.01em;line-height: 17px;padding-bottom: 22px;text-align: left;width: 55%;padding-right: 5px;color: #b3b3b3;font-family: sans-serif">
                                    <font style="color:#eb7507; font-size:13px;">You will need to carry a print out of this e-mail and present it to the hotel on arrival and check-in. This e-mail is the confirmation voucher for your booking.</font>

                                  </td>

                                </tr>
                              </tbody></table>
                            </td>
                          </tr>
                        </tbody></table>
                      </center>';
              
		//$this->invoiceHtml.= '<tr height="35px;"><td colspan="3" align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.TOTAL_TEXT.'</td><td align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.$this->currencySymbol.number_format($this->pricedata['subtotal'], 2 , '.', ',').'</td></tr>';
					
		if($this->taxPercent > 0 &&  $this->taxWithPrice == 0){ 	
		//$this->invoiceHtml.= '<tr height="35px;"><td colspan="3" align="right" style="background:#ffffff;">'.TAX_TEXT.'('.number_format($this->taxPercent, 2 , '.', '').'%)</td><td align="right" style="background:#ffffff;">(+) '.$this->currencySymbol.number_format($this->taxAmount, 2 , '.', ',').'</td></tr><tr height="35px;"><td colspan="3" align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.GRAND_TOTAL_TEXT.'</td><td align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.$this->currencySymbol.number_format($this->grandTotalAmount, 2 , '.', ',').'</td></tr>';
		}else{
			//$this->invoiceHtml.= '<tr height="35px;"><td colspan="3" align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.GRAND_TOTAL_TEXT.'</td><td align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.$this->currencySymbol.number_format($this->grandTotalAmount, 2 , '.', ',').'</td></tr>';	
		}
		if($this->depositenabled && ($this->pricedata['advancepercentage'] > 0 && $this->pricedata['advancepercentage'] < 100)){
			
			//$this->invoiceHtml.= '<tr height="35px;"><td colspan="3" align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.ADVANCE_PAYMENT_TEXT.'(<span style="font-size: 10px;">'.number_format($this->pricedata['advancepercentage'], 2 , '.', '').'% '.GRAND_TOTAL_TEXT.' </span>)</td><td align="right" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.$this->currencySymbol.number_format($this->totalPaymentAmount, 2 , '.', ',').'</td></tr>';
		} 
		//$this->invoiceHtml.= '</tbody></table>';
		
		if($this->paymentGatewayCode == "poa"){
                    
			$payoptions = $bsiCore->paymentGatewayName("poa");
                        echo $payoptions;
			$this->invoiceHtml.= '<br /><table  style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; background:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr height="35px;"><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#EEDFFF">'.INV_PAY_DETAILS.'</td></tr><tr height="35px;"><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps;background:#ffffff;">'.INV_PAY_OPTION.'</td><td align="left" style="background:#ffffff;">'.$payoptions.'</td></tr><tr height="35px;"><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff;">'.INV_TXN_ID.'</td><td align="left" style="background:#ffffff;">'.NA_TEXT.'</td></tr></table>';					
		}
		
                
		/* insert the invoice data in dots_invoice table */
		$insertInvoiceSQL = mysql_query("INSERT INTO dots_invoice(booking_id, client_name, client_email, invoice) values(".$this->bookingId.", '".$this->clientName."', '".$this->clientdata['email']."', '".$this->invoiceHtml."')");	
	}
        private function generateBookingDesc(){
            $desc = array();                          
            foreach ($this->reservationdata2 as $r) {                
                $desc[] = $r['roomtype'] ;
            }
         
            $desc = implode(' / ', $desc);
            $desc = $desc ;
            //$this->bookingDesc = 'Total '.$this->noOfRooms .' Rooms Booking'  ;
            $this->bookingDesc = $desc.' Rooms x'.$this->noOfRooms.'';
        }
}
?>