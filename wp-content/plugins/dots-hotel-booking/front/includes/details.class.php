<?php
 
 /**
 
 * @package BSI
 
 * @author BestSoft Inc see README.php
 
 * @copyright BestSoft Inc.
 
 * See COPYRIGHT.php for copyright notices and details.
 
 */
 
 
 
 class bsiBookingDetails
 
 {
 
	public $guestsPerRoom      = 0;			
 
	public $nightCount         = 0;	
 
	public $checkInDate        = '';
 
	public $checkOutDate       = '';	
 
	public $totalRoomCount     = 0;	
 
	public $roomPrices         = array();
 
	public $depositPlans       = array();
 
	
 
	private $selectedRooms     = '';
 
	private $mysqlCheckInDate  = '';
 
	private $mysqlCheckOutDate = '';
        
        private $extra_bed = '';
 
	private $searchVars        = array();
 
	private $detailVars	       = array();
 
 
 
	function bsiBookingDetails() {	
		$this->setRequestParams();			
 
		$this->advancePayment();
 
	}		
 
	
 
	private function setRequestParams() {	
 
		/**
 
		 * Global Ref: conf.class.php
 
		 **/
 
		$bsiCore = new bsiHotelCore;	
 
		
 
		$this->setMyParamValue($this->guestsPerRoom, 'SESSION', 'sv_guestperroom', NULL, true);		
 
		$this->setMyParamValue($this->checkInDate, 'SESSION', 'sv_checkindate', NULL, true);
 
		$this->setMyParamValue($this->mysqlCheckInDate, 'SESSION', 'sv_mcheckindate', NULL, true);
 
		$this->setMyParamValue($this->checkOutDate, 'SESSION', 'sv_checkoutdate', NULL, true);
 
		$this->setMyParamValue($this->mysqlCheckOutDate, 'SESSION', 'sv_mcheckoutdate', NULL, true);
 
		$this->setMyParamValue($this->nightCount, 'SESSION', 'sv_nightcount', NULL, true);		
 
		$this->setMyParamValue($this->searchVars, 'SESSION', 'svars_details', NULL, true);
 
		$this->setMyParamValue($this->selectedRooms, 'POST_SPECIAL', 'svars_selectedrooms', NULL, true);               
                
 
		$selected = 0;
 
		
 
		foreach($this->selectedRooms as &$val){		
 
			$val = $bsiCore->ClearInput($val); if($val) $selected++;
 
		}			
 
		if($selected == 0) $this->invalidRequest(9);				
 
	}
 
	
 
	private function setMyParamValue(&$membervariable, $vartype, $param, $defaultvalue, $required = false){
 
		$bsiCore = new bsiHotelCore;
	
		switch($vartype){
 
			case "POST": 
 
				if($required){if(!isset($_POST[$param])){$this->invalidRequest(9);} 
 
					else{$membervariable = $bsiCore->ClearInput($_POST[$param]);}}
 
				else{if(isset($_POST[$param])){$membervariable = $bsiCore->ClearInput($_POST[$param]);} 
 
					else{$membervariable = $defaultvalue;}}				
 
				break;	
 
			case "POST_SPECIAL":
 
				if($required){if(!isset($_POST[$param])){$this->invalidRequest(9);}
 
					else{$membervariable = $_POST[$param];}}
 
				else{if(isset($_POST[$param])){$membervariable = $_POST[$param];}
 
					else{$membervariable = $defaultvalue;}}				
 
				break;	
 
			case "GET":
 
				if($required){if(!isset($_GET[$param])){$this->invalidRequest(9);} 
 
					else{$membervariable = $bsiCore->ClearInput($_GET[$param]);}}
 
				else{if(isset($_GET[$param])){$membervariable = $bsiCore->ClearInput($_GET[$param]);} 
 
					else{$membervariable = $defaultvalue;}}				
 
				break;	
 
			case "SESSION":
 
				if($required){if(!isset($_SESSION[$param])){$this->invalidRequest(9);} 
 
					else{$membervariable = $_SESSION[$param];}}
 
				else{if(isset($_SESSION[$param])){$membervariable = $_SESSION[$param];} 
 
					else{$membervariable = $defaultvalue;}}				
 
				break;	
 
			case "REQUEST":
 
				if($required){if(!isset($_REQUEST[$param])){$this->invalidRequest(9);}
 
					else{$membervariable = $bsiCore->ClearInput($_REQUEST[$param]);}}
 
				else{if(isset($_REQUEST[$param])){$membervariable = $bsiCore->ClearInput($_REQUEST[$param]);}
 
					else{$membervariable = $defaultvalue;}}				
 
				break;
 
			case "SERVER":
 
				if($required){if(!isset($_SERVER[$param])){$this->invalidRequest(9);}
 
					else{$membervariable = $_SERVER[$param];}}
 
				else{if(isset($_SERVER[$param])){$membervariable = $_SERVER[$param];}
 
					else{$membervariable = $defaultvalue;}}				
 
				break;	
 
					
 
		}		
 
	}	
 
	
 
	private function advancePayment(){
 
		$month  = intval(substr($this->mysqlCheckInDate, 5, 2)) ;
 
		$result = mysql_query("SELECT * FROM dots_advance_payment WHERE month_num = ".$month);
 
		$this->depositPlans = mysql_fetch_assoc($result);		
 
		mysql_free_result($result);	
 
	}
 
	
 
	private function invalidRequest($errocode = 9){		
		header('Location: booking-failure.php?error_code='.$errocode.'');
	}	
 
	
 
	public function generateBookingDetails() {
 
		$bsiCore = new bsiHotelCore;
                
		$result = array();
 
		$_SESSION['dvars_details2'] = array();
 
		$dvroomidsonly = "";
 
		$selectedRoomsCount = count($this->selectedRooms);
                
                $nightcount= $this->nightCount;
                
		$this->roomPrices['subtotal']   = 0.00;	
 
		$this->roomPrices['totaltax']   = 0.00;			
 
		$this->roomPrices['grandtotal'] = 0.00;
                
                $this->roomPrices['extrabedprice']=0.00;
                
                $this->roomPrices['extrabedcount']=0.00;
                
                $this->roomPrices['excludebreakfast']=0.00;
                
                //
                if(isset($_REQUEST['extra_bed'])){
                $checked_arr = $_REQUEST['extra_bed'];
                
                $extrabedcount = count($checked_arr);
                
                foreach($checked_arr as $extravalue){
                    $bedprice = $extravalue;
                }
                
                }
                
                
                if(isset($_REQUEST['excludebreakfast'])){
                    $excludebreakfast = $_REQUEST['excludebreakfast'];
                }
 
		$dvarsCtr = 0;
 
		for($i = 0; $i < $selectedRoomsCount; $i++){
 
			if($this->selectedRooms[$i] > 0){		
 
				$this->detailVars[$dvarsCtr] = $this->searchVars[$i]; //selected only
                                
                                
                               
				$tmpTotalPrice = 0;
 
				$tmpTotalPrice2 = 0;
                                
                                if(isset($bedprice)){
                               
                                $totalextrabedprice=  $extrabedcount * $bedprice;
                                
                              
                                
                                $this->roomPrices['extrabedprice']=$selectedRoomsCount*$totalextrabedprice;
                                
                                }
                                
                                if(isset($excludebreakfast)){
                                
                                $this->roomPrices['excludebreakfast']=$selectedRoomsCount*$excludebreakfast;
                                
                                }
                                
				$tmpTotalPrice = $this->detailVars[$dvarsCtr]['roomprice'];
 
				$this->detailVars[$dvarsCtr]['totalprice'] = $tmpTotalPrice;
                                
				$tmpRoomCounter = 0;								
 
				foreach($this->detailVars[$dvarsCtr]['availablerooms'] as $availablerooms){
                                    
//                                    if(isset($excludebreakfast)){
//                                    
//                                        $this->roomPrices['excludebreakfast']=$selectedRoomsCount*$excludebreakfast;
//                                    }
                                    
//                                    if(isset($extrabedcount)){
//                                    
//                                        $this->roomPrices['extrabedprice']=$selectedRoomsCount*$bedprice;
//                                        
//                                        var_dump($this->roomPrices['extrabedprice']);
//                                    }
                                    
                                    if(isset($totalextrabedprice) && isset($excludebreakfast)){
 
					$this->roomPrices['subtotal'] = (($this->roomPrices['subtotal'] + $tmpTotalPrice) + $totalextrabedprice) - $this->roomPrices['excludebreakfast'];
                                    }elseif(isset($totalextrabedprice)){
                                        
                                        $this->roomPrices['subtotal'] = (($this->roomPrices['subtotal'] + $tmpTotalPrice) + $this->roomPrices['extrabedprice']);
                                    }elseif(isset($excludebreakfast)){
                                        $this->roomPrices['subtotal'] = (($this->roomPrices['subtotal'] + $tmpTotalPrice)- $this->roomPrices['excludebreakfast']);
                                    }else{
                                        $this->roomPrices['subtotal'] = $this->roomPrices['subtotal'] + $tmpTotalPrice;
                                    }
 
					$dvroomidsonly.= $availablerooms['roomid'].",";													
 
					$tmpRoomCounter++;	
 
					if($tmpRoomCounter == $this->selectedRooms[$i]){
 
						$tmpAvRmSize = count($this->detailVars[$dvarsCtr]['availablerooms']);
 
						for($akey = $tmpRoomCounter; $akey < $tmpAvRmSize; $akey++){
 
							unset($this->detailVars[$dvarsCtr]['availablerooms'][$akey]);
 
						}
 
						break;		
 
					}			
 
				}
				
				$child_flag2=false;
				$chld_row2=mysql_fetch_assoc(mysql_query("SELECT distinct(`no_of_child`) FROM `dots_room` WHERE `capacity_id`=".$this->detailVars[$dvarsCtr]['capacityid']." and `roomtype_id`=".$this->detailVars[$dvarsCtr]['roomtypeid'].""));
				if($chld_row2['no_of_child'] >= $_SESSION['sv_childcount'] && $_SESSION['sv_childcount'] != 0){
					$child_flag2=true;
				}
                                
                                
                                
                                
//                                $extrabed_flag2=false;
//				$extrabed_row2=mysql_fetch_assoc(mysql_query("SELECT distinct(`extra_bed`) FROM `dots_room` WHERE `capacity_id`=".$this->detailVars[$dvarsCtr]['capacityid']." and `roomtype_id`=".$this->detailVars[$dvarsCtr]['roomtypeid'].""));
//				if($extrabed_row2['extra_bed'] >= $_SESSION['sv_extrabedcount'] && $_SESSION['sv_extracount'] != 0){
//					$extrabed_flag2=true;
//				}
                                
				if(isset($extrabedcount) && isset($excludebreakfast)){
                                array_push($result, array('roomno'=>$tmpRoomCounter, 'roomtype'=>$this->detailVars[$dvarsCtr]['roomtypename'], 'capacitytitle'=>$this->detailVars[$dvarsCtr]['capacitytitle'] ,'capacity'=>$this->detailVars[$dvarsCtr]['capacity'], 'details'=>$tmpRoomCounter."x".$tmpTotalPrice, 'grosstotal'=>(($tmpRoomCounter*$tmpTotalPrice)+$this->roomPrices['extrabedprice'])- ($tmpRoomCounter*$this->roomPrices['excludebreakfast']), 'child_flag2'=>$child_flag2, 'childcount3'=>$_SESSION['sv_childcount'],'extrabedcount'=>$extrabedcount,'extrabedmoney'=>$this->roomPrices['extrabedprice'] ));
                                }elseif(isset($extrabedcount)){
                                    array_push($result, array('roomno'=>$tmpRoomCounter, 'roomtypeid' => 37, 'roomtype'=>$this->detailVars[$dvarsCtr]['roomtypename'], 'capacitytitle'=>$this->detailVars[$dvarsCtr]['capacitytitle'] ,'capacity'=>$this->detailVars[$dvarsCtr]['capacity'], 'details'=>$tmpRoomCounter."x".$tmpTotalPrice, 'grosstotal'=>(($tmpRoomCounter*$tmpTotalPrice)+$this->roomPrices['extrabedprice']), 'child_flag2'=>$child_flag2, 'childcount3'=>$_SESSION['sv_childcount'],'extrabedcount'=>$extrabedcount,'extrabedmoney'=>$this->roomPrices['extrabedprice'] ));
                                }elseif(isset($excludebreakfast)){
                                    array_push($result, array('roomno'=>$tmpRoomCounter, 'roomtype'=>$this->detailVars[$dvarsCtr]['roomtypename'], 'capacitytitle'=>$this->detailVars[$dvarsCtr]['capacitytitle'] ,'capacity'=>$this->detailVars[$dvarsCtr]['capacity'], 'details'=>$tmpRoomCounter."x".$tmpTotalPrice, 'grosstotal'=>(($tmpRoomCounter*$tmpTotalPrice))- ($tmpRoomCounter*$this->roomPrices['excludebreakfast']), 'child_flag2'=>$child_flag2, 'childcount3'=>$_SESSION['sv_childcount'] ));
                                }
                                else{
                                array_push($result, array('roomno'=>$tmpRoomCounter, 'roomtype'=>$this->detailVars[$dvarsCtr]['roomtypename'], 'capacitytitle'=>$this->detailVars[$dvarsCtr]['capacitytitle'] ,'capacity'=>$this->detailVars[$dvarsCtr]['capacity'], 'details'=>$tmpRoomCounter."x".$tmpTotalPrice, 'grosstotal'=>$tmpRoomCounter*$tmpTotalPrice, 'child_flag2'=>$child_flag2, 'childcount3'=>$_SESSION['sv_childcount']));
                                }
				$dvarsCtr++;				
 
			}
 
		}
                
                
 
		
 
		
 
		if(isset($_SESSION['dvars_details']))unset($_SESSION['dvars_details']);
 
		$_SESSION['dvars_details'] = $this->detailVars;
 
		
 
		if(isset($_SESSION['dvars_details2']))unset($_SESSION['dvars_details2']);
 
		$_SESSION['dvars_details2'] = $result;
 
				
 
		if(isset($_SESSION['dv_roomidsonly']))unset($_SESSION['dv_roomidsonly']);
 
		$_SESSION['dv_roomidsonly'] = substr($dvroomidsonly, 0, -1);
 
			
 
		$this->totalRoomCount =  count(explode(",", $_SESSION['dv_roomidsonly']));
                
                
 
		
 
		
 
		/* -------------------------------- calculate pricing ------------------------------------ */	
 
											
 
		if($bsiCore->config['conf_tax_amount'] > 0 &&  $bsiCore->config['conf_price_with_tax']==0){
 
			$this->roomPrices['totaltax'] = ($this->roomPrices['subtotal'] * $bsiCore->config['conf_tax_amount'])/100;
 
			$this->roomPrices['grandtotal'] = $this->roomPrices['subtotal'] + $this->roomPrices['totaltax'];
 
		}else{
			$this->roomPrices['grandtotal'] = $this->roomPrices['subtotal'];
                        
		}
 
		
 
		$this->roomPrices['advanceamount'] = $this->roomPrices['grandtotal'];
 
		if($bsiCore->config['conf_enabled_deposit']){
 
			$this->roomPrices['advancepercentage'] = $this->depositPlans['deposit_percent'];			
 
			if($this->roomPrices['advancepercentage'] > 0 && $this->roomPrices['advancepercentage'] < 100){
 
				$this->roomPrices['advanceamount'] = ($this->roomPrices['grandtotal'] * $this->roomPrices['advancepercentage'])/100;
 
			}
 
		}
 
		
 
		//format currencies round upto 2 decimal places		
 
		$this->roomPrices['subtotal'] = number_format($this->roomPrices['subtotal'], 2 , '.', '');	
 
		$this->roomPrices['totaltax'] = number_format($this->roomPrices['totaltax'], 2 , '.', '');			
 
		$this->roomPrices['grandtotal'] = number_format($this->roomPrices['grandtotal'], 2 , '.', '');
                
                $this->roomPrices['extrabedprice'] = number_format($this->roomPrices['extrabedprice'], 2 , '.', '');
                if(isset($excludebreakfast)){
                
                $this->roomPrices['extrabedbreakfast'] = number_format($this->roomPrices['extrabedbreakfast'], 2 , '.', '');
                }
                
                if(isset($extrabedcount)){
                
                $this->roomPrices['extrabedcount']= $extrabedcount;
                
                }
 
		if($bsiCore->config['conf_enabled_deposit']){	
 
		$this->roomPrices['advancepercentage'] = number_format($this->roomPrices['advancepercentage'], 2 , '.', '');
 
		$this->roomPrices['advanceamount'] = number_format($this->roomPrices['advanceamount'], 2 , '.', '');
 
		}
 
		if(isset($_SESSION['dvars_roomprices']))unset($_SESSION['dvars_roomprices']);
 
		$_SESSION['dvars_roomprices'] = $this->roomPrices;
 
		
 
		return $result;
 
	}	
 
	 public function getextrabedprice($roomTypeId){
             
                $sql = mysql_query("SELECT sun FROM dots_priceplan WHERE roomtype_id='$roomTypeId' and capacity_id='1002'"); 
                $row=mysql_fetch_assoc($sql);
                return $row['sun'];
       
        }
 
	//*************************************************************************************************
 
	public function generateBlockingDetails() {
 
		$bsiCore = new bsiHotelCore;
 
		$result = array();
 
		$dvroomidsonly = "";
 
		$selectedRoomsCount = count($this->selectedRooms);	
                
		$dvarsCtr = 0;
 
		for($i = 0; $i < $selectedRoomsCount; $i++){
 
			if($this->selectedRooms[$i] > 0){		
 
				$this->detailVars[$dvarsCtr] = $this->searchVars[$i]; //selected only					
 							
 
				$tmpRoomCounter = 0;								
 
				foreach($this->detailVars[$dvarsCtr]['availablerooms'] as $availablerooms){	
 
					
 
					$dvroomidsonly .= $availablerooms['roomid'].",";													
 
					$tmpRoomCounter++;
 
						
 
					if($tmpRoomCounter == $this->selectedRooms[$i]){
 
						$tmpAvRmSize = count($this->detailVars[$dvarsCtr]['availablerooms']);
 
						for($akey = $tmpRoomCounter; $akey < $tmpAvRmSize; $akey++){
 
							unset($this->detailVars[$dvarsCtr]['availablerooms'][$akey]);
 
						}
 
						break;		
 
					}			
 
				}
 
				array_push($result, array('roomno'=>$tmpRoomCounter, 'roomtype'=>$this->detailVars[$dvarsCtr]['roomtypename'], 'capacitytitle'=>$this->detailVars[$dvarsCtr]['capacitytitle'] ,'capacity'=>$this->guestsPerRoom));		
 
		
 
				$dvarsCtr++;				
 
			}
 
		}
 
				
 
		if(isset( $_SESSION['dvars_details']))unset($_SESSION['dvars_details']);
 
		$_SESSION['dvars_details'] = $this->detailVars;
 
				
 
		if(isset($_SESSION['dv_roomidsonly']))unset($_SESSION['dv_roomidsonly']);
 
		$_SESSION['dv_roomidsonly'] = substr($dvroomidsonly, 0, -1);	
 
		$this->totalRoomCount =  count(explode(",", $_SESSION['dv_roomidsonly']));
 
			
 
		return $result;
 
	}	
 
 
 
 }
 
 ?>