<?php define('ONLINE_BOOKING_TEXT','Reserva on-line');
define('CHECK_IN_DATE_TEXT','Data de check-in');
define('CHECK_OUT_DATE_TEXT','Data de check-out');
define('ADULT_OR_ROOM_TEXT','Adultos/quarto');
define('SEARCH_TEXT','Pesquisa');
define('ENTER_CHECK_IN_DATE_ALERT','Por favor, insira a data de Check-In');
define('ENTER_CHECK_OUT_DATE_ALERT','Informe a data de saída');
define('CHILD_PER_ROOM_TEXT','Crianças / quarto');
define('NONE_TEXT','Nenhum');
define('SEARCH_INPUT_TEXT','Entrada de busca');
define('CHECK_IN_D_TEXT','Data de check-in');
define('CHECK_OUT_D_TEXT','Data de check-out');
define('TOTAL_NIGHTS_TEXT','Noites de totais');
define('ADULT_ROOM_TEXT','Adultos/quarto');
define('SEARCH_RESULT_TEXT','Resultado da pesquisa');
define('SELECT_ONE_ROOM_ALERT','Por favor, selecione pelo menos um quarto');
define('MAX_OCCUPENCY_TEXT','Ocupação Max');
define('SELECT_NUMBER_OF_ROOM_TEXT','Selecione o número da sala');
define('ADULT_TEXT','Adulto');
define('TOTAL_PRICE_OR_ROOM_TEXT','Preço total / quarto');
define('VIEW_PRICE_DETAILS_TEXT','Ver detalhes do preço');
define('HIDE_PRICE_DETAILS_TEXT','Ocultar detalhes do preço');
define('CONTINUE_TEXT','Continuar');
define('MON','Mon');
define('TUE','Terça-feira');
define('WED','Quarta-feira');
define('THU','Quinta-feira');
define('FRI','Sexta-feira');
define('SAT','SAT');
define('SUN','Sol');
define('MONTH','Mês');
define('CHILD_TEXT','Criança');
define('WITH_CHILD','com criança');
define('SORRY_ONLINE_BOOKING_CURRENTLY_NOT_AVAILABLE_PLEASE_TRY_LATER','Desculpe reservas online atualmente não disponível. Por favor, tente mais tarde.');
define('SORRY_YOU_HAVE_ENTERED_A_INVALID_SEARCHING_CRITERIA_PLEASE_TRY_WITH_INVALID_SEARCHING_CRITERIA','Desculpe você inseriu um inválido critérios de busca. Por favor, tente com os critérios de pesquisando inválidos.');
define('MINIMUM_NUMBER_OF_NIGHT_SHOULD_NOT_BE_LESS_THAN_TEXT','Número mínimo de noite não deve ser inferior');
define('PLEASE_MODIFY_YOUR_SEARCHING_CRITERIA_TEXT','Por favor modifique seus critérios de buscando.');
define('BOOKING_NOT_POSSIBLE_FOR_CHECK_IN_DATE_TEXT','Reservas não possível para check-in date:');
define('PLEASE_MODIFY_YOUR_SEARCH_CRITERIA_ACCORDING_TO_HOTELS_DATE_TIME_TEXT','Por favor modifique os critérios de pesquisa de acordo com Hotéis data hora.');
define('HOTELS_CURRENT_DATE_TIME_TEXT','Hotéis atual data hora:');
define('SORRY_NO_ROOM_AVAILABLE_AS_YOUR_SEARCHING_CRITERIA_PLEASE_TRY_WITH_DIFFERENT_DATE_SLOT_TEXT','Desculpe nenhum quarto disponível como seus critérios de buscando. Por favor, tente com slot de data diferentes.');
define('NIGHTS_TEXT','Noite (s)');
define('BOOKING_DETAILS_TEXT','Detalhes da reserva');
define('CHECKIN_DATE_TEXT','Data de check-In');
define('CHECKOUT_DATE_TEXT','Data de Check-Out');
define('TOTAL_NIGHT_TEXT','Noites de totais');
define('TOTAL_ROOMS_TEXT','Total de quartos');
define('NUMBER_OF_ROOM_TEXT','Número de quartos');
define('ROOM_TYPE_TEXT','Tipo de quarto');
define('MAXI_OCCUPENCY_TEXT','Ocupação Max');
define('GROSS_TOTAL_TEXT','Total bruto');
define('SUB_TOTAL_TEXT','Sub Total');
define('TAX_TEXT','Fiscal');
define('GRAND_TOTAL_TEXT','Total geral');
define('ADVANCE_PAYMENT_TEXT','Pagamento antecipado');
define('OF_GRAND_TOTAL_TEXT','do Total geral');
define('CUSTOMER_DETAILS_TEXT','Detalhes do cliente');
define('EXISTING_CUSTOMER_TEXT','Cliente existente');
define('EMAIL_ADDRESS_TEXT','Endereço de e-mail');
define('FETCH_DETAILS_TEXT','Detalhes da busca');
define('OR_TEXT','OU');
define('NEW_CUSTOMER_TEXT','Novo cliente');
define('TITLE_TEXT','Título');
define('MR_TEXT','Senhor');
define('MS_TEXT','MS');
define('MRS_TEXT','Senhora deputada');
define('MISS_TEXT','Miss');
define('DR_TEXT','Dr');
define('PROF_TEXT','Prof');
define('FIRST_NAME_TEXT','Primeiro nome');
define('LAST_NAME_TEXT','Último nome');
define('ADDRESS_TEXT','Endereço');
define('CITY_TEXT','Cidade');
define('STATE_TEXT','Estado');
define('POSTAL_CODE_TEXT','Código postal');
define('COUNTRY_TEXT','País');
define('PHONE_TEXT','Telefone');
define('FAX_TEXT','Fax');
define('EMAIL_TEXT','Email');
define('PAYMENT_BY_TEXT','Pagamento por');
define('FIELD_REQUIRED_ALERT','Este campo é obrigatório');
define('ADDITIONAL_REQUESTS_TEXT','Quaisquer solicitações adicionais');
define('I_AGREE_WITH_THE_TEXT',' Concordo com o');
define('TERMS_AND_CONDITIONS_TEXT','Termos condições &');
define('CONFIRM_TEXT','Confirmar');
define('CHECKOUT_TEXT','Check-out');
define('BD_INC_TAX','Incluindo IVA');
define('PAYMENT_TAX_TEXT','Imposto de pagamento');
define('INV_BOOKING_NUMBER','Número de reserva');
define('INV_CUSTOMER_NAME','Nome do cliente');
define('INV_ADULT','Adulto');
define('INV_PAY_DETAILS','Detalhes de pagamento');
define('INV_PAY_OPTION','Opção de pagamento');
define('INV_TXN_ID','ID da transação');
define('PP_REGARDS','Atenciosamente');
define('PAYER_EMAIL','E-Mail do autor');
define('PP_CARRY','Você vai precisar levar uma impressão fora este e-mail e apresentá-lo para o hotel na chegada e check-in. Este e-mail é o voucher de confirmação de sua reserva.');
define('TOTAL_TEXT','Total');
define('MANUAL_PAY_ON_ARIVAL_TEXT','Manual: Pagar em Arival');
define('NA_TEXT','AT');
define('DIRECT_ACCESS_TO_THIS_PAGE_RESTRICTED_ALERT_TEXT','Direto não o acesso a esta página é restricted.error = 9');
define('DETAILS_NOT_FOUND_TEXT','Detalhes não encontrados. Por favor, Cadastre-se');
define('CC_DETAILS','Cartão de crédito');
define('CC_HOLDER','Nome do titular do cartão');
define('CC_TYPE','Tipo de cartão de crédito');
define('CC_NUMBER','Número de cartão de crédito');
define('CC_EXPIRY','Prazo de validade');
define('CC_AMOUNT','Montante total');
define('CC_TOS1','Concordo para permitir');
define('CC_TOS2','a deduzir');
define('CC_TOS3','meu cartão de crédito');
define('CC_SUBMIT','Enviar');
define('DEAR_TEXT','Querida');
define('BOOKING_NO','Reservas não.');
define('NOTIFICATION_OF_ROOM_BOOKING_BY','Notificação de quarto reserva por');
define('BOOKING_COMPLETED_TEXT','Reserva concluída');
define('THANK_YOU_TEXT','Obrigado');
define('YOUR_BOOKING_CONFIRMED_TEXT','Sua reserva confirmada');
define('INVOICE_SENT_EMAIL_ADDRESS_TEXT','Factura enviada em seu e-mail');
define('BOOKING_FAILURE_TEXT','Falha de reserva');
define('BOOKING_FAILURE_ERROR_9','Acesso directo a esta página é restrito.');
define('BOOKING_FAILURE_ERROR_13','Alguém já adquirir o bloqueio de reserva em quartos especificado por você. Bloqueio de reserva será automaticamente liberado após alguns minutos em conclusão de reserva ou falha por outra pessoa. Por favor modifique os critérios de pesquisa e tente novamente.');
define('BOOKING_FAILURE_ERROR_22','Método de pagamento indefinido selecionado. Entre em contato com o administrador.');
define('BOOKING_FAILURE_ERROR_25','Falha ao enviar notificação por email. Entre em contato com o suporte técnico.');
define('FAILED_TO_SENT_EMAIL_TEXT','Falha ao enviar notificação por email. Entre em contato com o suporte técnico. ERROR_CODE = 25');
?>