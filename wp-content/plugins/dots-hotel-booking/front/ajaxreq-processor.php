<?php
//if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
//	header( 'Status: 403 Forbidden' );
//	header( 'HTTP/1.1 403 Forbidden' );
//	exit();
//}
include("includes/db.conn.php");
include("includes/conf.class.php");	
include("languages/".$bsiCore->config['conf_language']); 
include("includes/ajaxprocess.class.php");	
$ajaxProc = new ajaxProcessor();
switch($ajaxProc->actionCode){
	case "1": 
		$ajaxProc->getBookingStatus(); 
		break;
		
	case "2": 
		$ajaxProc->getCustomerDetails();
		break;
		
	case "3": 
		$ajaxProc->sendContactMessage();
		break;
		
	case "4":
		$ajaxProc->applyCouponDiscount();
		break;
		
	default:
		$ajaxProc->sendErrorMsg();
}
?>