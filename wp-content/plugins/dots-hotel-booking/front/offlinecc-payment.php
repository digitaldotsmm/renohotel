<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
$pos2 = strpos($_SERVER['HTTP_REFERER'],$_SERVER['SERVER_NAME']);
if(!$pos2){
	//header('Location: booking-failure.php?error_code=9');
	echo "<script>
		alert(".DIRECT_ACCESS_TO_THIS_PAGE_RESTRICTED_ALERT_TEXT.")
		</script>";
		die;
}
include("includes/conf.class.php");
include("languages/".$bsiCore->config['conf_language']);
wp_enqueue_script( 'custom_script5', HOTELBOOKING_MANAGER_URL .'front/js/jquery.validate.js');
?>

<script id="demo" type="text/javascript">
jQuery(document).ready(function() {
	jQuery("#form1").validate();
});
</script>


  <div id="wrapper" style="width:600px;">
       <h2 align="left" style="padding-left:5px;"><?php echo CC_DETAILS; ?></h2>
      <form  name="signupform" id="form1"  action="" method="post" onSubmit="return testCreditCard();">
        <input type="hidden" name="bookingid" value="<?php echo $_POST['x_invoice_num'];?>" />
        <table cellpadding="6" cellspacing="6" width="100%"  style=" text-align:left;" >
          <tr>
            <td width="120px"><strong><?php echo CC_HOLDER; ?></strong></td>
            <td><input type="text" name="cc_holder_name" id="cc_holder_name" class="required" /></td><td  class="status"></td>
          </tr>
          <tr>
            <td><strong><?php echo CC_TYPE; ?></strong></td>
            <td><select name="CardType" id="CardType" class="textbox" style="width:100px;">
                <option value="AmEx">AmEx</option>
                <option value="DinersClub">DinersClub</option>
                <option value="Discover">Discover</option>
                <option value="JCB">JCB</option>
                <option value="Maestro">Maestro</option>
                <option value="MasterCard">MasterCard</option>
                <option value="Solo">Solo</option>
                <option value="Switch">Switch</option>
                <option value="Visa">Visa</option>
                <option value="VisaElectron">VisaElectron</option>
              </select></td><td></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong><?php echo CC_NUMBER; ?></strong></td>
            <td><input type="text" name="CardNumber" id="CardNumber" maxlength="16" class="required creditcard" /></td><td  class="status"></td>
          </tr>
          <tr>
            <td nowrap="nowrap"><strong><?php echo CC_EXPIRY; ?> (mm/yy)</strong></td>
            <td><input type="text" name="cc_exp_dt" id="cc_exp_dt" style="width:50px;" maxlength="5" class="required" />
               </td>
          </tr>
          <tr>
            <td><strong>CCV/CCV2</strong></td>
            <td><input type="text" name="cc_ccv" id="cc_ccv"  style=" width:50px" maxlength="4" class="required number"/></td>
          </tr>
          <tr>
            <td><strong><?php echo CC_AMOUNT; ?></strong></td>
            <td><b><?php echo $bsiCore->config['conf_currency_symbol']?><?php echo $_POST['total']?></b></td>
          </tr>
          <tr>
            <td></td>
            <td colspan="2"><input type="checkbox" name="tos" id="tos" value="" style="width:15px;"   class="required"/> 
            <?php echo CC_TOS1; ?> <?php echo $bsiCore->config['conf_hotel_name']?>
              <?php echo CC_TOS2; ?> <?php echo $bsiCore->config['conf_currency_symbol']?><?php echo $_POST['total']?> <?php echo CC_TOS3; ?>. </td> 
          </tr>
          <tr>
            <td></td>
            <td  align="left"><button  type="submit" style="float:left;"><?php echo CC_SUBMIT; ?></button></td><td></td>
          </tr>
        </table>
      </form>
     </div>
