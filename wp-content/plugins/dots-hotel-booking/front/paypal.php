<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
include("includes/conf.class.php");
if ($_REQUEST['action']) {
    include(HOTELBOOKING_MANAGER_PATH . 'includes/db.conn.php');
}
include("languages/" . $bsiCore->config['conf_language']);
include("includes/mail.class.php");
include("includes/process.class.php");
require_once('paypal.class.php');  // include the class file
$bsiCore = new bsiHotelCore;
$paymentGatewayDetails = $bsiCore->loadPaymentGateways();
$bsiMail = new bsiMail();
$emailContent = $bsiMail->loadEmailContent();
$invoice = time();
$p = new paypal_class;             // initiate an instance of the class
$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
$this_script = HOTELBOOKING_MANAGER_URL . "front/paypal.php";
// if there is not action variable, set the default action of 'process'
if (empty($_GET['action']))
    $_GET['action'] = 'process';
switch ($_GET['action']) {
    case 'process':      // Process and order... 
        $bookprs = new BookingProcess();
        $booking_id = $bookprs->bookingId;
        $total_amount = $bookprs->totalPaymentAmount;
        $p->add_field('business', $paymentGatewayDetails['pp']['account']);
        $p->add_field('notify_url', WP_HOME . '/search-room/?action=ipn');
        $p->add_field('item_name', $bsiCore->config['conf_hotel_name']);
        $p->add_field('invoice', $booking_id);
        $p->add_field('currency_code', $bsiCore->config['conf_currency_code']);
        $p->add_field('amount', number_format($total_amount, 2, '.', ''));
        $p->add_field('first_name', $_REQUEST['fname']);
        $p->add_field('last_name', $_REQUEST['lname']);
        $p->add_field('address1', $_REQUEST['str_addr']);
        $p->add_field('city', $_REQUEST['city']);
//$p->add_field('state',$_REQUEST['state']);
//$p->add_field('zip',$_REQUEST['zipcode']);
        $p->add_field('night_phone_a', $_REQUEST['phone']);
        $p->add_field('email', $_REQUEST['email']);
        $p->add_field('country', $_REQUEST['country']);
        $p->add_field('return', WP_HOME);
        $p->add_field('cancel_return', WP_HOME);
        $p->submit_paypal_post(); // submit the fields to paypal
//$p->dump_fields();      // for debugging, output a table of all the fields
        break;
    case 'ipn':
        if ($p->validate_ipn()) {
            if ($p->ipn_data['payment_status'] == "Completed" || $p->ipn_data['payment_status'] == "Pending") {
//*****************************************************************************************
                
                mysql_query("UPDATE dots_bookings SET payment_success=true, payment_txnid='" . $p->ipn_data['txn_id'] . "', paypal_email='" . $p->ipn_data['payer_email'] . "' WHERE booking_id='" . $p->ipn_data['invoice'] . "'");
 
                $invoiceROWS = mysql_fetch_assoc(mysql_query("SELECT client_name, client_email, invoice FROM dots_invoice WHERE booking_id='" . $p->ipn_data['invoice'] . "'"));
                mysql_query("UPDATE dots_clients SET existing_client = 1 WHERE email='" . $invoiceROWS['client_email'] . "'");

                $invoiceHTML = $invoiceROWS['invoice'];
                $invoiceHTML.= '<br><br><table style="font-family:Verdana, Geneva, sans-serif; font-size: 12px; bgcolor:#999999; width:700px; border:none;" cellpadding="4" cellspacing="1"><tr><td align="left" colspan="2" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . INV_PAY_DETAILS . '</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . INV_PAY_OPTION . '</td><td align="left" style="background:#ffffff">PayPal</td></tr><tr><td align="left" width="30%" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . PAYER_EMAIL . '</td><td align="left" style="background:#ffffff">' . $p->ipn_data['payer_email'] . '</td></tr><tr><td align="left" style="font-weight:bold; font-variant:small-caps; background:#ffffff">' . INV_TXN_ID . '</td><td align="left" style="background:#ffffff">' . $p->ipn_data['txn_id'] . '</td></tr></table>';

                mysql_query("UPDATE dots_invoice SET invoice = '$invoiceHTML' WHERE booking_id='" . $p->ipn_data['invoice'] . "'");

                $emailBody = '<a href="' . WP_HOME . '" target="_blank"><img src="' . WP_HOME . '/wp-content/themes/hotelshalom/assets/images/York1.jpg" alt="york residence" style="width:200px;margin-left: 237px;"/></a><br><br>';

                $emailBody .= "Dear " . $invoiceROWS['client_name'] . ",<br><br>";
//$emailBody .= html_entity_decode($emailContent['body']) . "<br><br>";
                $emailBody .= $invoiceHTML;
                $emailBody .= "<br><br>" . PP_REGARDS . ",<br>" . $bsiCore->config['conf_hotel_name'] . '<br>' . $bsiCore->config['conf_hotel_phone'];
                $emailBody .= "<br><br><font style=\"color:#F00; font-size:10px;\">[ " . PP_CARRY . " ]</font>";
                $flag = 1;
                $bsiMail->sendEMail($invoiceROWS['client_email'], $emailContent['subject'], $emailBody, $p->ipn_data['invoice'], $flag);

                /* Notify Email for Hotel about Booking */
                $notifyEmailSubject = BOOKING_NO . $p->ipn_data['invoice'] . " - " . NOTIFICATION_OF_ROOM_BOOKING_BY . $invoiceROWS['client_name'];

                $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $invoiceHTML);
                echo '<p>Thank you for choosing us for your stay in Yangon.</p>';
//*****************************************************************************************
            } elseif ($p->ipn_data['payment_status'] == "Refunded") {
                mysql_query("update paypal_payment set payment_success='0' where invoice=" . $p->ipn_data['invoice']);
            } elseif ($p->ipn_data['payment_status'] == "Reversed") {
                mysql_query("update paypal_payment set payment_success='0' where invoice=" . $p->ipn_data['invoice']);
            }
        }
        break;
}
?>