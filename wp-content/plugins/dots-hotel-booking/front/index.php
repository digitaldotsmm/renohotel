<?php
include("includes/conf.class.php");
include("languages/" . $bsiCore->config['conf_language']);
$langauge_selcted = explode(".", $bsiCore->config['conf_language']);
wp_enqueue_script('jquery-datepicker33333', HOTELBOOKING_MANAGER_URL . 'front/js/dtpicker/jquery.ui.datepicker-' . $langauge_selcted[0] . '.js');
$bsiCore = new bsiHotelCore;
?>
<script type="text/javascript">
    jQuery(document).ready(function() {
         jQuery("#txtFromDate").datepicker({ 
            minDate: "-0D",
            maxDate: "+365D",
            numberOfMonths: 2,
            dateFormat : "dd/mm/yy",
            onSelect: function(selected) {
                var date = jQuery(this).datepicker('getDate');                
                if(date){
                    date.setDate(date.getDate() + <?php echo $bsiCore->config['conf_min_night_booking']; ?>);
                }
                jQuery("#txtToDate").datepicker("option","minDate", date)
            } 
        });
 
        jQuery("#txtToDate").datepicker({ 
            minDate: "-0D",
            maxDate:"+365D",
            numberOfMonths: 2,            
            dateFormat : "dd/mm/yy",
            onSelect: function(selected) {
                jQuery("#txtFromDate").datepicker("option","maxDate", selected)
            }
        });  
        jQuery("#txtFromDate").click(function() { 
            jQuery("#txtFromDate").datepicker("show");
        });
        jQuery(".txtToDateCalendar").click(function() {             
            jQuery("#txtToDate").datepicker("show");
        });
          
    });
</script>
<?php
if (file_exists(TEMPLATEPATH . '/includes/hotel-search-form.php')) :
    include_once( TEMPLATEPATH . '/includes/hotel-search-form.php' );
else :
    ?>
    <div id="wrapper" style="width:300px !important;" >
        <form id="formElem" name="formElem" action="<?php the_permalink(); ?>" method="post">
            <table cellpadding="0"  cellspacing="7" border="0"  align="left" style="text-align:left;">
                <tr>
                    <td><strong><?php echo CHECK_IN_DATE_TEXT; ?>:</strong></td>
                    <td><input id="txtFromDate" name="check_in" style="width:90px" type="text" readonly="readonly" AUTOCOMPLETE=OFF />
                        <span style="padding-left:3px;"><a id="datepickerImage" href="javascript:;"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>front/images/month.png" height="16px" width="16px" style=" margin-bottom:-4px;" border="0" /></a></span></td>
                </tr>
                <tr>
                    <td><strong><?php echo CHECK_OUT_DATE_TEXT; ?>:</strong></td>
                    <td><input id="txtToDate" name="check_out" style="width:90px" type="text" readonly="readonly" AUTOCOMPLETE=OFF />
                        <span style="padding-left:3px;"><a id="datepickerImage1" href="javascript:;"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>front/images/month.png" height="18px" width="18px" style=" margin-bottom:-4px;" border="0" /></a></span></td>
                </tr>
                <tr>
                    <td><strong><?php echo ADULT_OR_ROOM_TEXT; ?>:</strong></td>
                    <td><?php echo $bsiCore->capacitycombo(); ?></td>
                </tr>
                <?php echo $bsiCore->getChildcombo(); ?>
                <tr>
                    <td></td>
                    <td><button id="btn_room_search" type="submit"><?php echo SEARCH_TEXT; ?></button></td>
                </tr>
            </table>
        </form>
    </div>
<?php endif; ?>