<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
include("includes/conf.class.php");
include("languages/" . $bsiCore->config['conf_language']);
include("includes/mail.class.php");
include("includes/process.class.php");
$bookprs = new BookingProcess();



switch ($bookprs->paymentGatewayCode) {
    case "poa":
        $bi = $bookprs->bookingId;
        $be = $bookprs->clientEmail;
        $cname = $bookprs->clientName;
        $htm = $bookprs->invoiceHtml;
        processPayOnArrival($bi, $be, $cname, $htm);
        break;

    case "pp":
        $id = $bookprs->bookingId;
        $tm = $bookprs->totalPaymentAmount;
        processPayPal($id, $tm);
        break;

    case "cc":
        $payamount = $bookprs->totalPaymentAmount;
        $bookid = $bookprs->bookingId;
        processCreditCard($payamount, $bookid);
        break;
    case "aunet" :
        $payamount = $bookprs->totalPaymentAmount;
        $bookid = $bookprs->bookingId;
        processAuthorizeNet($payamount, $bookid, $desc);
        break;

    default:
        processOther();
}
/* AUTHORIZE.NET PAYMENT */

function processAuthorizeNet($amount, $bookid, $desc) {
    $paymentAmount = number_format($amount, 2, '.', '');
    echo "<script language=\"javascript\">";
    echo "document.write('<form action=\"\" method=\"post\" name=\"formauthorizenet\">');";
    echo "document.write('<input type=\"hidden\" name=\"x_invoice_num\" value=\"" . $bookid . "\"/>');";
    echo "document.write('<input type=\"hidden\" name=\"total\" value=\"" . $paymentAmount . "\">');";
    echo "document.write('<input type=\"hidden\" name=\"description\" value=\"" . $desc . "\">');";
    echo "document.write('<input type=\"hidden\" name=\"pay_type\" value=\"authorizenet\">');";
    echo "document.write('</form>');";
    echo "setTimeout(\"document.formauthorizenet.submit()\",500);";
    echo "</script>";
}

/* PAY ON ARIVAL: MANUAL PAYMENT */

function processPayOnArrival($bbi, $bbe, $cname, $htm) {
    $bsiCore = new bsiHotelCore;
    $bsiMail = new bsiMail();

    $emailContent = $bsiMail->loadEmailContent();
    $subject = $emailContent['subject'];
    mysql_query("UPDATE dots_bookings SET payment_success=true WHERE booking_id = " . $bbi);
    mysql_query("UPDATE dots_clients SET existing_client = 1 WHERE email = '" . $bbe . "'");

    $emailBody = DEAR_TEXT . " " . $cname . ",<br><br>";
    $emailBody .= $emailContent['body'] . "<br><br>";
    $emailBody .= $htm;
    $emailBody .= '<br><br>' . PP_REGARDS . ',<br>' . $bsiCore->config['conf_hotel_name'] . '<br>' . $bsiCore->config['conf_hotel_phone'];
    $emailBody .= '<br><br><font style=\"color:#F00; font-size:10px;\">[ ' . PP_CARRY . ' ]</font>';

    $returnMsg = $bsiMail->sendEMail($bbe, $subject, $emailBody);

    if ($returnMsg == true) {

        $notifyEmailSubject = BOOKING_NO . " " . $bbi . " - " . NOTIFICATION_OF_ROOM_BOOKING_BY . " " . $cname;
        $notifynMsg = $bsiMail->sendEMail($bsiCore->config['conf_notification_email'], $notifyEmailSubject, $htm);
        include('booking-confirm.php');
    } else {
        //header('Location:'.$_SESSION['url'].'&error_code=25');  

        echo "<script>
		alert(" . BOOKING_FAILURE_ERROR_25 . ")
		</script>";
        die;
    }
    //header('Location: booking-confirm.php?success_code=1');
}

/* PAYPAL PAYMENT */

function processPayPal($i, $t) {

    echo "<script language=\"JavaScript\">";
    echo "document.write('<form action=\"\" method=\"post\" name=\"formpaypal\">');";
    echo "document.write('<input type=\"hidden\" name=\"amount\"  value=\"" . number_format($t, 2, '.', '') . "\">');";
    echo "document.write('<input type=\"hidden\" name=\"invoice\"  value=\"" . $i . "\">');";
    echo "document.write('</form>');";
    echo "setTimeout(\"document.formpaypal.submit()\",500);";
    echo "</script>";
}

/* CREDIT CARD PAYMENT */

function processCreditCard($pa, $bi) {
    global $bsiCore;
    $paymentAmount = number_format($pa, 2, '.', '');
    echo "<script language=\"javascript\">";
    echo "document.write('<form action=\"\" method=\"post\" name=\"form2checkout\">');";
    echo "document.write('<input type=\"hidden\" name=\"x_invoice_num_cc\" value=\"" . $bi . "\"/>');";
    echo "document.write('<input type=\"hidden\" name=\"total\" value=\"" . $paymentAmount . "\">');";
    echo "document.write('</form>');";
    echo "setTimeout(\"document.form2checkout.submit()\",500);";
    echo "</script>";
}

/* OTHER PAYMENT */

function processOther() {
    /* not implemented yet */
//	header('Location: booking-failure.php?error_code=22');
    echo "<script>
		alert(" . BOOKING_FAILURE_ERROR_22 . ")
		</script>";
    die;
    die;
}

?>