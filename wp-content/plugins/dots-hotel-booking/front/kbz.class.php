<?php

//if (!defined('HOTELBOOKING_VERSION')) {
//    header('Status: 403 Forbidden');
//    header('HTTP/1.1 403 Forbidden');
//    exit();
//}

class kbz_class {

    var $kbz_url = "";
    var $merchantid = "";
    var $secret_key = "";
    var $last_error;                 // holds the last error encountered
    var $ipn_log;                    // bool: log IPN results to text file?
    var $test_mode = false;
    var $ipn_response;               // holds the IPN response from paypal   
    var $ipn_data = array();         // array contains the POST values for IPN
    var $fields = array();           // array holds the fields to submit to paypal
    var $x_relay_url = "";
    var $amount;
    var $fingerprint;

    function kbz_class() {

        // initialization constructor.  Called when class is created.
        
        

        $this->kbz_url = 'https://demo3.2c2p.com/KBZPGW/payment/payment/payment';
        $this->merchantid = "70110004"; //sandbox log in id
        $this->secret_key = "2VFGXD30SPYMPBGUK0UJ1Y1QQOOVGFCN"; //sandbox log in id
        $this->last_error = '';

        $this->sequence = rand(1, 1000);
        // a timestamp is generated
        $this->timeStamp = time();

        // an invoice is generated using the date and time
        // a sequence number is randomly generated
        // populate $fields array with a few default values.  See the paypal
        // documentation for a list of fields and their data types. These defaul
        // values can be overwritten  by the calling script.
    }

    function add_field($field, $value) {

        // adds a key=>value pair to the fields array, which is what will be 
        // sent to paypal as POST variables.  If the value is already in the 
        // array, it will be overwritten.

        $this->fields["$field"] = $value;
    }

    function submit_kbz_post() {

        // this function actually generates an entire HTML page consisting of
        // a form with hidden elements which is submitted to authorize.net via the 
        // BODY element's onLoad attribute.  We do this so that you can validate
        // any POST vars from you custom form before submitting to paypal.  So 
        // basically, you'll have your own form which is submitted to your script
        // to validate the data, which in turn calls this function to create
        // another hidden form and submit to paypal.
        // The user will briefly see a message on the screen that reads:
        // "Please wait, your order is being processed..." and then immediately
        // is redirected to paypal.
        
        echo "<body onLoad=\"document.form1.submit();\">\n";
        echo "<center><h3>Please wait, your order is being processed...</h3></center>\n";
        echo "<form method=\"post\" name=\"form1\" action=\"" . $this->kbz_url . "\" id=\"processfrom\">\n";
        foreach ($this->fields as $name => $value) {
            echo "<input type=\"hidden\" name=\"$name\" id=\"$name\" value=\"$value\">";
        }

        echo "</form>\n";
       
    }

}


