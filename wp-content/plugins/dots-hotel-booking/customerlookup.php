<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if(isset($_REQUEST['update'])){
	
	header("location:admin.php?page=Customerlookup"); 
	exit;
}
include("includes/conf.class.php");
include("includes/admin.class.php");
$html = $bsiAdminMain->getCustomerHtml();
wp_enqueue_script( 'custom_script10', HOTELBOOKING_MANAGER_URL .'js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', HOTELBOOKING_MANAGER_URL .'js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', HOTELBOOKING_MANAGER_URL .'css/data.table.css');
wp_enqueue_style('custom-style10', HOTELBOOKING_MANAGER_URL .'css/jqueryui.css');
 ?>  
      
      <div id="container-inside" class="widefat">
          <h2>Customer List</h2>
      <hr />
        <table class="display datatable" border="0">
          <thead>
	<tr><th width="18%" nowrap="nowrap">Guest Name</th><th width="27%" nowrap="nowrap">Street Address</th><th width="15%" nowrap="nowrap">Phone Number</th><th width="25%" nowrap="nowrap">Email Id</th><th width="15%" nowrap="nowrap">&nbsp;</th></tr>
 </thead>
 <tbody id="getcustomerHtml">
 	<?php echo $html?>
 </tbody>
        </table>
      </div>
 <script>
jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'desc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of_TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_Entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 
