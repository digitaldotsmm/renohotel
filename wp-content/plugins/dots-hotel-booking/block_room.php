<?php 


if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

if(isset($_POST['block'])){

	include("includes/db.conn.php");

	include("includes/conf.class.php");

	 
	include("includes/details.class.php");


	 $bsibooking = new bsiBookingDetails;
	 
	$bsibooking->generateBlockingDetails(); 

	//$bsiCore->clearExpiredBookings();

	$reservationdata = array();

	$reservationdata = $_SESSION['dvars_details'];

	$bookingId       = time();


	$sql = mysql_query("INSERT INTO dots_bookings (booking_id, booking_time, start_date, end_date, client_id, is_block, payment_success, block_name) values(".$bookingId.", NOW(), '".$_SESSION['sv_mcheckindate']."', '".$_SESSION['sv_mcheckoutdate']."', '0', 1, 1, '".mysql_real_escape_string($_POST['block_name'])."')");	

	foreach($reservationdata as $revdata){

		foreach($revdata['availablerooms'] as $rooms){
                    $sql = mysql_query("INSERT INTO dots_reservation (bookings_id, room_id, room_type_id) values(".$bookingId.",  ".$rooms['roomid'].", ".$revdata['roomtypeid'].")");
                //insert into availabel room id
                    
                    $checkInDate=strtotime($_SESSION['sv_mcheckindate']);
                    $checkoutDate=strtotime("-1 day", strtotime ( $_SESSION['sv_mcheckoutdate'] ));
                        for ($i = $checkInDate; $i <= $checkoutDate; $i = $i + 86400) {
                            $thisDate = date('Y-m-d', $i); 
                            $sql=mysql_query("INSERT INTO dots_available_room(booking_id,book_date,room_type_id) VAlUES (".$bookingId.",'".$thisDate."',".$revdata['roomtypeid'].")");
                        }
                         
		} 

	}	
	
	header("location:admin.php?page=admin-block-room");

	exit;

}



include("includes/conf.class.php");

include("includes/admin.class.php");



if(isset($_POST['submit'])){

	include ('includes/search.class.php');

	$bsisearch = new bsiSearch();

	//$bsiCore->clearExpiredBookings();

}

?>

<script type="text/javascript">

jQuery(document).ready(function(){
jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ 'en' ] );
 jQuery.datepicker.setDefaults({ dateFormat: '<?php echo $bsiCore->config['conf_dateformat']?>' });

   jQuery("#txtFromDate").datepicker({

        minDate: 0,

        maxDate: "+365D",

        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",

        onSelect: function(selected) {

    	var date = jQuery(this).datepicker('getDate');

         if(date){

            date.setDate(date.getDate() + <?php echo $bsiCore->config['conf_min_night_booking']?>);

          }

         jQuery("#txtToDate").datepicker("option","minDate", date)

        }

    });

 

   jQuery("#txtToDate").datepicker({ 

        minDate: 0,

        maxDate:"+365D",

        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",

        onSelect: function(selected) {

          jQuery("#txtFromDate").datepicker("option","maxDate", selected)

        }

    });  

 jQuery("#datepickerImage").click(function() { 

    jQuery("#txtFromDate").datepicker("show");

  });

 jQuery("#datepickerImage1").click(function() { 

    jQuery("#txtToDate").datepicker("show");

  });

});

</script>

<div id="container-inside" class="wrap">

    <h2>Search</h2>

<input type="button" value="Back to Block List" onClick="window.location.href='admin.php?page=admin-block-room'" class="button-primary" />

  <hr style="margin-top:10px;" />

  <table cellpadding="4" width="100%">

    <tr>

      <td width="25%" valign="top"><form action="<?php echo admin_url('admin.php?page=block-room'); ?>" method="post" id="form1">

          <table cellpadding="0"  cellspacing="7" border="0">

            <tr>

              <td> Check-in Date</td>

              <td><input id="txtFromDate" name="check_in" style="width:100px" type="text" readonly="readonly" AUTOCOMPLETE=OFF />

                <span style="padding-left:3px;"><a id="datepickerImage" href="javascript:;"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>images/month.png" height="16px" width="16px" style=" margin-bottom:-4px;" /></a></span></td>

            </tr>

            <tr>

              <td>Check-out Date</td>

              <td><input id="txtToDate" name="check_out" style="width:100px" type="text" readonly="readonly" AUTOCOMPLETE=OFF />

                <span style="padding-left:3px;"><a id="datepickerImage1" href="javascript:;"><img src="<?php echo HOTELBOOKING_MANAGER_URL; ?>images/month.png" height="18px" width="18px" style=" margin-bottom:-4px;" /></a></span></td>

            </tr>

            <tr>

              <td></td>

              <td><input type="submit" value="Search" name="submit" class="button-primary" /></td>

            </tr>

          </table>

        </form></td>

      <td valign="top"><?php if(isset($_POST['submit'])){ echo '<script type="text/javascript" src="'.HOTELBOOKING_MANAGER_URL .'js/hotelvalidation.js"></script> '; ?>

        <form name="adminsearchresult" id="adminsearchresult" method="post" action="<?php echo admin_url('admin.php?page=block-room&noheader=true'); ?>" onsubmit="return validateSearchResult();">

          <table cellpadding="4" cellspacing="2" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border:#999 solid 1px;" width="450px">

            <tr>

              <th align="left" colspan="2"><b>

                SEARCH RESULT

                (

                <?php echo $_POST['check_in']?>

                to

                <?php echo $_POST['check_out']?>

                ) =

                <?php echo $bsisearch->nightCount?>

                NIGHTS</b></th>

            </tr>

            <tr>

              <td align="left" >DESCRIPTION / NAME</td>

              <td><input type="text" name="block_name" id="block_name"  style="width:230px !important;"/></td>

            </tr>

            <tr><td colspan="2"><hr /></td></tr>

            <tr>

              <th align="left">RoomType</th>

              <th align="left">Availablity</th>

            </tr>

             <tr><td colspan="2"><hr /></td></tr>

            <?php

	 	$gotSearchResult = false;

		$idgenrator = 0;

		foreach($bsisearch->roomType as $room_type){

			foreach($bsisearch->multiCapacity as $capid=>$capvalues){			

				$room_result = $bsisearch->getAvailableRooms($room_type['rtid'], $room_type['rtname'], $capid);

				if(intval($room_result['roomcnt']) > 0) {

					$gotSearchResult = true;	

			 ?>

            <tr>

              <td><?php echo $room_type['rtname']; ?>

                (

                <?php echo $capvalues['captitle']; ?>

                )</td>

              <td><select name="svars_selectedrooms[]" style="width:70px;">

                  <?php echo $room_result['roomdropdown']; ?>

                </select></td>

            </tr>

            <?php 

		$idgenrator++;

	} } }   

			if($gotSearchResult){

				echo '<tr>

				  <td>&nbsp;</td>

				  <td><input type="submit" value= BLOCK name="block" class="button-primary" /></td>

				</tr>';

			}else{

				echo '<tr>

				  <td colspan="2" align="center" style="color:red;"><b>Sorry no room available as your searching criteria.</b></td>

				</tr>';

			}

			?>

            

          </table>

        </form>

        <?php } ?></td>

    </tr>

  </table>

</div>

<script type="text/javascript">

	jQuery().ready(function() {

		jQuery("#form1").validate();

		

     });

         

</script> 



