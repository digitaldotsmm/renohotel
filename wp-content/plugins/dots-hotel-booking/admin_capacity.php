<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
//include("access.php");
if(isset($_GET['cdelid'])){
	include("includes/db.conn.php"); 
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	$bsiAdminMain->delete_capacity();
	header("location:admin.php?page=capacity-manager");
	exit;
}
//include("header.php"); 
include("includes/admin.class.php");
wp_enqueue_script( 'custom_script10', HOTELBOOKING_MANAGER_URL .'js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', HOTELBOOKING_MANAGER_URL .'js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', HOTELBOOKING_MANAGER_URL .'css/data.table.css');
wp_enqueue_style('custom-style10', HOTELBOOKING_MANAGER_URL .'css/jqueryui.css');
?>
<script type="text/javascript">
function capacitydelete(cid){
	var ans=confirm('Do You Want to delete the selected Capacity? Remember Corresponding Room And Roomtype Will Be Deleted Forever');
	if(ans){
		window.location='admin.php?page=capacity-manager&noheader=true&cdelid='+cid;
		return true;
		
	}else{
		return false;
		
	}
}
</script>

<div id="container-inside" class="wrap">
    <h2>Capacity List</h2>
    <input type="button" value="Add new Capacity" onClick="window.location.href='admin.php?page=aad-edit-capacity&id=0'" class="button-primary" />
<hr style="margin-top:10px;" />
  <table class="display datatable" border="0">
    <thead>
      <tr>
        <th width="20%">Capacity Title</th>
        <th width="20%">Number of Adult</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    
<!-- html will be generate here-->

    <?php echo $bsiAdminMain->generateCapacityListHtml();?>

      </table>
</div>
<script>
jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[1,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 
</div>