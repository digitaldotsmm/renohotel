<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if(isset($_POST['act_sbmt'])){

		include("includes/db.conn.php");

		include("includes/conf.class.php");

		include("includes/admin.class.php");

		$bsiAdminMain->global_setting_post();

		header("location:admin.php?page=global-setting");

}



include("includes/conf.class.php");

include("includes/admin.class.php");

$global_setting=$bsiAdminMain->global_setting();

?>    

      
      <div id="container-inside" class="wrap">

          <h2>GLOBAL SETTING</h2>

      <hr />

        <form action="<?php echo $_SERVER['PHP_SELF']?>?page=global-setting&noheader=true" method="post" id="form1">

          <table cellpadding="5" cellspacing="2" border="0">

            <tr>

              <td><strong> Email Notification:</strong></td>

              <td valign="middle"><input type="text" name="email_notification" id="email_notification" value="<?php echo $bsiCore->config['conf_notification_email']?>" class="required" style="width:250px;" /></td>

            </tr>

            <tr>

              <td><strong>Currency Code:</strong></td>

              <td><input type="text" name="currency_code" id="currency_code" value="<?php echo $bsiCore->config['conf_currency_code']?>"  class="required" style="width:70px;"  /></td>

            </tr>

             <tr>

              <td><strong>Currency Symbol:</strong></td>

              <td><input type="text" name="currency_symbol" id="currency_symbol" value="<?php echo $bsiCore->config['conf_currency_symbol']?>"  class="required" style="width:70px;"  /></td>

            </tr>

             <tr>

              <td><strong>Booking Engine:</strong></td>

              <td><select name="booking_turn" id="booking_turn"><?php echo $global_setting['select_booking_turn']?></select></td>

            </tr>

             <tr>

              <td><strong>Hotel Timezone:</strong></td>

              <td><select name="timezone" id="timezone"><?php echo $global_setting['select_timezone']?></select></td>

            </tr>

             <tr>

              <td><strong>Minimum Booking:</strong></td>

              <td><select name="minbooking" id="minbooking"><?php echo $global_setting['select_min_booking']?></select>&nbsp;&nbsp; Night Settings </td>

            </tr>

            <tr>

                <td><strong>Date Format:</strong></td>

                <td><select name="date_format" id="date_format">

                <?php echo $global_setting['select_dateformat']?>

                </select></td>

          </tr>

             <tr>

              <td><strong>Room Lock time</strong></td>

              <td><select name="room_lock" id="room_lock"><?php echo $global_setting['select_room_lock']?></select> <span style="font-size:10px">&nbsp;&nbsp;<?php echo "DURATION FOR CUSTOMER SELECTED ROOM";?></span></td>
              <tr>
              <td><strong>Terms and Condition Page Url:</strong></td><td><input type="text" value="<?php echo $bsiCore->config['conf_tos_url']?>" name="tc" style="width:450px;" /></td>
              </tr>

            </tr>
                <tr>
                <td><strong>Front-end Language:</strong></td> 
                <td>
                <select name="lang" id="lang">
                <?php echo $global_setting['lang_list'];?></select>
                </td>
                </tr>

            

            <tr>

              <td><strong>Tax:</strong></td>

              <td><input type="text" name="tax" id="tax" size="6" class="required number" value="<?php echo $bsiCore->config['conf_tax_amount']?>" />&nbsp;%</td>

            </tr>

            <tr>

              <td><strong>Price including Tax:</strong></td>

              <td><?php

                     if($bsiCore->config['conf_price_with_tax']==1){

	              ?>

    

                 <input type="checkbox" name="price_inclu_tax" id="price_inclu_tax" checked="checked" />

				<?php

                }else{

                ?>

                <input type="checkbox" name="price_inclu_tax" id="price_inclu_tax" />

                <?php

                }

                ?>

    </td>

            </tr>

              <td><input type="hidden" name="act_sbmt" value="1" /></td>

              <td><input type="submit" value="Submit" class="button-primary" /></td>

            </tr>

          </table>

        </form>

      </div>

<script type="text/javascript">

	jQuery().ready(function() {

		jQuery("#form1").validate();

		

     });
</script>


