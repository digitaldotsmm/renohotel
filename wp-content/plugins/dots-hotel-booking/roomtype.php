<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
include("includes/conf.class.php");
include("includes/admin.class.php");
if(isset($_GET['rdelid'])){	
	$bsiAdminMain->delete_roomtype();
	header("location:admin.php?page=room-type-manager");	 
	exit;
}
wp_enqueue_script( 'custom_script10', HOTELBOOKING_MANAGER_URL .'js/bsi_datatables.js');
wp_enqueue_script( 'custom_script11', HOTELBOOKING_MANAGER_URL .'js/DataTables/jquery.dataTables.js');
wp_enqueue_style('custom-style9', HOTELBOOKING_MANAGER_URL .'css/data.table.css');
wp_enqueue_style('custom-style10', HOTELBOOKING_MANAGER_URL .'css/jqueryui.css');
?>

<script type="text/javascript">
function deleteRoomType(rtid){
	var ans=confirm('<?php echo 'DO YOU WANT TO DELETE THE SELECTED ROOM TYPE';?>');
	if(ans){
		
		window.location='admin.php?page=room-type-manager&noheader=true&rdelid='+rtid;
			
	}else{
		return false;		
	}
}
</script>
<div id="container-inside" class="wrap">
    <h2>Room Type List</h2>
    <input type="button" value="Add New Room type" class="button-primary" onClick="window.location.href='admin.php?page=aad-new-room-type&id=0'" />
 <hr style="margin-top:10px;" /> 
  <table class="display datatable" border="0" width="640px">
    <thead>
      <tr>
        <th width="30%">Room Type Name</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    
        <?php echo $bsiAdminMain->generateRoomtypeListHtml()?>

    
    </table>
</div>
<script>
 jQuery(document).ready(function() {
	 	var oTable = jQuery('.datatable').dataTable( {
				"bJQueryUI": true,
				"sScrollX": "",
				"bSortClasses": false,
				"aaSorting": [[0,'asc']],
				"bAutoWidth": true,
				"bInfo": true,
				"sScrollY": "100%",	
				"sScrollX": "100%",
				"bScrollCollapse": true,
				"sPaginationType": "full_numbers",
				"bRetrieve": true,
				"oLanguage": {
								"sSearch": "Search:",
								"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
								"sInfoEmpty": "Showing 0 to 0 of 0 entries",
								"sZeroRecords": "No matching records found",
								"sInfoFiltered": "(filtered from _MAX_ total entries)",
								"sEmptyTable": "No data available in table",
								"sLengthMenu": "Show _MENU_ entries",
								"oPaginate": {
												"sFirst":    "First",
												"sPrevious": "Previous",
												"sNext":     "Next",
												"sLast":     "Last"
											  }
							 }
	} );
} );
</script> 
</div>