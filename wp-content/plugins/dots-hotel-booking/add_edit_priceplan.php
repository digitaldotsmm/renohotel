<?php
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if(isset($_POST['act']) && $_POST['act'] == 1){
	include("includes/db.conn.php");
	include("includes/conf.class.php");
	include("includes/admin.class.php");
	if($_POST['roomtype_edit'] > 0){
		$bsiAdminMain->priceplan_edit($_POST['roomtype_edit']);
	}else{
		$bsiAdminMain->priceplan_add_edit();
	}
	exit;
}
$pageid = 36; 
include("includes/conf.class.php"); 
include("includes/admin.class.php");
$getHTML  = array();
$getHTML1 = array();
$getHTML2 = array();
$row      = array();
$id=$bsiCore->ClearInput($_REQUEST['rtype']);
if($id){
	$text     = '';
	$start_dt = mysql_real_escape_string($_REQUEST['start_dt']);
	if($start_dt != '0000-00-00'){
		
		$row=mysql_fetch_assoc(mysql_query("SELECT DATE_FORMAT(start_date, '".$bsiCore->userDateFormat."') AS start_date1,
		DATE_FORMAT(end_date, '".$bsiCore->userDateFormat."') AS end_date1, start_date, end_date,roomtype_id,capacity_id,sun,mon,tue,wed,thu,fri,sat FROM `dots_priceplan`
		where `plan_id`='".$id."' and start_date='".$start_dt."' and default_plan=0"));
		
	}else{
		
		$row = mysql_fetch_assoc(mysql_query("SELECT DATE_FORMAT(start_date, '".$bsiCore->userDateFormat."') AS start_date1,
		DATE_FORMAT(end_date, '".$bsiCore->userDateFormat."') AS end_date1, start_date, end_date,roomtype_id,capacity_id,sun,mon,tue,wed,thu,fri,sat FROM `dots_priceplan`  
		where `plan_id`='".$id."' and start_date='".$start_dt."' and default_plan=1"));
		
	}
	$rtypeName = mysql_fetch_assoc(mysql_query("select * from dots_roomtype where roomtype_ID='".$row['roomtype_id']."'"));
	
	$getHTML   = $bsiAdminMain->getDatepicker($id, $rtypeName['type_name'], $row['start_date'], $row['end_date'], $row);
	
	$getHTML1  = $getHTML['html'];
	
	$getHTML2  = $getHTML['editPriceplanHTML'];
        
	
	$text      = '';
	
}else{
	$row=NULL;
	$getHTML   = $bsiAdminMain->getDatepicker();
	
	$getHTML1  = $getHTML['html'];
	
	$getHTML2  = $getHTML['editPriceplanHTML'];
        
        
	
	$start_dt  = '0000-00-00';
	
	$text      = "PLEASE SELECT ROOMTYPE FROM DROPDOWN";
}
?>
<!-- Load JQuery -->
<script type="text/javascript" charset="">
   jQuery(document).ready(function() {
   jQuery("#priceplanaddeit").validate();
   jQuery('#roomtype_id').change(function() {
         if(jQuery('#roomtype_id').val() != 0){
			var querystr = 'actioncode=3&roomtype_id='+jQuery('#roomtype_id').val(); 
	
			jQuery.post("<?php echo HOTELBOOKING_MANAGER_URL;  ?>admin_ajax_processor.php", querystr, function(data){												 
				if(data.errorcode == 0){
					 jQuery('#default_capacity').html(data.strhtml)
				}else{
				    jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">Not Found</span>")
				}
			}, "json");
		} else {
		 jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'Please select Room Type from combo box'</span>")
		}
	});
	
	if(jQuery('#roomtype').val() == 0){
		jQuery('#default_capacity').html("<span style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px;\">'Please select Room Type from combo box'</span>")
	}
});
 
jQuery(document).ready(function(){
	
	jQuery.datepicker.setDefaults({ dateFormat: '<?php echo $bsiCore->config['conf_dateformat'];  ?>' });
   jQuery("#txtFromDate").datepicker({
        minDate: 0,
        maxDate: "+365D",
        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",
        onSelect: function(selected) {
        var date = jQuery(this).datepicker('getDate');
        if(date){
            date.setDate(date.getDate() + 2);
        }
          jQuery("#txtToDate").datepicker("option","minDate", date)
        }
    });
   jQuery("#txtToDate").datepicker({ 
        minDate: 0,
        maxDate:"+365D",
        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",
        onSelect: function(selected) {
           jQuery("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });
	
	jQuery("#txtFromDate").datepicker();
	jQuery("#datepickerImage").click(function() { 
		jQuery("#txtFromDate").datepicker("show");
	});
	
	jQuery("#txtToDate").datepicker();
	jQuery("#datepickerImage1").click(function() { 
		jQuery("#txtToDate").datepicker("show");
	});    
});
</script>
<div id="container-inside" class="wrap">
    <h2>Price Plan Add / Edit</h2>
<input type="button" value="Back to Price Plan" onClick="window.location.href='admin.php?page=price-plan'" class="button-secondary" />
     <hr style="margin-top:10px;" />
  <form action="<?php echo admin_url('admin.php?page=add-price-plan&noheader=true'); ?>" method="post" id="form1">
    <input type="hidden" name="roomtype_edit" value="<?php echo $id?>" />
    <input type="hidden" name="roomtype" value="<?php echo $row['roomtype_id']?>" />
    <input type="hidden" name="start_date_old" value="<?php echo $start_dt?>" />
    <input type="hidden" name="act" value="1" />
    <table cellpadding="5" cellspacing="2" border="0">
      <tr>
        <td colspan="2" align="center" style="font-size:14px; color:#006600; font-weight:bold"><?php if(isset($error_msg)) echo $error_msg; ?></td>
      </tr>
      <?php echo $getHTML1?>
      <tr>
        <td id="default_capacity" colspan="2">
          <?php echo $text?>
          <?php echo $getHTML2?>
        </td>
      </tr> 
    </table>
  </form>
  
</div>   
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#form1").validate();
     });    
</script> 
</div>