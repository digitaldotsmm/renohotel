<?php 
if ( ! defined( 'HOTELBOOKING_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
if(isset($_POST['sbt_details'])){
	include("includes/db.conn.php");
	include("includes/admin.class.php");
	include("includes/conf.class.php");
	$bsiAdminMain->hotel_details_post();
	header("location:admin.php?page=hotel-details");
}
include("includes/conf.class.php");
?>
<div id="container-inside" class="wrap">
    <h2>Hotel Details</h2>
<hr />
  <form action="<?php echo admin_url('admin.php?page=hotel-details&noheader=true'); ?>" method="post" id="form1">
    <table cellpadding="5" class="widefat" cellspacing="2" border="0">
      <tr>
        <td valign="middle" width="200px"><strong>Hotel Name:</strong></td>
        <td><input type="text" name="hotel_name" class="required" size="50" value="<?php echo $bsiCore->config['conf_hotel_name']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>Street Address:</strong></td>
        <td><input type="text" name="str_addr" class="required" size="40" value="<?php echo  $bsiCore->config['conf_hotel_streetaddr']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>City:</strong></td>
        <td><input type="text" name="city" size="30" class="required" value="<?php echo  $bsiCore->config['conf_hotel_city']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>State:</strong></td>
        <td><input type="text" name="state" class="required" size="30" value="<?php echo  $bsiCore->config['conf_hotel_state']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>Country:</strong></td>
        <td><input type="text" name="country" class="required" size="30" value="<?php echo $bsiCore->config['conf_hotel_country']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>Zip / Potal Code:</strong></td>
        <td><input type="text" name="zipcode" class="required" size="10" value="<?php echo $bsiCore->config['conf_hotel_zipcode']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong> Phone Number:</strong></td>
        <td><input type="text" name="phone" class="required" size="15" value="<?php echo  $bsiCore->config['conf_hotel_phone']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>Fax:</strong></td>
        <td><input type="text" name="fax" class="" size="15" value="<?php echo $bsiCore->config['conf_hotel_fax']; ?>"/></td>
      </tr>
      <tr>
        <td valign="middle"><strong>Email Id:</strong></td>
        <td><input type="text" name="email" class="required email" size="30" value="<?php echo $bsiCore->config['conf_hotel_email']; ?>"/></td>
      </tr>
      <tr>
        <td></td>
        <td><input type="submit" value="Submit" name="sbt_details" id="sbt_details" class="button-primary"  /></td>
      </tr>
    </table>
  </form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#form1").validate();
		
     });
         
</script> 
<script src="<?php echo HOTELBOOKING_MANAGER_URL; ?>js/jquery.validate.js" type="text/javascript"></script>
