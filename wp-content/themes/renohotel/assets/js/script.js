/*!
 *      Author: DigitalDots
	name: script.js	
	requires: jquery	
 */

var ddFn = {
    
    init: function(){
        Global = new this.Global();
        Utils = new this.Utils();     
        Global.__init();        
    },
    

    Global:function(){ 
        
        this.__init = function(){             
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/mmexpress/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }  
        
        
    }, // end of Global

    Utils: function(){
        //this.exist= false;
        this.validate_email = function (email){	
            var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            //var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
            if(filter.test(email)){
                return true;
            }
            else{
                return false;
            }
        };	
	
        this.format_my_date = function(dateStr) {
            return dateStr.replace(/^([a-z]{3})( [a-z]{3} \d\d?)(.*)( \d{4})$/i, '$1,$2$4$3');
        };
	
        this.remove_GMTorUTC = function(dateStr) {
            var noGMT = dateStr.replace(/^(.*)(:\d\d GMT+)(.*)$/i, '$1');  // Trim string like 'Thu Apr 21 2011 14:08:46 GMT+1000 (AUS Eastern Standard Time)'
            var noUTC = noGMT.replace(/^(.*)(:\d\d UTC+)(.*)$/i, '$1');    // Trim string like 'Thu Apr 21 14:08:46 UTC+1000 2011'
            return noUTC;
        };
        
        this.is_user_email_exist = function(email, callback){                 
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_email_exist',
                    'email'  :   email     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){
                        return callback (true);
                    }else{
                        return callback (false);
                    }
                }
            });            
        }; 
        this.is_user_name_exist = function(username, callback){              
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_name_exist',
                    'username'  :   username     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){                          
                        return callback (true);
                    }else{
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_pass_valid = function(username, userpass, callback){           
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data:  {
                    action : "is_password_exist", 
                    'id'   : username,
                    'pass' : userpass
                }, 
                async: false,
                dataType: "json",
                success:function(response){                          
                    if(response){                                 
                        return callback (true);
                    }else{                         
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_logged_in = function(callback){
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_logged_in'                     
                },
                async: false,
                dataType:"json",
                success:function(response){                       
                                               
                    return callback (response.is_logged_in);
                     
                }
            });      
        }
         
    } 
   
};

var Global;
var Utils;
var Show;
$ = $.noConflict();
$(document).ready(function(){  
    ddFn.init();

    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // $(function() {
    $( "#checkin" ).datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: "-0D"
    });
    // });
    
    // $(function() {
    $( "#checkout" ).datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: "-0D"
    });

    $( "#txtFromDate" ).datepicker({
        minDate: "-0D",
        maxDate: "+365D",
        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",
        onSelect: function(selected) {
            $("#txtToDate").datepicker("option","minDate", date)
        } 
    });

    $( "#txtToDate" ).datepicker({
        minDate: "-0D",
        maxDate: "+365D",
        numberOfMonths: 2,
        dateFormat : "dd/mm/yy",
        onSelect: function(selected) {
            $("#txtToDate").datepicker("option","minDate", date)
        } 
    });
         
       
    //pagination li 
    $('ul.pagination a').wrap('<li></li>');
    $('ul.pagination span').wrap('<li></li>');

    // var tabsFn = (function() {
  
    //   function init() {
    //     setHeight();
    //   }
          
    //   function setHeight() {
    //     var $tabPane = $('.tab-pane'),
    //         tabsHeight = $('.nav-tabs').height();
            
    //     $tabPane.css({
    //       height: tabsHeight
    //     });
    //   }
            
    //   $(init);
    // })();

    // });

    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false,
        animation_speed: 'normal', // fast/slow/normal 
        slideshow: 5000, // false OR interval time in ms
        autoplay_slideshow: false, // true/false
        opacity: 0.80, // Value between 0 and 1 
        show_title: true, // true/false            
        allow_resize: true, // Resize the photos bigger than viewport. true/false
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/', // The separator for the gallery counter 1 "of" 2
        theme: 'pp_default', // light_rounded / dark_rounded / light_square / dark_square / facebook
        horizontal_padding: 20, // The padding on each side of the picture 
        hideflash: false, // Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto
        wmode: 'opaque', // Set the flash wmode attribute
        autoplay: true, // Automatically start videos: True/False 
        modal: false, // If set to true, only the close button will close the window
        deeplinking: true, // Allow prettyPhoto to update the url to enable deeplinking. 
        overlay_gallery: true, // If set to true, a gallery will overlay the fullscreen image on mouse over 
        keyboard_shortcuts: true, // Set to false if you open forms inside prettyPhoto 
        changepicturecallback: function () {}, // Called everytime an item is shown/changed 
        callback: function () {} // Called when prettyPhoto is closed
    });

    

    $(window).load(function() {
        $('#slider').nivoSlider();
        $('#slider0').nivoSlider();

        $('#slider1').nivoSlider();
    });

    $(function(){
        $('.crsl-items').carousel({
            autoRotate: 4000,  
            visible: 1,
            itemMinWidth: 180,
            itemEqualHeight: 370,
            itemMargin: 9,
        });
      
        $("a[href=#]").on('click', function(e) {
            e.preventDefault();
        });
    });
    
    
 
});

function show_hide(id)
{
    
    if(id==1)
    {
        document.getElementById("down_img").style.display='none';
        document.getElementById("up_img").style.display='';
        document.getElementById("on_off_div").style.display='block';
    }

    if(id==2)
    {
        document.getElementById("down_img").style.display='';
        document.getElementById("up_img").style.display='none';
        document.getElementById("on_off_div").style.display='none';
    }

}