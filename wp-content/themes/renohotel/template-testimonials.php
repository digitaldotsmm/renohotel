<?php
/*
 * Template Name:Reviews Page
 */
?>
<?php
    $email_sent = false;

    if( isset($_POST['submit']) ){                 
       // setup to create new post             
        $new_post['post_type'] = RN_REVIEW;
        $new_post['post_status'] = 'publish';
        $new_post['post_title'] = test_input($_POST['customer_name']);
        $new_post["post_content"] = test_input($_POST["message"]);

        $data["email"] = test_input($_POST["email"]);
        $data["location"] = test_input($_POST["location"]);
        $data["review_title"] = test_input($_POST["review_title"]);
        $data["date"] = test_input($_POST["month_stay"]);
        
        

        // create post
        $post_id = wp_insert_post($new_post);
        // update meta only if post is created successfully
        if (!is_wp_error($post_id)) {
            //update post meta
            foreach ($data as $meta_key => $meta_value) {
                update_post_meta($post_id, $meta_key, $meta_value);
            }

            // send mail to Admin about new testimonial
            $from = test_input($_POST['email']);
            $name = test_input($_POST['title']) . " " . test_input($_POST['customer_name']);
            $subject = 'A New Testimonial had been posted by ' . $new_post['customer_name'] . '.';
            $body = "Hi Admin,<br /><br />
                                    A New Testimonial had been posted by " . test_input($new_post['customer_name']) . ". <br />                                                
                                    You can reveiw and publish the testimonial from following link.<br />
                                    <a href=\"" . WP_HOME . "/wp-admin/edit.php?post_type=review\"> Review Testimonial</a><br /> <br /> 
                                    Name      : " . $name . "<br/> 
                                    Email     : " . test_input($_POST['email']) . "<br/>                  
                                    Message   : " . test_input($_POST["messages"]) . "    
                                    Best Regards, <br />
                                    Digital Dots";
            $content .= "<html>\n";
            $content .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;\">\n";
            $body = html_entity_decode($body);
            $content .= $body;
            $content .= "\n\n";
            $content .= "</body>\n";
            $content .= "</html>\n";
            $headers[] = 'From: ' . $name . ' <' . $from . '>';
            $headers[] = "MIME-Version: 1.0\n";
            $headers[] = "Content-type: text/html; charset=UTF-8\r\n";
            $to = $THEME_OPTIONS['feedback_receive_mail'];
            if (!$to) {  // use admin mail if there is no reservation form mail
                $to = get_option('admin_email');
            } else {
                if (strpos($to, ',')) { // if it is comman separated, changed to array
                    $to = explode(',', str_replace(' ', '', $to));
                }
            }
           // if (1 == 1) {
            if(wp_mail($to, $subject, $content, $headers)){
                $email_sent = true;
            }
    }
}
?>
<?php get_header();?>

<section class="section reviews">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">

                <?php 
                    global $wp_query;   
                    $i=1;  
                    $wrap_div='<div class="row">';   
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;  
                    $wp_query= new WP_Query(array('post_type'=>RN_REVIEW,'posts_per_page'=>4,'paged'=>$paged));
                    if($wp_query->have_posts()):
                        echo '<div class="row">';
                        while ($wp_query->have_posts()):$wp_query->the_post();  

                            $title=get_the_title();
                            $content=get_the_content();
                ?>                                                 
                            <div class="col-md-6 each_review">
                                <div class="testimonials">
                                    <blockquote>
                                        <h3><?php echo get_field('review_title');?></h3>
                                        <div class="review_date"><i class="fa fa-calendar"></i><?php echo get_field('date');?></div>
                                        <p><?php echo $content;?></p>
                                        <div class="review_info">
                                            <div class="review_title"><i class="fa fa-user"></i><?php echo $title;?></div> - 
                                            <div class="review_location"><?php echo get_field('location');?></div>
                                        </div>
                                    </blockquote>
                                </div>
                            </div>
                    <?php if ($i % 2 === 0 ) { echo '</div>' . $wrap_div; } $i++; endwhile; echo '</div>';?>

                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                  <?php dd_pagination(); ?>
                              </ul>
                          </nav>
                    <?php
                            else:
                             wp_reset_query();
                        endif;
                  ?>   
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                  <?php if( !$email_sent ): ?>
                    <h3 class="title1">Write a Review</h3>
                <?php endif; ?>
                <div class="testimonial-wrap row">
                    <?php if( $email_sent ): ?>
                        <div class="alert alert-success" style="display: block;">Thanks For Your Message.</div>
                    <?php endif; ?>
                    <form action="" id="testimonial_form" role="form" enctype="multipart/form-data" method="post">

                        <div class="col-md-12">
                            <label for="contactName">Full Name *</label>
                            <input type="text" name="customer_name" id="customer_name" value="" class="input-text form-control required">                       
                        </div>

                        <div class="col-md-12">
                            <label for="email">Email *</label>
                            <input type="text" name="email" id="email" value="" class="input-text email form-control required">                       
                        </div>

                        <div class="col-md-12">
                            <label for="country">Country *</label>
                            <select name="location" id="country" class="form-control required">
                                <?php
                                $cou = array('Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas,The', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia', 'Botswana', 'Bougainville', 'Brazil', 'British Indian Ocean', 'British Virgin Islands', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde Islands', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China, Hong Kong', 'China, Macau', 'China, People \'s Republic', 'China, Taiwan', 'Colombia', 'Comoros', 'Congo, Democratic Republic of', 'Congo, Republic of', 'Cook Islands', 'Costa Rica', 'Cote d\'Ivoire', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Faeroe Islands', 'Falkland Islands', 'Federated States of Micronesia', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'Gabon', 'Gambia, The', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Holy See (Vatican City State)', 'Honduras', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, Democratic People\'s Rep', 'Korea, Republic of', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Antilles', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Norway', 'Oman', 'Pakistan', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'R&ecirc;union', 'Romania', 'Russia', 'Rwanda', 'Saint Barthelemy', 'Saint Helena', 'Saint Kitts & Nevis', 'Saint Lucia', 'Saint Martin', 'Saint Pierre & Miquelon', 'Saint Vincent', 'Samoa', 'San Marino', 'Sao Tom&ecirc; & Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Tajikistan', 'Tanzania', 'Thailand', 'Timor Leste', 'Togo', 'Tokelau Islands', 'Tonga', 'Trinidad & Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks & Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom of GB & NI', 'United States of America', 'Uruguay', 'US Virgin Islands', 'Uzbekistan', 'Vanuatu', 'Venezuela', 'Vietnam', 'Wallis & Futuna Islands', 'Yemen', 'Zambia', 'Zimbabwe');
                                for ($c = 0; $c < 226; $c++) {
                                    if (isset($_POST['country']) AND $_POST['country'] == $cou[$c])
                                        $select = "selected='selected'"; else
                                        $select = "";
                                    echo "<option value='" . $cou[$c] . "'" . $select . " style='text-align:left;margin-left:5px;' >" . $cou[$c] . "</option>";
                                }
                                ?>
                            </select>                     
                        </div>

                        <div class="col-md-12">
                            <label for="month_stay">Month of Stay *</label>
                            <select name="month_stay" id="country" class="form-control required">
                                <?php
                                    $months = array("January", "February", "March","April","May","June","July","August","September","October","November", "December");
                                    foreach ($months as $month) {
                                        echo "<option value='" . $month ." ".date(Y) ."'>" . $month ." ".date(Y) . "</option>";
                                    }
                                ?>     
                            </select>                  
                        </div>

                        <div class="col-md-12">
                            <label for="subject">Caption for your review *</label>
                            <input type="text" name="review_title" id="subject" value="" class="input-text form-control required">                       
                        </div>

                        <div class="col-md-12">
                            <label for="add_msg">Your Review </label>
                            <textarea name="message" id="messages" class="input-textarea form-control required"></textarea>
                        </div>

                        <div class="col-md-12">
                            <input class="btn btn-main formButton mainbtn" name="submit" type="submit" value="Add Your Mind">
                            <input type="hidden"  name="form_type"  value="testimonial_form" />
                            <input type="hidden"  name="action"  value="sendmail" />
                        </div>

                    </form>
                </div>
              </div>
        </div>
    </div>
</section> 
  
<script type="text/javascript">
    
      jQuery('#testimonial_form').submit(function(){
          $valid = true;
          
          $(this).find('.required').each(function(){              
              var $ele = $(this);
              var $val = $(this).val();                       
              
              // reset validation
              $ele.removeClass('invalid');
              
              // check empty
              if( $val == "" ){
                  $ele.addClass('invalid');
                  $valid = false;
              }
              
              // check email
              if( $ele.hasClass('email')){
                  if( $val.indexOf('@')== -1 ){
                      $ele.addClass('invalid');
                      $valid =  false;
                  }
              }
          });          
          return $valid;
          
      });
       
      
    
//    jQuery('#testimonial_form').submit(ajaxSubmit);
	 
//	 var rsjqu = jQuery.noConflict();
	 
//	 function showErrorField(DOM)
//	 {
//		DOM.addClass("required");
//	 }
//	 function ajaxSubmit()
//	 {

//		$("#testimonial_form *").each(function(){
//			$(this).removeClass("required");
//		});
//		
//		var newCustomerForm = jQuery(this).serialize();
//		
//		
//			if($("#customer_name").val()=="")
//			{
//				showErrorField($("#customer_name"));	
//                                $("#customer_name").addClass('invalid');
//				return false;		
//			}  
//			else if($("#subject").val()=="")
//			{
//				showErrorField($("#subject"));	
//                                 $("#subject").addClass('invalid');
//				return false;		
//			} 			
//			else if($("#email").val().indexOf('@')== -1)
//			{
//				showErrorField($("#email"));	
//                                $("#email").addClass('invalid');				
//				return false;	
//			}
//			else if($("#location").val()=="")
//			{
//				showErrorField($("#location"));	
//                                $("#location").addClass('invalid');
//				return false;	
//			}
//			else if($("#messages").val()=="")
//			{
//				showErrorField($("#messages"));				
//                                $("#messages").addClass('invalid');
//				return false;	
//			}
//		return true;
	
//}

</script>

<?php get_footer();?>
                