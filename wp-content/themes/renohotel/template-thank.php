<?php
/*
 * Template Name: Thank
 */
?>
<?php get_header(); ?>
<?php
if ($_REQUEST['vpc_TxnResponseCode']) {
    include_once( HOTELBOOKING_MANAGER_PATH . "/front/cb.php" );
} elseif ($_REQUEST['respCode']) {
    include_once( HOTELBOOKING_MANAGER_PATH . "/front/mpu.php" );
}
?>

<section class="section about_us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">    
                    <h2 class="page-h2"><?php the_title(); ?></h2>                  
                    <div>
                    <?php
                        if (have_posts()):
                            while (have_posts()):the_post();
                                the_content();
                            endwhile;
                        endif;
                    ?>
                    <?php
                        if($message){
                            echo $message;
                        }
                    ?>
                    </div>                  
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>

