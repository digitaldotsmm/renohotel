<?php
/*
 * Template Name:Contact
 */
?>
<?php get_header(); ?>
<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="contact_text"><?php echo apply_filters('the_content',$post->post_content);?></div>
				<div class="row contact_info">
					<?php echo do_shortcode('[contact-form-7 id="81" title="Contact form 1"]');?>
				</div>
				<div class="map">
					<iframe src="https://www.google.com/maps/d/embed?mid=1n14fqeKWQRM5dL08PcTvtB7dMtQ&z=15" width="100%" style="min-height:400px;"></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
