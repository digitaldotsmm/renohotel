<?php
/*
 * Template Name:Special Offers
 */
?>
<?php get_header(); ?>

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12"> 
				<?php 
		    		$special_offers=get_field('special_offer');
		    		$wrap_div= '<div class="row">';
		    		$i=1;
		    		if ($special_offers) {
		    			echo $wrap_div;
		    			foreach ($special_offers as $key => $special_offer) {
		    				$special_img=  aq_resize($special_offer['image'],555,416,true,true,true);        
				?>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 offer_wrapper">
					    		<div class="offer-image">
					    			<div class="icon_per">
						    			<img src="<?php echo ASSET_URL;?>images/badgeframe.png" alt="Special Offer" class="special_icon">
						    			<h4 class="special_per"><?php echo $special_offer['discount_percentage'];?> % </h4>
					    			</div>
					    			<img src="<?php echo $special_img;?>" alt="<?php echo $special_offer['title'];?>" class="img-responsive special_img">
					    		</div>
					    		<div class=" offer-heading">
					    			<h5><?php echo $special_offer['title'];?></h5>
					    			<?php echo $special_offer['content'];?>
					    			<form action="<?php echo WP_HOME; ?>/search-room/" method="post" class="book_special">                                               
			                            <input value="Book Now" name="submit" class="read_more" type="submit">
			                            <input type="hidden" class="start_date" name="start_date" value="<?php echo $special_offer['start_date']?>" />
			                        </form>
					    		</div>
					    	</div>
				<?php
		    				if ($i % 2 === 0 ) { echo '</div>' . $wrap_div; } $i++;
		    			}
		    			echo '</div>';
		    		}
		    	?>
			</div>
		</div>
	</div>
</section>
	
<?php get_footer(); ?>