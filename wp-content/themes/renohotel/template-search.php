<?php
/*
 * Template Name:Search
 */
?>
<?php
    if (isset($_REQUEST['vpc_Merchant'])) {
        if (have_posts()):
            while (have_posts()):the_post();
                the_content();
            endwhile;
        endif;
    } else {
?>
<?php get_header(); ?>
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                 
                    <?php
                        if (have_posts()):
                            while (have_posts()):the_post();
                                the_content();
                            endwhile;
                        endif;
                    ?>
                    </div>   
                </div>                 
            </div>
        </section>
<?php get_footer(); ?>
<?php }?>