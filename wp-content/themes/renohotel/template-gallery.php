<?php
/*
 * Template Name:Gallery
 */
?>
<?php get_header(); ?>

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12"> 
				<div class="row offers-block">
							<div class="col-md-12">
								<div class="itemblock-special-offers three">
									<?php 
										$gals=get_field('gallery',RN_GALLERY,true);
				                    	foreach($gals as $gal):	                        	
								  			$gal_imgs=aq_resize($gal["url"],350,355,true,true,true);
								  			
					                ?> 
											<div class="col-md-4 col-sm-6 col-xs-6 i-block offer">

												<a href="<?php echo $gal["url"]; ?>" rel="prettyPhoto[roomgal]" class="prettyPhoto">
												<figure>
													<img src="<?php echo $gal_imgs;?>" alt="<?php echo $gal['title']?>" class="img-responsive">
													<figcaption>
													<h4><?php echo $gal['title']?></h4>
													</figcaption>
												</figure>
												</a>
											</div>
									<?php 
								            
					                        endforeach;	                       
						            ?>
								</div>
							</div>
						</div>
			</div>
		</div>
	</div>
</section>
	
<?php get_footer(); ?>