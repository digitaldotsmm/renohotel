<!-- room_margin class and room_img class in 4 or more page -->
<?php get_header(); ?>
<section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<?php 
                		if (have_posts()) : 
                            while (have_posts()) : the_post(); 
                                $img_url= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                                $bnew_img=  aq_resize($img_url[0],585,400,true,true,true);                                
                                $title=get_the_title();
                                $content=get_the_content();             
                    ?>      
                                <div class="row room_margin">
                                    <div class="col-md-6 no_padding">
                                        <div class="content_wrapper">
                                            <h2 class="title1">Information</h2>
                                            <div class="room_info"> <?php echo $content;?> </div>
                                            <?php $usd_price = dd_get_room_price($post->ID);?>
                                            <div class="acc_price">USD : $ <?php echo $usd_price ?></div>
                                            <form action="<?php echo WP_HOME; ?>/search-room/" method="post" class="">
                                                <input value="Book Now" name="submit" class="book_room read_more" type="submit">
                                                <?php the_search_hidden_fields(); ?>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-6 no_padding">
                                        <img src="<?php echo $bnew_img;?>" alt="<?php echo $title;?>" class="img-responsive room_img">   
                                    </div>
                                </div>

                                <div class="row room_margin">
                                    <div class="col-md-6 no_padding">
                                        <div class="amenities_short_info">
                                            <h2 class="title1">Amenities</h2>
                                            <div class="room_info"><?php echo get_field('amenities_short_info');?></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 no_padding">
                                        <div class="amenities_items_wrapper">
                                            <?php 
                                                $i=1;
                                                $amenities=get_field('amenities');
                                                $wrap_div='<div class="row">';
                                                if($amenities) {
                                                    echo '<div class="row">';
                                                foreach ($amenities as $am_key=>$amenity) {                                                   
                                            ?>
                                                    <div class="col-sm-6">
                                                        <label><?php echo $amenity['label'];?> </label>
                                                        <p class="item-text"> <?php echo $amenity['value'];?></p>
                                                    </div>
                                            <?php
                                                    if ($i % 2 === 0 ) { echo '</div>' . $wrap_div; }
                                                    $i++;
                                                }
                                                echo '</div>';
                                            } //end if
                                            ?>
                                        </div>
                                    </div>
                                </div>
                    			 
                    			<!-- gallery -->

                                <div class="row room_margin">
                                    <div class="col-md-4 no_padding">
                                        <div class="gal_content_wrapper">
                                            <h2 class="title1">Gallery</h2>
                                            <div class="room_info"><?php echo get_field('gallery_content');?></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 no_padding">
                                        <div class="gal_wrapper">
                                            <?php $gals=get_field('gallery');if($gals) {?>
                                                <div id="slider" class="nivoSlider">
                                                    <?php $sliders=get_field('slider');?>
                                                    <?php 
                                                        foreach ($gals as $key => $gal) {                                                        
                                                        $gal_imgs=  aq_resize($gal["url"],780,400,true,true,true);    
                                                    ?>

                                                            <img src="<?php echo $gal_imgs;?>" alt="<?php echo $gal['title'];?>" title="<?php echo $gal['title'];?>" />
                                                    <?php }?>           
                                                </div>
                                            <?php }?>  
                                        </div>
                                    </div>
                                </div>
                    <?php endwhile; 
                            else:
                             wp_reset_query();
                        endif;
                 	?>      
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
