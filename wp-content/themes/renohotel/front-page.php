<?php get_header();?>

<div id="welcome_id" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 welcome_contanear">
	<div class="container">
		<div class="welcome_text_area">
		    <h2>welcome to</h2>
		    <h1>Reno Hotel</h1>
		    <?php echo get_field('welcome_message');?>
		    <a href="<?php echo get_permalink(RN_ABOUT); ?>" title="Read More About Us" class="more_about_but" >More About Us</a>
	    </div>
	</div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rooms_contanear">
	<div class="container">
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rooms_title_area">
		    <h2>check our</h2>
		    <h1>rooms</h1>
		    <p><?php $roompage=get_post(RN_ROOM_PAGE);echo $roompage->post_content;?></p>
	    </div>
    
	    <div class="rooms_boxes_main_area">
		    <div class="row">
		    	<?php 
					$rooms = get_posts(
				        array(
				            'post_type' => RN_ROOM,
				            'posts_per_page' => 3,
				            'orderby' => 'rand'
				        )
					);
					foreach ($rooms as $key => $room) :
						$room_title=$room->post_title;
						$room_content=$room->post_content;
						$room_link=get_permalink($room->ID);
						$room_img = get_attachment_image_src($room->ID, 'full');	
						$roomnew_img=aq_resize($room_img[0],500,375,true,true,true);	
						// var_dump($room_img);
				?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				        	<div class="rooms_box1">
				            	<div class="rooms_box1_img_and_but_area">
				                	<div class="rooms_box1_img_area"><img src="<?php echo $roomnew_img;?>" alt="<?php echo $room_title;?>" class="img-responsive"></div>
				                    <div class="rooms_box1_but_area">
					                    <a href="<?php echo $room_link; ?>" title="<?php echo $room_title;?>" class="rooms_details_but">detail</a>
					                    <form action="<?php echo WP_HOME; ?>/search-room/" method="post" class="Book-now-room">
	                                        <input value="Book Now" name="submit" class="rooms_book_but" type="submit">
	                                        <img src="<?php echo ASSET_URL;?>images/book_but_arrow.png" alt="arrow" class="hover_arrow">
	                                        
	                                        <?php the_search_hidden_fields($room->ID); ?>
	                                    </form>
					                    <!-- <a href="" class="rooms_book_but"><img src="<?php //echo ASSET_URL;?>images/book_but_arrow.png" alt="" />book now</a> -->
				                    </div>
				                </div>
				                <div class="rooms_book_text_area">
					                <a href="<?php echo $room_link; ?>" title="<?php echo $room_title;?>"><h2><?php echo $room_title;?></h2></a>
					                <p><?php echo string_limit_words(strip_tags($room_content), 19) . '...';?></p>
				                </div>
				            </div>
				        </div>						
				<?php endforeach;	?>
		    </div>    
	    </div>
    </div>
</div>


<div class="meeting_panel">
	<div class="container">
    	<div class="meeting_panel_inner">
        	<div class="meeting_panel_inner_left">
            	<?php  
					$meet_room=get_post(RN_METETING_ROOM);
					$mr_img = get_attachment_image_src($meet_room->ID, 'large');	
					$mrnew_img=aq_resize($mr_img[0],538,404,true,true,true);	
					$meet_title=$meet_room->post_title;
				?>
            	<h2>Check our <br><span><?php echo $meet_title;?></span></h2>                
            	<p><?php echo string_limit_words(strip_tags($meet_room->post_content), 20) . '...';?></p>
 				<a href="<?php echo get_permalink(RN_METETING_ROOM); ?>" title="<?php echo $meet_title;?>" >DETAIL</a>
            </div>
            <div class="meeting_panel_inner_right">  
            	<div class="img_border">    
            		<div class="inner_img">
           	 			<img src="<?php echo $mrnew_img;?>" alt="<?php echo $meet_title;?>" class="img-responsive">
           	 		</div> 
       	 		</div>     
            </div>
        </div>       
        <div class="meeting_panel_inner">
        	<div class="meeting_panel_inner_left meeting_panel_inner_left2">
            	<?php  
					$restaurant=get_post(RN_RESTAURANT);
					$res_img = get_attachment_image_src($restaurant->ID, 'large');	
					$resnew_img=aq_resize($res_img[0],538,404,true,true,true);	
					$res_title=$restaurant->post_title;
				?>
            	<h2>Check our <br><span><?php echo $res_title;?></span></h2>
                <p><?php echo string_limit_words(strip_tags($restaurant->post_content), 20) . '...'; ?></p>
 				<a href="<?php echo get_permalink(RN_RESTAURANT); ?>" title="<?php echo $res_title;?>" >DETAIL</a>
            </div>
            <div class="meeting_panel_inner_right meeting_panel_inner_right2">
            	<div class="img_border">    
            		<div class="inner_img"> 
						<img src="<?php echo $resnew_img;?>" alt="<?php echo $res_title;?>" class="img-responsive">   
					</div> 
				</div>       		
            </div>
        </div>        
    </div>
</div>  


<div class="testimonial_contanear">
	<div class="container">
    	<h2>our customer’s<br> <span>reviews</span></h2>
        <div id="w">
		    <div class="crsl-items" data-navigation="navbtns">
		      	<div class="crsl-wrap">

		      		<?php 
						$reviews = get_posts(
					        array(
					            'post_type' => RN_REVIEW,
					            'posts_per_page' => 6,
					            'orderby' => 'rand'
					        )
						);
						foreach ($reviews as $key1 => $review) :
							$review_title=$review->post_title;
							$review_content=$review->post_content;
							$review_link=get_permalink($review->ID);						
					?>
			        	<div class="crsl-item">
			        		<div class="testimonial_main_area">
				                <div class="testimonial_text_area">				                
				                <h3><?php echo get_field('title',$review->ID);?></h3>				                
				                <p><?php echo $review_content;?>
									<span><?php echo $review_title;?> . <?php echo get_field('location',$review->ID);?><br> <?php echo get_field('date',$review->ID);?></span>
								<p>
				                </div>
			            	</div> 		            
			            	<div class="testimonial_text_bottom"><a href="/<?php echo RN_REVIEW; ?>/" title="See All Review" >DETAIL</a></div> 
			        	</div><!-- crsl-item -->
		        	<?php
						endforeach;														
					?>

      			</div><!-- @end .crsl-wrap -->
    		</div><!-- @end .crsl-items -->
    
		    <nav class="slidernav">
		      <div id="navbtns" class="clearfix">
		        <a href="#" class="previous"><img src="<?php echo ASSET_URL;?>images/left_arrow.png" alt="" /></a>
		        <a href="#" class="next"><img src="<?php echo ASSET_URL;?>images/right_arrow.png" alt="" /></a>
		      </div>
		    </nav>  

  		</div> <!-- end w -->        
    </div>
</div> 
<?php get_footer();?>