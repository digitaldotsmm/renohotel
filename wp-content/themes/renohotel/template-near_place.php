<?php
/*
 * Template Name:Near By Places
 */
?>
<?php get_header(); ?>

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12"> 
				<?php 
	                    	$nearby_places=get_field('nearby_places');   
	                    	if($nearby_places):
	                    		echo '<div class="near_place_wrapper">';
	                    		foreach ($nearby_places as $place_key => $nearby_place): 
	                    		
	            				$item_img=aq_resize($nearby_place['image'],653,490,true,true,true);
	                    		if(($place_key+1)%2 ==0 ) {
	        			?>
	        						<div class="row room_margin">
		        						<div class="col-md-7">
		        							<h2 class="title1 visible-sm visible-xs"><?php echo $nearby_place['title'];?></h2>
			                            	<img src="<?php echo $item_img;?>" title="<?php echo $nearby_place['title'];?>" class="img-responsive room_img" />
			                            </div>   
			                            <div class="col-md-5 place_info">
			                            	<h2 class="title1 visible-md visible-lg"><?php echo $nearby_place['title'];?></h2>
			                            	<div class="room_info"><?php echo $nearby_place['content'];?></div>
			                            </div>
		                            </div> <!-- end row-->
	        			<?php
	                    		}else {
	        			?>
	        						<div class="row room_margin">
	        							<div class="col-md-7 visible-sm visible-xs">
	        								<h2 class="title1 visible-sm visible-xs"><?php echo $nearby_place['title'];?></h2>
			                            	<img src="<?php echo $item_img;?>" title="<?php echo $nearby_place['title'];?>" class="img-responsive room_img" />
			                            </div>  

		        						<div class="col-md-5 place_info">
		        							<h2 class="title1 visible-md visible-lg"><?php echo $nearby_place['title'];?></h2>
			                            	<div class="room_info"><?php echo $nearby_place['content'];?></div>
			                            </div>
		        						<div class="col-md-7 visible-md visible-lg">
			                            	<img src="<?php echo $item_img;?>" title="<?php echo $nearby_place['title'];?>" class="img-responsive room_img" />
			                            </div>   
		                            </div> <!-- end row-->
	        			<?php }	?>                
	                        <?php endforeach;echo '</div>'; endif;?>     
			</div>
		</div>
	</div>
</section>
	
<?php get_footer(); ?>