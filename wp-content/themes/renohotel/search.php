<?php get_header(); ?>

	<section class="section about_us">
		<div class="container">
			<div class="row">
				<div class="col-md-12"> 					
		    		<div>
		    			<h2><?php _e("Search Results"); ?></h2>
					
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
					
							<h4><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
							
							<p><?php echo string_limit_words(strip_tags(get_the_content()), 30) . '...'; ?></p>
							<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">View More</a>
							<hr/>
						<?php endwhile; else: ?>
							<p>
								<?php _e('Sorry, no posts matched your criteria.'); ?>
							</p>
						<?php endif; ?>
	    			</div>				    
				</div>
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>