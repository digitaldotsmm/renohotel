<?php
if ($bsiCore == null) {
    if (file_exists(HOTELBOOKING_MANAGER_PATH . "includes/conf.class.php")) {
        include_once( HOTELBOOKING_MANAGER_PATH . "includes/conf.class.php" );
    }
}
if (file_exists(HOTELBOOKING_MANAGER_PATH . "front/languages/" . $bsiCore->config['conf_language'])) {
    include_once(HOTELBOOKING_MANAGER_PATH . "front/languages/" . $bsiCore->config['conf_language']);
}
include(HOTELBOOKING_MANAGER_PATH."includes/admin.class.php");

$start_date=$_POST['start_date'];
?>
<script type="text/javascript">
$ = $.noConflict();
$(document).ready(function(){

        var checkin_val = '<?php echo $start_date ;?>';        
        var dmy = checkin_val.split("/"); 
        var nd='';       
        var joindate = new Date(
            parseInt(dmy[2], 10),
            parseInt(dmy[1], 10) - 1,
            parseInt(dmy[0], 10)
        );                            
        joindate.setDate(joindate.getDate() + 5); // substitute 1 with actual number of days to add                           
        
        nd=("0" + joindate.getDate()).slice(-2) + "/" +
            ("0" + (joindate.getMonth() + 1)).slice(-2) + "/" +
            joindate.getFullYear()
        
        
        if(checkin_val != '') {
           $("#txtToDate").datepicker('setDate', nd); 
        }

        // $( "#roomtype_id" ).change(function () {

        //     var rtid= $( "#roomtype_id" ).val();
        //     if(rtid){
        //                $.ajax({                                                     
        //                 url: '/wp-admin/admin-ajax.php',
        //                 type:"post",                        
        //                 data: {
        //                     action: 'get_adult_per_room',
        //                     'room_id':rtid, 
        //                 },
        //                 async: false,
        //                 dataType:"json",                   
        //                 success:function(response){  
        //                     var selector = $("#capacity"); 
        //                     selector.html('');//remove all default values
        //                     if(response.retur_capicity>0){
        //                         var retur_capicity =response.retur_capicity;                                                                   
        //                         // $("#roomtype_id").val(retur_capicity);                                               
        //                         for (i=1;i<=retur_capicity;i++){
        //                             selector.append($("<option />").val(i).text(i));                               
        //                         }
        //                         $(selector).prop("disabled", false); // Element(s) are now enabled.
        //                     }
        //                     else{
        //                         selector.append($("<option />").val(0).text('Adult Per Room'));
        //                         $("#capacity").val('0');
        //                         $(selector).prop("disabled", true);

        //                     }
        //                 }
        //             }); 
        //         }
               
        // });

}); 
</script>
<section id="reservation-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <?php  
                    $home_adult=$_REQUEST['capacity'];
                    // var_dump($home_adult);
                ?> 
                <form method="post" action="<?php echo WP_HOME; ?>/search-room/" class="form-inline" id="easy_form">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-2">
                            <div class="form-group" data-date-format="dd/mm/yyyy">
                                <label for="checkin" class="reservation-label">Check-in</label>
                                <i class="fa fa-calendar infield"></i>
                                <?php
                                    $checkin='';
                                    if($bsisearch && $bsisearch->checkInDate){$checkin=$bsisearch->checkInDate;}
                                    else if($start_date){$checkin=$start_date;}
                                    else {$checkin=$checkin;}
                                ?>
                                <input name="check_in" type="text" id="txtFromDate" value="<?php echo $checkin ?>" class="form-control required1 datepicker" placeholder="Select a Date"/>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-4 col-md-2" data-date-format="dd/mm/yyyy">
                            <div class="form-group">
                                <label for="checkout" class="reservation-label">Check-out</label>
                                <i class="fa fa-calendar infield"></i>
                                <input name="check_out" id="txtToDate" class="required1 form-control" type="text" value="<?php echo ($bsisearch && $bsisearch->checkInDate) ? $bsisearch->checkOutDate : ''; ?>" placeholder="Select a Date"><span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                       


                        <div class="col-xs-6 col-sm-4 col-md-2">
                            <div class="form-group">
                                <label for="roomtype" accesskey="R" class="reservation-label">Room type</label>
                                <?php
                                //var_dump($bsisearch);
                                $rid = ($bsisearch && $bsisearch->roomTypeID) ? $bsisearch->roomTypeID : '';
                                $roomtypeCombo = $bsiAdminMain->generateRoomtypecombo($rid);
                                echo $roomtypeCombo;
                                ?>

                                <!--                                    <option value="Premier Suite">Premier Suite</option>
                                                                    <option value="Executive Suite">Executive Suite</option>
                                                                    <option value="Deluxe">Deluxe</option>
                                                                    <option value="Superior">Superior</option>
                                                                    <option value="Standard">Standard</option>-->

                            </div>
                        </div>
                        <?php //echo  $no_capicity=get_home_capacity($rid);?>
                        <div class="col-xs-6 col-sm-4 col-md-2 hsf_adult">
                            <div class="form-group">
                                <label for="adult" accesskey="A" class="reservation-label">Adults/Room</label>
                                <select name="capacity" id="capacity" class="selectbox  form-control required">
                                    <?php 
                                        $choice='';
                                        if($home_adult > 0) {

                                            $no_capicity=get_home_capacity($rid);

                                            for ($c=1; $c <= $no_capicity; $c++) { 

                                                if ($c == $home_adult) {
                                                    $choice = ' selected="selected" ';
                                                }
                                                else {
                                                    $choice='';
                                                }
                                                echo "<option " . $choice . " value='" . $c . "'>" . $c . "</option>";
                                            }

                                        }else {
                                            echo '<option value="0">0</option>';
                                        }   
                                    ?>
                                   
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-4 col-md-2">
                            <div class="form-group">
                                <label for="adult" accesskey="A" class="reservation-label">Child</label>
                                <select name="child_per_room" class="selectbox form-control">
                                    <?php
                                    if ($bsisearch) {
                                        $no_of_child = $bsisearch->childPerRoom;
                                    }
                                    for ($child = 0; $child <= 10; $child++) {
                                        $selected = "";
                                        if ($child == $no_of_child) {
                                            $selected = ' selected="selected" ';
                                        }
                                        echo "<option " . $selected . " value='" . $child . "'>" . $child . "</option>";
                                    }
                                    ?>  
                                </select>
                            </div>
                        </div>   


                        <div class="col-xs-6 col-sm-4 col-md-2">
                            <label></label>
                            <input type="submit" id="btn_room_search" name="home_reservation" class="btn btn-primary btn-block" value="SEARCH">
<!--                            <input type="submit" name="home_reservation" id="btn_room_search" class="btn btn-large btn-primary" value="Check Room Availability" />-->
                        </div>


                    </div><!--end of row-->



                    <!--                    <div class="pull-left">
                                            <label class="control-label"><strong>CHECK IN</strong></label><br />
                                            <div class="input-append date" id="dp1" data-date-format="dd/mm/yyyy">
                                                <input name="check_in" id="txtFromDate" class="required1" size="16" type="text" value="<!?php echo ($bsisearch && $bsisearch->checkInDate) ? $bsisearch->checkInDate : ''; ?>" readonly><span class="add-on"><i class="icon-th"></i></span>
                                            </div>
                                        </div>
                                        <div class="pull-left">
                                            <label class="control-label"><strong>CHECK OUT</strong></label><br />
                                            <div class="input-append date" id="dp2" data-date-format="dd/mm/yyyy">
                                                <input name="check_out" id="txtToDate" class="required1" size="16" type="text" value="<!?php echo ($bsisearch && $bsisearch->checkInDate) ? $bsisearch->checkOutDate : ''; ?>" readonly><span class="add-on"><i class="icon-th"></i></span>
                                            </div>
                                        </div>
                                        <div class="controls  pull-left">
                                            <label class="control-label"><strong>ADULTS</strong></label><br>
                                            <select name="capacity" id="capacity" class="selectbox">
                                                <!?php
                                                if ($bsisearch) {
                                                    $no_of_guests = $bsisearch->guestsPerRoom;
                                                }
                                                for ($adult = 1; $adult <= 10; $adult++) {
                                                    $selected = "";
                                                    if ($adult == $no_of_guests) {
                                                        $selected = ' selected="selected" ';
                                                    }
                                                    echo "<option " . $selected . " value='" . $adult . "'>" . $adult . "</option>";
                                                }
                                                ?>        
                                            </select>
                                        </div>
                                        <div class="controls  pull-left">
                                            <label class="control-label"><strong>CHILDREN</strong></label><br>
                                            <select name="child_per_room" class="selectbox">
                                                <!?php
                                                if ($bsisearch) {
                                                    $no_of_child = $bsisearch->childPerRoom;
                                                }
                                                for ($child = 0; $child <= 10; $child++) {
                                                    $selected = "";
                                                    if ($child == $no_of_child) {
                                                        $selected = ' selected="selected" ';
                                                    }
                                                    echo "<option " . $selected . " value='" . $child . "'>" . $child . "</option>";
                                                }
                                                ?>  
                                            </select>
                                        </div>
                                        <input type="submit" name="home_reservation" id="btn_room_search" class="btn btn-large btn-primary" value="Check Room Availability" />-->

                </form>
            </div>
        </div>
    </div>    
</section>


<script type="text/javascript">
    // jQuery(document).ready(function() {
    //     jQuery('#btn_room_search').click(function() { 	            
    //         if(jQuery('#txtFromDate').val()==""){
    //             alert('<?php echo mysql_real_escape_string(ENTER_CHECK_IN_DATE_ALERT); ?>'); 
    //             return false;
    //         }else if(jQuery('#txtToDate').val()==""){
    //             alert('<?php echo mysql_real_escape_string(ENTER_CHECK_OUT_DATE_ALERT); ?>'); 
    //             return false;
    //         } else {               
    //             return true;               
    //         }	  
    //     });	
    // });
</script>