<?php
define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('ASSET_URL', TEMPLATE_URL . '/assets/');
require_once('includes/aq_resizer.php');
// post type
define('RN_ROOM', 'room');
define('RN_REVIEW', 'review');
define('RN_POST', 'post');

// pages
define('RN_SP_OFFERS', 16);
define('RN_ABOUT', 8);
define('RN_SERVICES_AND_FACILITIES', 10);
define('RN_GALLERY', 12);
define('RN_METETING_ROOM',32);
define('RN_RESTAURANT', 34);
define('RN_NEAR_PLACE', 14);
define('RN_ROOM_PAGE', 100);
define('RN_SEARCH_ROOM', 134);

#######################################################
/* * ********* CSS merge and minify process *********** */
#######################################################

if ($_SERVER['HTTP_HOST'] == 'renohotel.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
    require( 'includes/class.magic-min.php' );
    $minified = new Minifier(
            array(
        'echo' => false
            )
    );
    //exclude example
    $css_exclude = array(
            //TEMPLATEPATH . '/assets/css/bootstrap.css', 
            //TEMPLATEPATH . '/assets/css/flexslider.css'
    );
    //order example
    $css_order = array(
            TEMPLATEPATH . '/assets/css/combine/style.css',     
            TEMPLATEPATH . '/assets/css/combine/default2.css',
            TEMPLATEPATH . '/assets/css/combine/nivo-slider.css',                
            TEMPLATEPATH . '/assets/css/combine/responsiveCarousel.min.css',     
            TEMPLATEPATH . '/assets/css/combine/z.css',
            // TEMPLATEPATH . '/assets/css/combine/media_screen.css',
    );

    $minified->merge(TEMPLATEPATH . '/assets/css/combine.min.css', TEMPLATEPATH . '/assets/css/combine', 'css', $css_exclude, $css_order);
}

function the_template_url() {
    echo TEMPLATE_URL;
}

################################################################################
// Enqueue Scripts
################################################################################
// add_action( 'wp_default_scripts', function( $scripts ) {
//     if ( ! empty( $scripts->registered['jquery'] ) ) {
//     $jquery_dependencies = $scripts->registered['jquery']->deps;
//     $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
//     }
// } );

function init_scripts() {
    wp_deregister_script('wp-embed');
    wp_deregister_script('jquery');
    wp_deregister_script('comment-reply');
}

function add_scripts() {
    $js_path = ASSET_URL . 'js';
    $css_path = ASSET_URL . 'css';
    if ($_SERVER['HTTP_HOST'] == 'renohotel.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),            
            // array(
            //     'name' => 'jquery.nivo.slider',
            //     'src' => $js_path . '/jquery.nivo.slider.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(
            //     'name' => 'responsiveCarousel.min',
            //     'src' => $js_path . '/responsiveCarousel.min.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(
            //     'name' => 'bootstrap-hover-dropdown',
            //     'src' => $js_path . '/bootstrap-hover-dropdown.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),            
            // array(
            //     'name' => 'jquery.prettyphoto',
            //     'src' => $js_path . '/jquery.prettyphoto.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(
            //     'name' => 'easing',
            //     'src' => $js_path . '/jquery.easing.min.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            
            array(
                'name' => 'plugins',
                'src' => $js_path . '/plugins.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name' => 'google-font',
                'src' => 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800|Lato:700|Raleway|Roboto+Condensed:300,400',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ), 
            array(
                'name'  => 'bootstrap',
                'src'   => $css_path . '/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name' => 'font-awesomes',
                'src' => $css_path . '/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
            array(
                'name' => 'media_screen',
                'src' => $css_path . '/media_screen.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
            
        );
    } else {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' =>  'https://cdn.jsdelivr.net/jquery/2.1.4/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' =>  'https://cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),          
            array(
                'name' => 'plugins',
                'src' => $js_path . '/plugins.js',
                'dep' => 'jquery',
                'ver' => ASSET_VERSION,
                'is_footer' => true
            ),
            array(
                'name'      => 'script',
                'src'       => $js_path . '/script.js',
                'dep'       => 'jquery',
                'ver'       => ASSET_VERSION,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name' => 'google-font',
                'src' => 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800|Lato:700|Raleway|Roboto+Condensed:300,400',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ), 
            array(
                'name'  => 'bootstrap',
                'src'   =>  'https://cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name' => 'font-awesomes',
                'src' =>  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
            array(
                'name' => 'media_screen',
                'src' => $css_path . '/media_screen.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    }
    foreach ($js_libs as $lib) {
        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);
    }
    foreach ($css_libs as $lib) {
        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);
    }
}

function my_deregister_scripts() {
    global $post_type;

    if (!is_front_page() && !is_home()) {
        
    }
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

function my_deregister_styles() {
    global $post_type;
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

if (!is_admin())
    add_action('init', 'init_scripts', 10);
add_action('wp_enqueue_scripts', 'add_scripts', 10);
add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);
add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);



################################################################################
// Add theme support
################################################################################

add_theme_support('automatic-feed-links');
add_theme_support('nav-menus');
add_post_type_support('page', 'excerpt');
add_theme_support('post-thumbnails', array('post', 'page',RN_ROOM));

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main' => 'Main',
                'footer' => 'Footer',
                'helpful' => 'Helpful Info',
                'privacy' => 'Privacy Info',
            )
    );
}

################################################################################
// Add theme sidebars
################################################################################

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Main - Sidebar'),
        'id' => 'main-sidebar-widget-area',
        'description' => 'Widgets in this area will be shown on the right sidebar of default page',
        'before_widget' => '<aside class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
    ));
//    register_sidebar(array(
//        'name' => __('Page - Sidebar'),
//        'id' => 'page-widget-area',
//        'description' => 'Widgets in this area will be shown on the right sidebar of pages',
//        'before_widget' => '<aside class="widget">',
//        'after_widget' => '</aside>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Newsletter - Sidebar'),
//        'id' => 'footer-newsletter-widget-area',
//        'description' => 'Widgets in this area will be shown on the top of footer',
//        'before_widget' => '<div class="row" id="newsletter-form-container">',
//        'after_widget' => '</div>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Left - Sidebar'),
//        'id' => 'footer-left-widget-area',
//        'description' => 'Widgets in this area will be shown on the left-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Middle - Sidebar'),
//        'id' => 'footer-middle-widget-area',
//        'description' => 'Widgets in this area will be shown in the middle of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Right - Sidebar'),
//        'id' => 'footer-right-widget-area',
//        'description' => 'Widgets in this area will be shown on the right-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
}



################################################################################
// Pagination
################################################################################
if ( ! function_exists( 'dd_pagination' ) ) :
    function dd_pagination() {
        global $wp_query;

        $big = 999999999; // need an unlikely integer
        
        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
                        'show_all' => false,
                        'end_size' => 1,
                        'mid_size' => 2,
                        'prev_next' => true,
                        'prev_text' => __('First'),
                        'next_text' => __('Last'),
                ) );
    }
endif;

//post per page
function progress_archive( $query ) {
     if( $query->is_main_query() && !is_admin() && is_post_type_archive( RN_REVIEW ) ) {
        $query->set( 'posts_per_page', '4' );
    }
}
add_action( 'pre_get_posts', 'progress_archive' ); 
################################################################################
// Comment formatting
################################################################################

function theme_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <header class="comment-author vcard">
    <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>
                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>
            </header>
                <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

    <?php comment_text() ?>

            <nav>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </nav>
        </article>
        <!-- </li> is added by wordpress automatically -->
                <?php
            }
            

// booking functions
function the_search_hidden_fields($id = null) {
        global $wpdb, $post;
        if ($id == null) {
            $id = $post->ID;
        }
        $roomtypes = $wpdb->get_results("SELECT roomtype_ID FROM dots_roomtype WHERE room_post_id = '" . $id . "'");
        $id_list = array();
        foreach($roomtypes as $rid){
            $id_list[] = $rid->roomtype_ID;
        }
        $ids = implode(",", $id_list);
        ?>
        <input type="hidden" name="roomtype_id" value="<?php echo $ids; ?>">
        <input type="hidden" name="capacity" value="1">
        <input type="hidden" name="check_in" value="<?php echo current_time('d/m/Y'); ?>">
        <input type="hidden" name="check_out" value="<?php echo date("d/m/Y", strtotime("+7 days")); ?>" />
    <?php
}

function dd_get_room_price($post_ID = Null){
        global $wpdb;
        
        // if there is no $post_ID get from global
        if( empty($post_ID) ){
            global $post;
            $post_ID = $post->ID;
        }
        
        // get special price first from dots_priceplan table
        $result = $wpdb->get_row("SELECT pp.*
                        FROM dots_priceplan pp
                            INNER JOIN dots_roomtype rt on pp.roomtype_id = rt.roomtype_id
                            INNER JOIN dots_capacity cc on pp.capacity_id = cc.id
                        WHERE rt.room_post_id = $post_ID
                            AND CURDATE() BETWEEN start_date and end_date
                            AND pp.sun != 0");
        
        // If there is no special price get default price
        if( empty($result)){
            $result = $wpdb->get_row("SELECT pp.*
                        FROM dots_priceplan pp
                            INNER JOIN dots_roomtype rt on pp.roomtype_id = rt.roomtype_id
                            INNER JOIN dots_capacity cc on pp.capacity_id = cc.id
                        WHERE rt.room_post_id = $post_ID
                            AND pp.default_plan=1
                            AND pp.sun != 0");
        } 
        
        // Get a textual representation of a day, three letters eg: mon, tue
        $today = strtolower(date('D'));
        // remove .00
        $price = str_replace('.00', '', $result->$today);
        
        return $price;
    }


// function mine ($rid = 0) {

//     global $wpdb;
//     // get by room typ id and capacity id
//     $getcid = "SELECT `capacity_id` FROM dots_room where `roomtype_id`=$rid group by `roomtype_id`";
    
//     $cidresult=$wpdb->get_results($getcid);
//     $cid=$cidresult[0]->capacity_id;
//     if ($cid) {
//             $subquery = " where id=" . $cid;
//         } else {
//             $subquery = "";
//         }
//         $getcval = "select * from dots_capacity" . $subquery;
//         $cvalue=$wpdb->get_results($getcval);
        
//         var_dump($cvalue);
//         // var_dump($cvalue[0]->capacity);
// }

function ajax_get_adult_per_room() {
    global $wpdb;
    $room_id=$_REQUEST['room_id'];
    // var_dump($room_id);
    // get by room typ id and capacity id
    $getcid = "SELECT `capacity_id` FROM dots_room where `roomtype_id`=".$room_id." group by `roomtype_id`";
    
    $cidresult=$wpdb->get_results($getcid);

    $cid=$cidresult[0]->capacity_id;

    if ($cid) {
            $subquery = " where id=" . $cid;
        } else {
            $subquery = "";
        }

        $getcval = "select * from dots_capacity" . $subquery;
        $cvalue=$wpdb->get_results($getcval);

    if( $cvalue[0]->capacity > 0) {
        $data['return_capicity'] = $cvalue[0]->capacity; 
    }
    echo json_encode($data);
    die;
}

add_action('wp_ajax_get_adult_per_room', 'ajax_get_adult_per_room');           // for logged in user  
add_action('wp_ajax_nopriv_get_adult_per_room', 'ajax_get_adult_per_room');

function get_home_capacity ($rid = 0) {
    global $wpdb;
    
    $getcid = "SELECT `capacity_id` FROM dots_room where `roomtype_id`=".$rid." group by `roomtype_id`";
    $cidresult=$wpdb->get_results($getcid);

    $cid=$cidresult[0]->capacity_id;

    if ($cid) {
            $subquery = " where id=" . $cid;
        } else {
            $subquery = "";
        }

        $getcval = "select * from dots_capacity" . $subquery;
        $cvalue=$wpdb->get_results($getcval);
        return $cvalue[0]->capacity;
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}