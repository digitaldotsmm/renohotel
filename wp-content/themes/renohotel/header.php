<?php global $THEME_OPTIONS; ?>
<!doctype html>
<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<?php if ( file_exists(TEMPLATEPATH .'/favicon.png') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script type="text/javascript">
 $ = $.noConflict();
$(document).ready(function(){

        $('#btn_room_search').click(function() {               
            if(jQuery('#txtFromDate').val()==""){
                alert('Please Enter Check In Date'); 
                return false;
            }else if(jQuery('#txtToDate').val()==""){
                alert('Please Enter Check Out Date'); 
                return false;
            } else {               
                return true;               
            }     
        }); 
        $('.header_submitbox').click(function() {               
            if(jQuery('#checkin').val()==""){
                alert('Please Enter Check In Date'); 
                return false;
            }else if(jQuery('#checkout').val()==""){
                alert('Please Enter Check Out Date'); 
                return false;
            } else {               
                return true;               
            }     
        }); 

    $( "#roomtype_id" ).change(function () {

        var rtid= $( "#roomtype_id" ).val();
        if(rtid){
                   $.ajax({                                                     
                    url: '/wp-admin/admin-ajax.php',
                    type:"post",                        
                    data: {
                        action: 'get_adult_per_room',
                        'room_id':rtid
                    },
                    async: false,
                    dataType:"json",                   
                    success:function(response){  
                        var selector = $("#capacity"); 
                        selector.html('');//remove all default values
                        if(response.return_capicity > 0){
                            var return_capicity =response.return_capicity;                                                                   
                            // $("#roomtype_id").val(return_capicity);                                               
                            for (i=1;i<=return_capicity;i++){
                                selector.append($("<option />").val(i).text(i));                               
                            }
                            $(selector).prop("disabled", false); // Element(s) are now enabled.
                        }
                        else{
                            selector.append($("<option />").val(0).text('Adult Per Room'));
                            $("#capacity").val('0');
                            $(selector).prop("disabled", true);

                        }
                    }
                }); 
            }
           
    });

}); 
</script>
</head>
<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() )echo $body_classes; ?>">
    <div class="toggle_drp_arrow_box">
        <a href="#" onClick="show_hide(1)" id="down_img"><img src="<?php echo ASSET_URL;?>images/dropdown-arrow.png"  alt=""/></a>
        <a href="#" onClick="show_hide(2)" id="up_img" style="display:none"><img src="<?php echo ASSET_URL;?>images/dropup-arrow.png"  alt="" /></a>
    </div>


    <div class="header_panel">
        <?php if($THEME_OPTIONS['logo']) {$logo=$THEME_OPTIONS['logo'];}else {$logo=ASSET_URL.'images/logo.png';}?>
        <div class="header_panel_left"><a href="#"><img src="<?php echo $logo;?>"  alt="Reno Hotel Logo"/></a></div>
        <div class="header_panel_right">
            
            <div class="header_panel_right_top <?php echo (is_page(RN_SEARCH_ROOM))?'topbanner':'';?>" id="on_off_div" >
            <?php if(!is_page(RN_SEARCH_ROOM)) { ?>
            <form class="form-inline reservation-horizontal clearfix" role="form" method="post" action="<?php echo WP_HOME; ?>/search-room/" name="reservationform" id="homereservationform">  
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 header_panel_right_top_inner">
                    <p>check-in date</p>
                    <input name="check_in" type="text" id="checkin" value="<?php echo ($bsisearch && $bsisearch->checkInDate) ? $bsisearch->checkInDate : ''; ?>" class="form-control inputtextbox required datepicker" placeholder="Select a Date"/>
                </div>
                
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 header_panel_right_top_inner">
                    <p>check-out date</p>
                    <input name="check_out" type="text" id="checkout" value="" class="form-control inputtextbox required datepicker" placeholder="Select a Date"/>
                </div>
                
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 header_panel_right_top_inner rt_select">
                    <p>Room Type</p>
                   <?php        
                     include_once( HOTELBOOKING_MANAGER_PATH . "/includes/admin.class.php" );
                      $bsiAdminMain = new bsiAdminCore;
                     $roomtypeCombo = $bsiAdminMain->generateRoomtypecombo();
                     echo $roomtypeCombo;
                     ?>
                </div>
                
                 <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 header_panel_right_top_inner gender_count">
                    <p>Adult / Room</p>
                    <select name="capacity" id="capacity" class="selectbox  form-control required">
                       <option value="0">0</option>
                    </select>
                </div>
                
                 <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 header_panel_right_top_inner gender_count">
                    <p>Kids</p>
                    <select name="child_per_room" class="selectbox form-control required">
                        <?php
                        if ($bsisearch) {
                            $no_of_child = $bsisearch->childPerRoom;
                        }
                        for ($child = 0; $child <= 10; $child++) {
                            $selected = "";
                            if ($child == $no_of_child) {
                                $selected = ' selected="selected" ';
                            }
                            echo "<option " . $selected . " value='" . $child . "'>" . $child . "</option>";
                        }
                        ?>  
                    </select>  
                </div>
                
                 <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 header_panel_right_top_inner">
                    <button type="submit" id="registerButton1" class="btn btn-primary btn-block header_submitbox">CHECK NOW!</button>
                 </div>
           </form>
           <?php }?>
            </div>
            
            
            <div class=" header_panel_right_bottom">
             <nav  class="navbar" role="banner">
                  
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                    
                <div class="collapse navbar-collapse menupanel">                   
                    <?php
                        wp_nav_menu(
                                array(
                                    'theme_location' => 'main',
                                    'menu_id'        => 'menu-mainmenu',
                                    'menu_class'     => 'nav navbar-nav',
                                    'container'      => false,
                                    // 'walker' => new wp_bootstrap_navwalker()
                                )
                        );
                    ?>  
                </div>
                
    </nav>
            </div>
        </div>
    </div>

    <?php if(is_front_page() || is_home()){?>
    <div class="slider_panel">
        
        <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <?php $sliders=get_field('slider');?>
                <?php foreach ($sliders as $key => $slider) { ?>
                        <img src="<?php echo $slider['image'];?>" alt="<?php echo $slider['title'];?>" title="<?php echo $slider['title'];?>" />
                <?php }?>           
            </div>
            <div class="slider_link"><a href="<?php echo get_the_permalink(RN_SP_OFFERS);?>" title="<?php echo $slider['title'];?>">Read More</a></div>
      </div>
        
    </div>
    <!-- <div class="drop_scroll"><a class="page-scroll" href="#welcome_id"><img src="<?php echo ASSET_URL;?>images/drop-white.png" alt=""/>SCROLL DOWN</a></div>  -->
    <?php } else{ ?>
        <div class="page-title-wrapper">
            <div class="banner-wrapper container">
                <?php
                     // get current quried object
                    $quried_obj = get_queried_object();                                
                    // if is archive page
                    if( is_archive() ){  
                        // if is taxonomy archive
                        if( is_tax() ){                    
                            $head_title=ucfirst($quried_obj->name);
                        }else{ // if is normal archive
                            $head_title=ucfirst($quried_obj->labels->name);
                        }
                    }                               
                    elseif(is_singular()){ // if is single page
                        $head_title=get_the_title();
                    }            
                ?>
                <h2 class="heading_title">
                    <?php echo $head_title;?>
                </h2>
            </div>
        </div>
    <?php }?>

    <?php// echo mine('37');?>