<?php global $THEME_OPTIONS; ?>
<div class="footer_panel">
    <div class="container">
        <div class="footer_panel_top">
            <img src="<?php echo ASSET_URL;?>images/footer-logo.png" alt="Reno Hotel Logo"/>
            <p><?php echo $THEME_OPTIONS['info_phone'];?></p>
            <p><?php echo $THEME_OPTIONS['info_email'];?></p>
            <p><span><?php echo nl2br($THEME_OPTIONS['info_address']);?></span></p>
        </div>
    </div>
    
    <div class="footer_bottom">
        <div class="container">
            <div class="footer_bottom_top">
                <?php
                    $menuParameters = array(
                        'theme_location' => 'footer',
                        'container'       => false,
                        'echo'            => false,
                        'items_wrap'      => '%3$s',
                        'depth'           => 0,
                    );

                    echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
                ?>  
            </div>
            <div class="footer_bottom_bottom">   
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6 tripadvisor_widget">                        
                        <div id="TA_certificateOfExcellence981" class="TA_certificateOfExcellence"><ul id="OQxw89Z" class="TA_links wUHzoy"><li id="tqghYHPnRD" class="BhnTlE3W9"><a target="_blank" href="https://www.tripadvisor.com/Hotel_Review-g294191-d6641784-Reviews-Reno_Hotel-Yangon_Rangoon_Yangon_Region.html"><img src="https://www.tripadvisor.com/img/cdsi/img2/awards/CoE2017_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a></li></ul></div><script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=981&amp;locationId=6641784&amp;lang=en_US&amp;year=2017&amp;display_version=2"></script>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="social_links">
                            <?php if($THEME_OPTIONS['facebookid']) { ?><a href="<?php echo $THEME_OPTIONS['facebookid'];?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> :: Facebook</a><?php }?>
                            <?php if($THEME_OPTIONS['twitterid']) { ?><a href="<?php echo $THEME_OPTIONS['twitterid'];?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> :: Twitter</a><?php }?>
                            <?php if($THEME_OPTIONS['linkedinid']) { ?><a href="<?php echo $THEME_OPTIONS['linkedinid'];?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i> :: Linkedin</a><?php }?>
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
    </div>
    <div class="footer_extreme_bottom">
        <div class="container">copyright <?php echo date("Y");?> &copy; RenoHotelMyanmar <span>developed by &nbsp; &nbsp; <a href="http://www.digitaldots.com.mm" target="_blank"><img src="<?php echo ASSET_URL; ?>images/digitaldots.png" alt="Web Development in Yangon: Digital Dots" class="ddlogo" /></a></span></div>
    </div>
    
</div>

<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88426121-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- trip advisor widget -->
<script src="https://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq=977&amp;locationId=6641784&amp;lang=en_US&amp;border=true&amp;display_version=2"></script>

</body>
</html>