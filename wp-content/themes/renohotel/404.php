<?php get_header(); ?>

	<section class="section about_us">
		<div class="container">
			<div class="row">
				<div class="col-md-12"> 					
		    		<div>
		    			<h2><?php _e("Search Results"); ?></h2>	
						<?php if( $_REQUEST['calcat'] ) : ?>
							<p>No event is found.</p>
						<?php else: ?>
							<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.' ); ?></p>
							<?php get_search_form(); ?>	
						<?php endif; ?>
						<div class="clear"></div>
	    			</div>				    
				</div>
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>