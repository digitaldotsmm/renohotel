<?php get_header(); ?>
	<?php
		$welcome_message=get_field('welcome_message');
		echo $welcome_message;
	?>
	<a href="<?php echo get_permalink(RN_ABOUT); ?>" title="Read More About Us" >More About Us</a>

	<!-- room section -->
	<?php 
		$rooms = get_posts(
	        array(
	            'post_type' => RN_ROOM,
	            'posts_per_page' => 3,
	            'orderby' => 'rand'
	        )
		);
		foreach ($rooms as $key => $room) :
			$room_title=$room->post_title;
			$room_link=get_permalink($room->ID);
			$room_img = get_attachment_image_src($room->ID, 'large');	
			// $roomnew_img=aq_resize($room[0],370,345,true,true,true);	
	?>
			<img src="<?php echo $room[0];?>" alt="<?php echo $room_title;?>" class="img-responsive">
			<a href="<?php echo $room_link; ?>" title="<?php echo $room_title;?>" >Detail</a>
	<?php
			echo $room_title;
		endforeach;														
	?>

		<!-- meeiting room -->
		<?php  
			$meet_room=get_post(RN_METETING_ROOM);
			$mr_img = get_attachment_image_src($meet_room->ID, 'large');	
			// $mrnew_img=aq_resize($mr_img[0],170,170,true,true,true);	
			echo $meet_room->post_title;
			echo string_limit_words(strip_tags($meet_room->post_content), 10) . '...';

		?>
		<a href="<?php echo get_permalink(RN_METETING_ROOM); ?>" title="<?php echo $meet_room->post_title;?>" >Detail</a>
		<img src="<?php echo $mr_img[0];?>" alt="<?php echo $room_title;?>" class="img-responsive">
		<!-- restaurant -->
		<?php  
			$restaurant=get_post(RN_RESTAURANT);
			$res_img = get_attachment_image_src($restaurant->ID, 'large');	
			// $resnew_img=aq_resize($res_img[0],170,170,true,true,true);	
			echo $restaurant->post_title;
			echo string_limit_words(strip_tags($restaurant->post_content), 10) . '...';
		?>
		<a href="<?php echo get_permalink(RN_RESTAURANT); ?>" title="<?php echo $restaurant->post_title;?>" >Detail</a>
		<img src="<?php echo $res_img[0];?>" alt="<?php echo $room_title;?>" class="img-responsive">
		<!-- review -->
	<?php 
		$reviews = get_posts(
	        array(
	            'post_type' => RN_REVIEW,
	            'posts_per_page' => 6,
	            'orderby' => 'rand'
	        )
		);
		foreach ($reviews as $key1 => $review) :
			$review_title=$review->post_title;
			$review_link=get_permalink($review->ID);
			echo $review_title;
	?>
		<a href="<?php echo $review_link; ?>" title="<?php echo $review_title;?>" >Detail</a>
	<?php
		endforeach;														
	?>
	<a href="/<?php echo RN_REVIEW; ?>/" title="See All Review" >Detail</a>

	

<?php get_footer(); ?>