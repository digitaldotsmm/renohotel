<?php
/*
 * Template Name: Reservation
 */
?>
<?php get_header(); ?>
<div class="content-block about-block">
    <h2 class="page-h2"><?php the_title(); ?></h2> 
</div>
<div id="wrapper">
    <div id="reservation" class="content-sec">                
        <div class="container about-sec">
            <div class="divide50"></div>
            <div class="row">                            

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h2> Contact Person </h2>
                    <hr class="reservation-hr">
                    <div id="thank_message" style="display: none;">Thank you for choosing to stay with us at Hotel Hotel.</div> 
                    <form id="easyFrontendFormular" method="post" name="easyFrontendFormular" action="<?php the_permalink(); ?>">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="title" class="col-xs-12 col-sm-4 control-label reservation-label">Title *</label>
                                    <div class="col-xs-12 col-sm-2">
                                        <select name="title" id="title"  class="form-control reservation-label required">
                                            <option value="Mr">Mr</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Ms">Ms</option>
                                            <option value="Dr">Dr</option>
                                            <option value="Prof">Prof</option>
                                            <option value="Rev">Rev</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="lastname" class="col-xs-12 col-sm-4 control-label reservation-label">First name*</label>
                                    <div class="col-xs-12 col-md-8">
                                        <input type="text" class="form-control reservation-label required" name="firstname" id="firstname">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="firstname" class="col-xs-12 col-sm-4 control-label reservation-label">Last name*</label>
                                    <div class="col-xs-12 col-md-8">
                                        <input type="text" class="form-control reservation-label required" name="lastname" id="lastname">
                                    </div>                                    
                                </div>                                

                                <div class="col-xs-12 col-sm-12 form-group">         
                                    <label for="email" class="col-xs-12 col-sm-4 control-label reservation-label">Email*</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <input type="email" class="form-control reservation-label required email" name="email" id="email">
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="phone" class="col-xs-12 col-sm-4 control-label reservation-label">Phone *</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <input type="text" name="phone" id="phone" class="reservation-label form-control required">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="addressline1" class="col-xs-12 col-sm-4 control-label reservation-label">Address Line 1 *</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <input type="text" name="addressline1" id="addressline1" class="reservation-label form-control required">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="addressline2" class="col-xs-12 col-sm-4 control-label reservation-label">Address Line 2</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <input type="text" name="addressline2" id="addressline1" class="reservation-label form-control">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="town_city" class="col-xs-12 col-sm-4 control-label reservation-label">Town/City *</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <input type="text" name="town_city" id="addressline1" class="form-control reservation-label required">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="country" class="col-xs-12 col-sm-4 control-label reservation-label">Country *</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <select name="country" id="country" class="form-control reservation-label required">
                                            <?php
                                            $cou = array('Afghanistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas,The', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bosnia', 'Botswana', 'Bougainville', 'Brazil', 'British Indian Ocean', 'British Virgin Islands', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde Islands', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China, Hong Kong', 'China, Macau', 'China, People \'s Republic', 'China, Taiwan', 'Colombia', 'Comoros', 'Congo, Democratic Republic of', 'Congo, Republic of', 'Cook Islands', 'Costa Rica', 'Cote d\'Ivoire', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Faeroe Islands', 'Falkland Islands', 'Federated States of Micronesia', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'Gabon', 'Gambia, The', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Holy See (Vatican City State)', 'Honduras', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, Democratic People\'s Rep', 'Korea, Republic of', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Antilles', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Norway', 'Oman', 'Pakistan', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'R&ecirc;union', 'Romania', 'Russia', 'Rwanda', 'Saint Barthelemy', 'Saint Helena', 'Saint Kitts & Nevis', 'Saint Lucia', 'Saint Martin', 'Saint Pierre & Miquelon', 'Saint Vincent', 'Samoa', 'San Marino', 'Sao Tom&ecirc; & Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Tajikistan', 'Tanzania', 'Thailand', 'Timor Leste', 'Togo', 'Tokelau Islands', 'Tonga', 'Trinidad & Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks & Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom of GB & NI', 'United States of America', 'Uruguay', 'US Virgin Islands', 'Uzbekistan', 'Vanuatu', 'Venezuela', 'Vietnam', 'Wallis & Futuna Islands', 'Yemen', 'Zambia', 'Zimbabwe');
                                            for ($c = 0; $c < 226; $c++) {
                                                if (isset($_POST['country']) AND $_POST['country'] == $cou[$c])
                                                    $select = "selected='selected'"; else
                                                    $select = "";
                                                echo "<option value='" . $cou[$c] . "'" . $select . " style='text-align:left;margin-left:5px;' >" . $cou[$c] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="postcode" class="col-xs-12 col-sm-4 control-label reservation-label">Postcode *</label>
                                    <div class="col-xs-12 col-sm-2">
                                        <input type="text" name="postcode" id="postcode" class="form-control reservation-label required">
                                    </div>                                    
                                </div>

                                <h2>Booking Information</h2>
                                <hr class="reservation-hr">
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="intend_date" class="col-xs-12 col-sm-4 control-label reservation-label">When do you intend to visit?</label>
                                    <div class="col-xs-12 col-sm-2">
                                        <input type="text" name="intend_date" id="intend_datepicker" class="form-control reservation-label">
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="intend_day" class="col-xs-12 col-sm-4 control-label reservation-label">How many days do you intend to stay?</label>
                                    <div class="col-xs-12 col-sm-2">
                                        <select name="intend_day" class="form-control reservation-label">
                                            <?php
                                            $days = range(1, 31);
                                            foreach ($days as $day) {
                                                ?>
                                                <option value ="<?php echo $day; ?>"><?php echo $day; ?></option>
                                                <?php
                                            }
                                            ?>  
                                        </select>
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="people" class="col-xs-12 col-sm-4 control-label reservation-label">How many people?</label>
                                    <div class="col-xs-12 col-sm-2">
                                        <select name="people" class="form-control reservation-label">
                                            <?php
                                            $people = range(1, 5);
                                            foreach ($people as $p) {
                                                ?>
                                                <option value ="<?php echo $p; ?>"><?php echo $p; ?> person</option>
                                                <?php
                                            }
                                            ?>  
                                            <option value="5">5 more..</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="package" class="col-sm-4 control-label reservation-label">Room Type</label>
                                    <div class="col-sm-2">
                                        <select name="roomtype" id="roomtype" class="selectbox form-control reservation-label" >
                                            <option value="" disabled selected>Select a Room</option>
                                            <option value="Premier Suite">Premier Suite</option>
                                            <option value="Executive Suite">Executive Suite</option>
                                            <option value="Deluxe">Deluxe</option>
                                            <option value="Superior">Superior</option>
                                            <option value="Standard">Standard</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="question_request" class="col-xs-12 col-sm-4 control-label reservation-label">Additional question or request</label>
                                    <div class="col-xs-12 col-sm-8">                                        
                                        <textarea id="question_request" name="question_request" class="form-control reservation-label" rows="8" cols="100"></textarea>
                                    </div>                                    
                                </div>

                                <div class="col-xs-12 col-sm-12 form-group">
                                    <label for="captcha" class="col-xs-12 col-sm-4 control-label reservation-label">Captcha</label>
                                    <div class="col-xs-12 col-sm-8">
                                        <img src="<?php bloginfo('template_url'); ?>/includes/coolcaptcha/captcha.php" id="captcha">
                                        <div class="captcha_info">
                                            <a href="#" onclick="
                                                document.getElementById('captcha').src='<?php bloginfo('template_url'); ?>/includes/coolcaptcha/captcha.php?'+Math.random();
                                                document.getElementById('captcha-form').focus();return false;" id="change-image">Not readable?<br> Change text.</a>
                                        </div>
                                        <br>
                                        <input type="text" name="captcha" id="captcha-form" autocomplete="off" class="required captcha all_boxs"><br>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 form-group">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8"><button class="btn btn-primary" type="submit">Book now</button> </div>
                                </div>
                            </div>
                        </div>
                        <div id="processing" style="display: none"><p>Processing...please wait</p></div>
                        <input type="hidden" name="form_type" value="reservation_form"/>
                    </form>
                    <!--                    <div id="success" class="alert alert-success msg-hide">                          
                                            <p>Your message was sent successfully! I will be in touch as soon as I can.</p>
                                        </div>-->
                </div>
            </div>                 
        </div>
    </div>
</div>
<?php get_footer(); ?>