<?php $post_type = get_queried_object(); get_header();?>
    <section class="section contact_wrapper">
        <div class="container mt50">
            <div class="row"> 
                <section id="" class="blog">
                    <div class="col-md-12">
                        <?php                
                            global $wp_query;
                            $i=1;
                            $wrap_div='<div class="row">';
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;                          
                            $blog_args = array('post_type' => $post_type->name,'posts_per_page' => 3,'paged' => $paged);

                            $wp_query=new WP_Query($blog_args);        
                            if ($wp_query->have_posts()) : 
                              echo '<div class="row">';
                                while ($wp_query->have_posts()) : $wp_query->the_post(); 
                                    $bimg_url= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                                    $bnew_img=  aq_resize($bimg_url[0],360,270,true,true,true);
                                    $post_title = get_the_title();
                                    $permalink=  get_the_permalink();
                                    $content=get_the_content();
                        ?>    
                                      <div class="col-md-4 col-sm-4">
                                      <div class="each_review each_blog">

                                      	<?php if($bnew_img){?>    <a href="<?php echo $permalink; ?>" title="<?php echo $post_title;?>" class="" ><img src="<?php echo $bnew_img;?>" title="<?php echo $post_title;?>" class="img-responsive hover_effect"></a>     <?php } ?>                             
                                        <?php if(get_field('event_date')) { ?>
                                        <div class="post_date_wrapper">
                                              <div class="review_date"><?php echo get_field('event_date');?></div>
                                        </div>
                                        <?php } ?>
                                      <!-- </div>                                      -->
                                      <!-- <div class="col-xs-12 col-md-4 post_wrapper"> -->
                                          <header class="entry-header"><a href="<?php echo $permalink; ?>" title="Read More" class="" ><h3><?php echo $post_title;?></h3></a></header> 
                                          <div class="entry-summary"><?php echo content(30);?></div><!-- .entry-summary --> 
                                          <a href="<?php echo $permalink; ?>" title="Read More" class="read_more" >Read More</a>         
                                      </div>
                                  </div>    

                        <?php if ($i % 3 === 0 ) { echo '</div>' . $wrap_div; }
                                                    $i++;
                         endwhile; echo '</div>'; ?>

                            <nav aria-label="Page navigation">
                              <ul class="pagination">
                                  <?php dd_pagination(); ?>
                              </ul>
                          </nav>
                      <?php  else:
                             wp_reset_query();
                        endif;
                     ?>
                    </div>
                </section>

                <?php //get_sidebar();?>
          </div>
        </div>
    </section>
<?php get_footer();?>