<?php
/*
 * Template Name:Service and Facility
 */
?>
<?php get_header(); ?>

<section class="section services">
	<div class="container">
		<div class="row">
			<div class="col-md-12"> 
				<div class="tabs-left">
					<?php
							$service_args = array(
							        'post_type' => 'page',
							        'order'		=> 'ASC',
							        'post_parent' => RN_SERVICES_AND_FACILITIES
							    );
						    $service_childs = get_posts( $service_args );	    
					?>

					        <ul class="nav nav-tabs">
					        	<?php 
									foreach($service_childs as  $ser_key=>$service_child) {
							    	$ser_imgs=get_attachment_image_src($service_child->ID, 'full');
							    	//var_dump($service_child);die;
							    	//$ser_img=aq_resize($air_imgs[0],71,47,true,true,true);  
							    	$ser_title=$service_child->post_title;	 
							    	$ser_name=$service_child->post_name;	
							    	if($service_child->ID == RN_RESTAURANT) {
							    		$icon= '<i class="fa fa-cutlery"></i>';
							    	}elseif($service_child->ID == RN_METETING_ROOM) {
							    		$icon= '<i class="fa fa-users"></i>';
							    	}else {
							    		$icon= '<i class="fa fa-bell"></i>';
							    	}
								?>
					          		<li class="<?php echo ($ser_key==0)?'active':'';?>">
					          			<div class="service_list">						          			
						          			<a href="#<?php echo $ser_name;?>" data-toggle="tab"><?php echo $icon;?><?php echo $ser_title;?></a>
					          			</div>
				          			</li>
					          	<?php	} ?>
					        </ul>
					        <div class="tab-content">
					        	<?php 
									foreach($service_childs as  $ser_key=>$service_child) {
							    	$ser_imgs=get_attachment_image_src($service_child->ID, 'full');
							    	//$ser_img=aq_resize($air_imgs[0],71,47,true,true,true);  
							    	$ser_name=$service_child->post_name;	 
							    	$ser_content=$service_child->post_content;
								?>
					          		<div class="tab-pane <?php echo ($ser_key==0)?'active':'';?>" id="<?php echo $ser_name;?>">
					          			<?php $gals=get_field('gallery',$service_child->ID,true);if($gals) {?>
					          				<div class="gal_wrapper">					          			
	                                            <div id="slider<?php echo $ser_key;?>" class="nivoSlider">
	                                                <?php $sliders=get_field('slider');?>
	                                                <?php 
	                                                    foreach ($gals as $key => $gal) {                                                        
	                                                    $gal_imgs=  aq_resize($gal["url"],940,482,true,true,true);    
	                                                ?>
	                                                        <img src="<?php echo $gal_imgs;?>" alt="<?php echo $gal['title'];?>" title="<?php echo $gal['title'];?>" />
	                                                <?php }?>           
	                                            </div>
                                            </div>
                                        <?php }?> 
                                        
					          			<div class="service_content">	<?php echo $ser_content;?></div>
					          		</div>
					      		<?php	} ?>

					        </div><!-- /tab-content -->
					      

						</div><!-- /tabbable -->
			</div>
		</div>
	</div>
</section>
	
<?php get_footer(); ?>