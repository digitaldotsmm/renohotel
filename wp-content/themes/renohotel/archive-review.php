<?php get_header(); ?>
<section class="section reviews">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<?php 
                		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;   
                    $i=1;  
                    $wrap_div='<div class="row">';    
                		if (have_posts()) : 
                        echo '<div class="row">';
                            while (have_posts()) : the_post(); 
                                $title=get_the_title();
                                $content=get_the_content();
                    ?>                  
                    			     
                                  <div class="col-md-6 each_review">
                                      <div class="testimonials">
                                          <blockquote>
                                              <h3><?php echo get_field('title');?></h3>
                                              <div class="review_date"><i class="fa fa-calendar"></i><?php echo get_field('date');?></div>
                                              <p><?php echo $content;?></p>
                                              <div class="review_info">
                                                  <div class="review_title"><i class="fa fa-user"></i><?php echo $title;?></div> - 
                                                  <div class="review_location"><?php echo get_field('location');?></div>
                                              </div>
                                          </blockquote>
                                      </div>
                                  </div>
                    <?php if ($i % 2 === 0 ) { echo '</div>' . $wrap_div; } $i++; endwhile; echo '</div>';?>

                    		<nav aria-label="Page navigation">
                              <ul class="pagination">
                                  <?php dd_pagination(); ?>
                              </ul>
                          </nav>
                    <?php
                            else:
                             wp_reset_query();
                        endif;
                 	?>   
              </div>
        </div>
    </div>
</section>   

<?php get_footer(); ?>