<?php get_header(); ?>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">	
            	<?php 
                    $r=1;
            		if (have_posts()) : 
                        while (have_posts()) : the_post(); 
                            $img_url= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                            $new_img=  aq_resize($img_url[0],585,480,true,true,true);
                            $permalink=  get_the_permalink();
                            $title=get_the_title();
                            $content=get_the_content();
                            $usd_price = dd_get_room_price($post->ID);
                            if($r%2 ==0 ) {
                ?>
                                <div class="row room_margin">
                                    <div class="col-md-6 no_padding">
                                        <img src="<?php echo $new_img;?>" title="<?php echo $title;?>" class="img-responsive room_img" />
                                    </div>   
                                    <div class="col-md-6 no_padding">
                                        <div class="content_wrapper">
                                            <h2 class="title1"><?php echo $title;?></h2>
                                            <div class="acc_price">USD : $ <?php echo $usd_price ?></div>
                                            <div class="room_info"><?php echo $content;?></div>
                                            <a href="<?php echo $permalink; ?>" title="<?php echo $title;?>" class="read_more">detail</a>
                                            <form action="<?php echo WP_HOME; ?>/search-room/" method="post" class="Book-now-room">
                                                <input value="Book Now" name="submit" class="book_room read_more" type="submit">
                                                <?php the_search_hidden_fields(); ?>
                                            </form>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                <?php
                            }else {
                ?>
                                <div class="row room_margin">
                                    <div class="col-md-6 no_padding visible-sm visible-xs">
                                        <img src="<?php echo $new_img;?>" title="<?php echo $title;?>" class="img-responsive room_img" />
                                    </div> 
                                    <div class="col-md-6 no_padding">
                                        <div class="content_wrapper">
                                            <h2 class="title1"><?php echo $title;?></h2>
                                            <div class="acc_price">USD : $ <?php echo $usd_price ?></div>
                                            <div class="room_info"><?php echo $content;?></div>
                                            <a href="<?php echo $permalink; ?>" title="<?php echo $title;?>" class="read_more">detail</a>
                                            <form action="<?php echo WP_HOME; ?>/search-room/" method="post" class="Book-now-room">                                               
                                                <input value="Book Now" name="submit" class="book_room read_more" type="submit">
                                                <?php the_search_hidden_fields(); ?>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-6 no_padding visible-md visible-lg">
                                        <img src="<?php echo $new_img;?>" title="<?php echo $title;?>" class="img-responsive room_img" />
                                    </div>   
                                </div> <!-- end row-->
                <?php 
                            }
                ?>                  

                <?php $r++; endwhile; 
                        else:
                         wp_reset_query();
                    endif;
             	?>      
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>