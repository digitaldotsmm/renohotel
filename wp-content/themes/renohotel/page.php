<?php get_header(); ?>

	<section class="section about_us">
		<div class="container">
			<div class="row">
				<div class="col-md-12"> 					
		    		<div>
		    			<?php
		    				$img_url= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                            // $bnew_img=  aq_resize($img_url[0],585,400,true,true,true);                   
		    			?>
		    			<img src="<?php echo $img_url[0];?>" class="about_hotel" title="About Our Hotel">
		    			<div class="post_contenet"><?php echo apply_filters('the_content',$post->post_content);?></div>
	    			</div>				    
				</div>
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>
