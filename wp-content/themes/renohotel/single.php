<?php
/*
 * Template Name:Blog
 */
?>
<?php get_header();?>
    <section class="section contact_wrapper">
        <div class="container">
            <div class="row">                 
                    <div class="col-md-9">
                        <?php                       
                            if (have_posts()) : 
                                while (have_posts()) : the_post(); 
                                    
                                    $gals=get_field('gallery');                 
//                                  
                                    $post_title = get_the_title();
                                    $permalink=  get_the_permalink();
                                    $content=get_the_content();
                        ?>    
                                    <div class="row each_review each_blog">
                                      <div class="col-md-12">
                                      
                                            <div class="gal_wrapper">
                                              <?php $gals=get_field('gallery');if($gals) {?>
                                                  <div id="slider" class="nivoSlider">
                                                      <?php $sliders=get_field('slider');?>
                                                      <?php 
                                                          foreach ($gals as $key => $gal) {                                                        
                                                          $gal_imgs=  aq_resize($gal["url"],780,400,true,true,true);    
                                                      ?>

                                                              <img src="<?php echo $gal_imgs;?>" alt="<?php echo $gal['title'];?>" title="<?php echo $gal['title'];?>" />
                                                      <?php }?>           
                                                  </div>
                                              <?php }?>  
                                          </div>
                                      
                                        <div class="post_date_wrapper">
                                              <div class="review_date"><?php echo get_field('event_date');?></div>
                                        </div>
                                      </div>
                                     
                                      <div class="col-xs-12 col-md-12 post_wrapper">
                                          <!-- <header class="entry-header"><a href="<?php echo $permalink; ?>" title="Read More" class="" ><h3><?php echo $post_title;?></h3></a></header>  -->
                                          <div class="entry-summary"><?php echo apply_filters('the_content',$content);?></div><!-- .entry-summary --> 
                                                   
                                      </div>
                                  </div>                    
                        <?php endwhile; ?>
                      <?php  else:
                             wp_reset_query();
                        endif;
                     ?>
                    </div>
                
                <?php get_sidebar();?>
          </div>
        </div>
    </section>
<?php get_footer();?>