<?php
/*
 * Template Name:Blog Old
 */
?>
<?php get_header();?>
    <section class="section contact_wrapper">
        <div class="container mt50">
            <div class="row"> 
                <section id="" class="blog">
                    <div class="col-md-12">
                        <?php                
                            global $wp_query;
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;                          
                            $blog_args = array('post_type' => RN_POST,'posts_per_page' => 1,'paged' => $paged);

                            $wp_query=new WP_Query($blog_args);        
                            if ($wp_query->have_posts()) : 
                                while ($wp_query->have_posts()) : $wp_query->the_post(); 
                                    $bimg_url= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                                    $gals=get_field('gallery');                 
//                                    $bnew_img=  aq_resize($bimg_url[0],848,370,true,true,true);
                                    $post_title = get_the_title();
                                    $permalink=  get_the_permalink();
                                    $content=get_the_content();
                        ?>    
                                    <div class="row each_review">
                                      <div class="col-md-12">
                                      <?php if($bimg_url[0]) { ?>
                                            <img src="<?php echo $bimg_url[0];?>" title="" class="img-responsive">
                                      <?php  }else { ?>
                                            <div class="gal_wrapper">
                                              <?php $gals=get_field('gallery');if($gals) {?>
                                                  <div id="slider" class="nivoSlider">
                                                      <?php $sliders=get_field('slider');?>
                                                      <?php 
                                                          foreach ($gals as $key => $gal) {                                                        
                                                          $gal_imgs=  aq_resize($gal["url"],780,400,true,true,true);    
                                                      ?>

                                                              <img src="<?php echo $gal_imgs;?>" alt="<?php echo $gal['title'];?>" title="<?php echo $gal['title'];?>" />
                                                      <?php }?>           
                                                  </div>
                                              <?php }?>  
                                          </div>
                                      <?php  }?>
                                        
                                      </div>
                                      <div class="col-xs-12 col-md-3 date_wrapper">                                    
                                        <div class="post_date_wrapper">
                                              <div class="review_date"><?php echo get_field('event_date');?></div>
                                        </div>
                                      </div>
                                      <div class="col-xs-12 col-md-9 post_wrapper">
                                          <header class="entry-header"><a href="<?php echo $permalink; ?>" title="Read More" class="" ><h3><?php echo $post_title;?></h3></a></header> 
                                          <div class="entry-summary"><?php echo $content;?></div><!-- .entry-summary --> 
                                           <a href="<?php echo $permalink; ?>" title="Read More" class="read_more" >Read More</a>         
                                      </div>
                                  </div>                    
                        <?php endwhile; ?>
                            <nav aria-label="Page navigation">
                              <ul class="pagination">
                                  <?php dd_pagination(); ?>
                              </ul>
                          </nav>
                      <?php  else:
                             wp_reset_query();
                        endif;
                     ?>
                    </div>
                </section>

                <?php get_sidebar();?>
          </div>
        </div>
    </section>
<?php get_footer();?>